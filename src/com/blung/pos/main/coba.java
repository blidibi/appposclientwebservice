/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.main;

import com.blung.pos.entity.simple.Barang;
import com.blung.pos.entity.simple.Cabang;
import com.blung.pos.entity.simple.Harga;
import com.blung.pos.entity.simple.Servis;
import com.blung.pos.entity.simple.User;
import com.blung.pos.service.intrf.MasterSimpleIntrf;
import com.blung.pos.service.intrf.TransaksiORMIntrf;
import com.blung.pos.service.intrf.TransaksiSimpleIntrf;
import com.blung.pos.ui.main.mainFrame;
import com.google.gson.Gson;
import com.twmacinta.util.MD5;
import java.util.Date;
import java.util.List;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author lukman
 */
public class coba {
//    public static MasterSimpleIntrf msi;
//    public static TransaksiORMIntrf toi;
    public static TransaksiSimpleIntrf tsi;
    
    public static void main(String args[]) {
       
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("SpringXMLConfigPOSWebService.xml");
        RestTemplate restTemplate=applicationContext.getBean("restTemplate", RestTemplate.class);
//        msi = (MasterSimpleIntrf) applicationContext.getBean("mastersimple");
//        msi.setMasterSimpleRestAcces(restTemplate);
//        
//        toi = (TransaksiORMIntrf) applicationContext.getBean("transaksiorm");
//        toi.setTransaksiORMpostRestAcces(restTemplate);
        
        tsi = (TransaksiSimpleIntrf) applicationContext.getBean("transaksisimple");
        tsi.setTransaksiRestAccess(restTemplate);
        
        coba c=new coba();
        c.nyoba();
    }
    
    public void nyoba(){
        /* 
        Barang b=new Barang();
        b.setIdBarangBarcode("12345");
        b.setNamaBarang("asdfghj");
        Harga h=new Harga();
        h.setBatasDua(Long.valueOf("1"));
        h.setBatasSatu(Long.valueOf("1"));
        h.setBatasTiga(Long.valueOf("1"));
        h.setDiskonPromo(Double.valueOf("1"));
        h.setHargaBeliRata(Double.valueOf("1"));
        h.setHargaBeliTerakhir(Double.valueOf("1"));
        h.setHargaDua(Double.valueOf("1"));
        h.setHargaEmpat(Double.valueOf("1"));
        h.setHargaSatu(Double.valueOf("1"));
        h.setHargaEmpat(Double.valueOf("1"));
        h.setHargaSatu(Double.valueOf("1"));
        h.setHargaTiga(Double.valueOf("1"));
        //h.setIdBarang(Long.valueOf("1"));
        h.setIdCabang(Long.valueOf("1"));
        h.setIdKategori(Long.valueOf("1"));
        h.setIdSatuan(Long.valueOf("1"));
        h.setIdSupliyer(Long.valueOf("1"));
        b.setHarga(h);
        Gson g=new Gson();
        System.out.println(g.toJson(b));
        msi.save(b);
        
        */
        Cabang c=new Cabang();
        c.setIdCabang(Long.valueOf("1"));
        
        List<Servis> l=tsi.servisSudahByCabangAndDate(new Date(113,0,1), new Date(113,0,7), c);
        System.out.println(l.size());
    }
    
}
