/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.main;

import com.blung.pos.entity.bantuan.JmlBarang;
import com.blung.pos.entity.simple.Barang;
import com.blung.pos.entity.simple.Cabang;
import com.blung.pos.entity.simple.Harga;
import com.blung.pos.entity.simple.HutangPembelian;
import com.blung.pos.entity.simple.Karyawan;
import com.blung.pos.entity.simple.Penjualan;
import com.blung.pos.entity.simple.Satuan;
import com.blung.pos.service.intrf.MasterSimpleIntrf;
import com.blung.pos.service.intrf.TransaksiORMIntrf;
import com.blung.pos.service.intrf.TransaksiSimpleIntrf;
import com.lukman.utility.reflection.ReflecsonAlfa;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.client.RestTemplate;
/**
 *
 * @author lukman
 */
public class Test {

    public static MasterSimpleIntrf msi;
    public static TransaksiSimpleIntrf tsi;
    public static TransaksiORMIntrf toi;
    
    public static void main(String args[], TransaksiORMIntrf toi) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("SpringXMLConfigPOSWebService.xml");
        RestTemplate restTemplate=applicationContext.getBean("restTemplate", RestTemplate.class);
        msi = (MasterSimpleIntrf) applicationContext.getBean("mastersimple");
        msi.setMasterSimpleRestAcces(restTemplate);
        
        tsi = (TransaksiSimpleIntrf) applicationContext.getBean("transaksisimple");
        tsi.setTransaksiRestAccess(restTemplate);
        Test t = new Test();
        t.coba();
    }
    
    public void coba(){
        Barang b=new Barang();
        b.setIdBarangBarcode("12345");
        b.setNamaBarang("asdfghj");
        Harga h=new Harga();
        h.setBatasDua(Long.valueOf("1"));
        h.setBatasSatu(Long.valueOf("1"));
        h.setBatasTiga(Long.valueOf("1"));
        h.setDiskonPromo(Double.valueOf("1"));
        h.setHargaBeliRata(Double.valueOf("1"));
        h.setHargaBeliTerakhir(Double.valueOf("1"));
        h.setHargaDua(Double.valueOf("1"));
        h.setHargaEmpat(Double.valueOf("1"));
        h.setHargaSatu(Double.valueOf("1"));
        h.setHargaEmpat(Double.valueOf("1"));
        h.setHargaSatu(Double.valueOf("1"));
        h.setHargaTiga(Double.valueOf("1"));
        h.setIdBarang(Long.valueOf("1"));
        h.setIdCabang(Long.valueOf("1"));
        h.setIdKategori(Long.valueOf("1"));
        h.setIdSatuan(Long.valueOf("1"));
        h.setIdSupliyer(Long.valueOf("1"));
        b.setHarga(h);
        msi.save(b);
    }
    
    
    
}
