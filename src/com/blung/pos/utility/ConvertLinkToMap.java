/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.utility;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author lukman
 */
public class ConvertLinkToMap {
        
    public static Map<String,Object> convertLinkToMap(Object domain, LinkedHashMap linkedHashMap){
         
        if(linkedHashMap==null) return null;
        Map<String,Object> map=new HashMap<>();
        for(Field f : domain.getClass().getFields()) {
            map.put(f.getName().toString(), linkedHashMap.get(f.getName().toString())+"");
        }
        return map;
     }
    
    public static List<Map<String,Object>> convertLinkToMapList(Object domain, List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null) return null;
         List<Map<String,Object>> listMap=new ArrayList<>();
         for(LinkedHashMap linkedHashMap : linkedHashMaps){
             listMap.add(convertLinkToMap(domain, linkedHashMap));
         }
         return listMap;
     }
}
