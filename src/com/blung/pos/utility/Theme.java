/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.utility;

import java.util.Properties;

/**
 *
 * @author lukman
 */
public class Theme {
    
    public final static String fast="com.jtattoo.plaf.fast.FastLookAndFeel";
    public final static String graphite="com.jtattoo.plaf.graphite.GraphiteLookAndFeel";
    public final static String smart="com.jtattoo.plaf.smart.SmartLookAndFeel";
    public final static String acryl="com.jtattoo.plaf.acryl.AcrylLookAndFeel";
    public final static String aero="com.jtattoo.plaf.aero.AeroLookAndFeel";
    public final static String bernstein="com.jtattoo.plaf.bernstein.BernsteinLookAndFeel";
    public final static String aluminium="com.jtattoo.plaf.aluminium.AluminiumLookAndFeel";
    public final static String mcwin="com.jtattoo.plaf.mcwin.McWinLookAndFeel";
    public final static String mint="com.jtattoo.plaf.mint.MintLookAndFeel";
    public final static String hifi="com.jtattoo.plaf.hifi.HiFiLookAndFeel";
    public final static String noire="com.jtattoo.plaf.noire.NoireLookAndFeel";
    public final static String luna="com.jtattoo.plaf.luna.LunaLookAndFeel";
    
    Properties props = new Properties();

    public Properties getProps() {
        
        props.put("windowDecoration", "Default");
        props.put("logoString", "blungStudio"); 
        props.put("licenseKey", "blungStudio");
        return props;
    }
}
