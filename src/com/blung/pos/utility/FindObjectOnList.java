/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.utility;

import com.blung.pos.entity.bantuan.JmlBarang;
import com.blung.pos.entity.simple.Barang;
import com.blung.pos.entity.simple.Cabang;
import com.blung.pos.entity.simple.DetailPembelian;
import com.blung.pos.entity.simple.DetailPenjualan;
import com.blung.pos.entity.simple.Gudang;
import com.blung.pos.entity.simple.Karyawan;
import com.blung.pos.entity.simple.Kasir;
import com.blung.pos.entity.simple.Kategori;
import com.blung.pos.entity.simple.KategoriPelanggan;
import com.blung.pos.entity.simple.Pelanggan;
import com.blung.pos.entity.simple.Pembelian;
import com.blung.pos.entity.simple.Penjualan;
import com.blung.pos.entity.simple.Satuan;
import com.blung.pos.entity.simple.Supliyer;
import com.blung.pos.entity.simple.User;
import java.util.List;

/**
 *
 * @author lukman
 */
public class FindObjectOnList {
    
    public static Barang findBarangFromListByBarcode(List<Barang> list,String barcode){
        int i=0;
        while(!list.get(i).getIdBarangBarcode().equals(barcode)){
            i++;
        }
        return list.get(i);
    }
    
    public static Barang findBarangFromListById(List<Barang> list,Long id){
        int i=0;
        while(!list.get(i).getIdBarang().equals(id)){
            i++;
        }
        return list.get(i);
    }
    
    public static JmlBarang findJmlBarangFromListById(List<JmlBarang> list,Long id){
        int i=0;
        while(!list.get(i).getIdBarang().equals(id)){
            i++;
        }
        return list.get(i);
    }
    
    public static Cabang findCabangFromListByNama(List<Cabang> list,String nama){
        int i=0;
        while(!list.get(i).getNamaCabang().equals(nama)){
            i++;
        }
        return list.get(i);
    }
    
    public static Cabang findCabangFromListById(List<Cabang> list,Long nama){
        int i=0;
        while(!list.get(i).getIdCabang().equals(nama)){
            i++;
        }
        return list.get(i);
    }
    
    public static Gudang findGudangFromListById(List<Gudang> list,Long id){
        int i=0;
        while(!list.get(i).getIdGudang().equals(id)){
            i++;
        }
        return list.get(i);
    }
    
    public static Gudang findGudangFromListByNama(List<Gudang> list,String nama){
        int i=0;
        while(!list.get(i).getNamaGudang().equals(nama)){
            i++;
        }
        return list.get(i);
    }
    
    public static Karyawan findKaryawanFromListById(List<Karyawan> list,Long id){
        int i=0;
        while(!list.get(i).getIdKaryawan().equals(id)){
            i++;
        }
        return list.get(i);
    }
    
    public static Karyawan findKaryawanFromListByNOID(List<Karyawan> list,String id){
        int i=0;
        while(!list.get(i).getNoID().equals(id)){
            i++;
        }
        return list.get(i);
    }
    
    public static Kasir findKasirFromListById(List<Kasir> list,Long id){
        int i=0;
        while(!list.get(i).getIdKasir().equals(id)){
            i++;
        }
        return list.get(i);
    }
    
    public static Kategori findKategoriFromListById(List<Kategori> list,Long id){
        int i=0;
        while(!list.get(i).getIdKategori().equals(id)){
            i++;
        }
        return list.get(i);
    }
    
    public static Kategori findKategoriFromListByNama(List<Kategori> list,String nama){
        int i=0;
        while(!list.get(i).getNamaKategori().equals(nama)){
            i++;
        }
        return list.get(i);
    }
    
    public static KategoriPelanggan findKategoriPelangganiFromListById(List<KategoriPelanggan> list,Long id){
        int i=0;
        while(!list.get(i).getIdKategoriPelanggan().equals(id)){
            i++;
        }
        return list.get(i);
    }
    
    public static KategoriPelanggan findKategoriPelangganiFromListByKet(List<KategoriPelanggan> list,String ket){
        int i=0;
        while(!list.get(i).getKeterangan().equals(ket)){
            i++;
        }
        return list.get(i);
    }
    
    public static Pelanggan findPelangganFromListById(List<Pelanggan> list,Long id){
        int i=0;
        while(!list.get(i).getIdPelanggan().equals(id)){
            i++;
        }
        return list.get(i);
    }
    
    public static Pelanggan findPelangganFromListByNamaDanAlamat(List<Pelanggan> list,String nama,String alamat){
        int i=0;
        while(!list.get(i).getNama().equals(nama) && !list.get(i).getAlamat().equals(alamat)){
            i++;
        }
        return list.get(i);
    }
    
    public static Satuan findSatuanFromListById(List<Satuan> list,Long id){
        int i=0;
        while(!list.get(i).getIdSatuan().equals(id)){
            i++;
        }
        return list.get(i);
    }
    
    public static Satuan findSatuanFromListByNama(List<Satuan> list,String nama){
        int i=0;
        while(!list.get(i).getNamaSatuan().equals(nama)){
            i++;
        }
        return list.get(i);
    }
    
    public static Supliyer findSupliyerFromListById(List<Supliyer> list,Long id){
        int i=0;
        while(!list.get(i).getIdSupliyer().equals(id)){
            i++;
        }
        return list.get(i);
    }
    
    public static Supliyer findSupliyerFromListByNama(List<Supliyer> list,String nama){
        int i=0;
        while(!list.get(i).getNamaSupliyer().equals(nama)){
            i++;
        }
        return list.get(i);
    }
    
    public static User findUserFromListById(List<User> list,Long id){
        int i=0;
        while(!list.get(i).getIdUser().equals(id)){
            i++;
        }
        return list.get(i);
    }
    
    public static User findUserFromListByNamaAndUsername(List<User> list,String username,Long idk){
        int i=0;
        while(!list.get(i).getUsername().equals(username) && !list.get(i).getIdKaryawan().equals(idk)){
            i++;
        }
        return list.get(i);
    }
    
    public static Pembelian findPembelianFromListById(List<Pembelian> list,Long id){
        int i=0;
        while(!list.get(i).getIdPembelian().equals(id)){
            i++;
        }
        return list.get(i);
    }
    
    public static Penjualan findPenjualanFromListById(List<Penjualan> list,Long id){
        int i=0;
        while(!list.get(i).getIdPenjualan().equals(id)){
            i++;
        }
        return list.get(i);
    }
    
    public static Boolean cekIsExistPembelian(List<DetailPembelian> list,Barang b){
        int i=0;
        int banyak=list.size();
        while(!list.get(i).getIdBarang().equals(b.getIdBarang())){
            if (i==(banyak-1)) {
                i++;
                break;
            }
            i++;
        }
        if(i==banyak) return false;
        else return true;
    }
    
    public static int findIndexOnListDetPembelian(List<DetailPembelian> list,Barang b){
        int i=0;
        while(!list.get(i).getIdBarang().equals(b.getIdBarang())){
            i++;
        }
        return i;
    }
    
    public static Boolean cekIsExistPenjualan(List<DetailPenjualan> list,Barang b){
        int i=0;
        int banyak=list.size();
        while(!list.get(i).getIdBarang().equals(b.getIdBarang())){
            if (i==(banyak-1)) {
                i++;
                break;
            }
            i++;
        }
        if(i==banyak) return false;
        else return true;
    }
    
    public static int findIndexOnListDetPenjualan(List<DetailPenjualan> list,Barang b){
        int i=0;
        while(!list.get(i).getIdBarang().equals(b.getIdBarang())){
            i++;
        }
        return i;
    }
    
}
