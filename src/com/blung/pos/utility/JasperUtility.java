/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.utility;

import java.awt.Dimension;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.table.TableModel;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.swing.JRViewer;

/**
 *
 * @author Unknown
 */
public class JasperUtility {

    public static void loadReport(String resource, List list, Map<String, Object> parameter, JFrame frame){
        InputStream inputStream = null;
        try {
            inputStream = JRLoader.getResourceInputStream(resource);
            JRDataSource dataSource= new JRBeanCollectionDataSource(list);
            Map<String, Object> map= new HashMap<String, Object>();
            map.put(JRParameter.REPORT_DATA_SOURCE, dataSource);
            map.putAll(parameter);
            
            JasperPrint jasperPrint=JasperFillManager.fillReport(inputStream, map);
            JRViewer jRViewer=new JRViewer(jasperPrint);
            JDialog dialog = new JDialog(frame, true);
            dialog.setMinimumSize(new Dimension(800, 600));
            dialog.setPreferredSize(new Dimension(800, 600));
            dialog.setContentPane(jRViewer);
            dialog.setLocationRelativeTo(null);
            dialog.setVisible(true);
            dialog.setAlwaysOnTop(true);
        } catch (JRException ex) {
            System.out.println(ex);
        } finally {
            try {
                inputStream.close();
            } catch (IOException ex) {
                //Logger.getLogger(DialogReport.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static void loadReport(String resource, List list){
        InputStream inputStream = null;
        try {
            inputStream = JRLoader.getResourceInputStream(resource);
            JRDataSource dataSource= new JRBeanCollectionDataSource(list);
            Map<String, Object> map= new HashMap<String, Object>();
            map.put(JRParameter.REPORT_DATA_SOURCE, dataSource);
           // map.putAll(parameter);
            
            JasperPrint jasperPrint=JasperFillManager.fillReport(inputStream, map);
            JRViewer jRViewer=new JRViewer(jasperPrint);
            JDialog dialog = new JDialog();
            dialog.setMinimumSize(new Dimension(800, 600));
            dialog.setPreferredSize(new Dimension(800, 600));
            dialog.setContentPane(jRViewer);
            dialog.setLocationRelativeTo(null);
            dialog.setVisible(true);
            dialog.setAlwaysOnTop(true);
        } catch (JRException ex) {
            System.out.println(ex);
        } finally {
            try {
                inputStream.close();
            } catch (IOException ex) {
                //Logger.getLogger(DialogReport.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static void loadReport(String resource, List list, Map<String, Object> parameter){
        InputStream inputStream = null;
        try {
            inputStream = JRLoader.getResourceInputStream(resource);
            JRDataSource dataSource= new JRBeanCollectionDataSource(list);
            Map<String, Object> map= new HashMap<String, Object>();
            map.put(JRParameter.REPORT_DATA_SOURCE, dataSource);
            map.putAll(parameter);
            
            JasperPrint jasperPrint=JasperFillManager.fillReport(inputStream, map);
            JRViewer jRViewer=new JRViewer(jasperPrint);
            JDialog dialog = new JDialog();
            dialog.setMinimumSize(new Dimension(800, 600));
            dialog.setPreferredSize(new Dimension(800, 600));
            dialog.setContentPane(jRViewer);
            dialog.setLocationRelativeTo(null);
            dialog.setVisible(true);
            dialog.setAlwaysOnTop(true);
        } catch (JRException ex) {
            System.out.println(ex);
        } finally {
            try {
                inputStream.close();
            } catch (IOException ex) {
                //Logger.getLogger(DialogReport.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static void loadReport(String resource, TableModel model, Map<String, Object> parameter){
        InputStream inputStream = null;
        try {
            inputStream = JRLoader.getResourceInputStream(resource);
            JRDataSource dataSource= new JRTableModelDataSource(model);
            Map<String, Object> map= new HashMap<String, Object>();
            map.put(JRParameter.REPORT_DATA_SOURCE, dataSource);
            map.putAll(parameter);
            
            JasperPrint jasperPrint=JasperFillManager.fillReport(inputStream, map);
            JRViewer jRViewer=new JRViewer(jasperPrint);
            JDialog dialog = new JDialog();
            dialog.setMinimumSize(new Dimension(800, 600));
            dialog.setPreferredSize(new Dimension(800, 600));
            dialog.setContentPane(jRViewer);
            dialog.setLocationRelativeTo(null);
            dialog.setVisible(true);
            dialog.setAlwaysOnTop(true);
        } catch (JRException ex) {
            System.out.println(ex);
        } finally {
            try {
                inputStream.close();
            } catch (IOException ex) {
                //Logger.getLogger(DialogReport.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
