/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.utility;

import com.blung.pos.entity.simple.Barang;
import com.blung.pos.entity.simple.BarangHilang;
import com.blung.pos.entity.simple.Cabang;
import com.blung.pos.entity.simple.CicilanHutangPembelian;
import com.blung.pos.entity.simple.CicilanPiutangPenjualan;
import com.blung.pos.entity.simple.DetailPembelian;
import com.blung.pos.entity.simple.DetailPenjualan;
import com.blung.pos.entity.simple.DetailReturPembelian;
import com.blung.pos.entity.simple.DetailReturPenjualan;
import com.blung.pos.entity.simple.DetailServis;
import com.blung.pos.entity.simple.Gudang;
import com.blung.pos.entity.simple.Harga;
import com.blung.pos.entity.simple.HutangPembelian;
import com.blung.pos.entity.simple.Karyawan;
import com.blung.pos.entity.simple.Kasir;
import com.blung.pos.entity.simple.Kategori;
import com.blung.pos.entity.simple.KategoriPelanggan;
import com.blung.pos.entity.simple.Pelanggan;
import com.blung.pos.entity.simple.Pembelian;
import com.blung.pos.entity.simple.Penjualan;
import com.blung.pos.entity.simple.PiutangPenjualan;
import com.blung.pos.entity.simple.ReturPembelian;
import com.blung.pos.entity.simple.ReturPenjualan;
import com.blung.pos.entity.simple.Satuan;
import com.blung.pos.entity.simple.Servis;
import com.blung.pos.entity.simple.SetoranHarian;
import com.blung.pos.entity.simple.Supliyer;
import com.blung.pos.entity.simple.User;
import com.lukman.utility.converter.TextUtility;
import com.lukman.utility.converter.sql.ConvertBoolean;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 * @author user
 */
public class ConverterMapToObject {
    
    public static Cabang convertMapToCabang(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         
         Cabang c=new Cabang();
         
         c.setIdCabang(Long.valueOf(linkedHashMap.get("idCabang")+""));
         c.setNamaCabang(linkedHashMap.get("namaCabang")+"");
         return c;
         
     }
    
    public static List<Cabang> convertMapToCabangList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<Cabang> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToCabang(linkedHashMap));
         }
         
         return list;
     }
    
    public static Barang convertMapToBarang(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         
         Harga c=new Harga();
         c.setIdHarga(Long.valueOf(linkedHashMap.get("idHarga")+""));
         c.setIdBarang(Long.valueOf(linkedHashMap.get("idBarang")+""));
         c.setBatasDua(Long.valueOf(linkedHashMap.get("batasDua")+""));
         c.setBatasSatu(Long.valueOf(linkedHashMap.get("batasSatu")+""));
         c.setBatasTiga(Long.valueOf(linkedHashMap.get("batasTiga")+""));
         c.setDiskonPromo(Double.valueOf(linkedHashMap.get("diskonPromo")+""));
         c.setHargaBeliTerakhir(Double.valueOf(linkedHashMap.get("hargaBeliTerakhir")+""));
         c.setHargaBeliRata(Double.valueOf(linkedHashMap.get("hargaBeliRata")+""));
         c.setHargaDua(Double.valueOf(linkedHashMap.get("hargaDua")+""));
         c.setHargaEmpat(Double.valueOf(linkedHashMap.get("hargaEmpat")+""));
         c.setHargaSatu(Double.valueOf(linkedHashMap.get("hargaSatu")+""));
         c.setHargaTiga(Double.valueOf(linkedHashMap.get("hargaTiga")+""));
         c.setIdKategori(Long.valueOf(linkedHashMap.get("idKategori")+""));
         c.setIdSatuan(Long.valueOf(linkedHashMap.get("idSatuan")+""));
         c.setIdSupliyer(Long.valueOf(linkedHashMap.get("idSupliyer")+""));
         c.setIdCabang(Long.valueOf(linkedHashMap.get("idCabang")+""));
         Barang b=new Barang();
         b.setIdBarangBarcode(linkedHashMap.get("idBarangBarcode")+"");
         b.setNamaBarang(linkedHashMap.get("namaBarang")+"");
         b.setIdBarang(Long.valueOf(linkedHashMap.get("idBarang")+""));
         b.setHarga(c);
         return b;
         
     }
    
    public static List<Barang> convertMapToBarangList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<Barang> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToBarang(linkedHashMap));
         }
         
         return list;
     }
    
    public static Gudang convertMapToGudang(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         
         Gudang c=new Gudang();
         
         c.setIdGudang(Long.valueOf(linkedHashMap.get("idGudang")+""));
         c.setIdCabang(Long.valueOf(linkedHashMap.get("idCabang")+""));
         c.setNamaGudang(linkedHashMap.get("namaGudang")+"");
         return c;
         
     }
    
    public static List<Gudang> convertMapToGudangList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<Gudang> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToGudang(linkedHashMap));
         }
         
         return list;
     }
    
    public static Karyawan convertMapToKaryawan(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         
         Karyawan c=new Karyawan();
         
         c.setAlamat(linkedHashMap.get("alamat")+"");
         c.setIdCabang(Long.valueOf(linkedHashMap.get("idCabang")+""));
         c.setIdKaryawan(Long.valueOf(linkedHashMap.get("idKaryawan")+""));
         c.setNama(linkedHashMap.get("nama")+"");
         c.setNoID(linkedHashMap.get("noID")+"");
         c.setNoTlp(linkedHashMap.get("noTlp")+"");
         return c;
         
     }
    
    public static List<Karyawan> convertMapToKaryawanList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<Karyawan> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToKaryawan(linkedHashMap));
         }
         
         return list;
     }
    
    public static Kasir convertMapToKasir(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         
         Kasir c=new Kasir();
         
         c.setIdCabang(Long.valueOf(linkedHashMap.get("idCabang")+""));
         c.setIdKasir(Long.valueOf(linkedHashMap.get("idKasir")+""));
         c.setNamaKasir(linkedHashMap.get("namaKasir")+"");
         return c;
         
     }
    
    public static List<Kasir> convertMapToKasirList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<Kasir> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToKasir(linkedHashMap));
         }
         
         return list;
     }
    
    public static Kategori convertMapToKategori(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         
         Kategori c=new Kategori();
         
         c.setIdKategori(Long.valueOf(linkedHashMap.get("idKategori")+""));
         c.setNamaKategori(linkedHashMap.get("namaKategori")+"");
         return c;
         
     }
    
    public static List<Kategori> convertMapToKategoriList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<Kategori> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToKategori(linkedHashMap));
         }
         
         return list;
     }
    
    public static KategoriPelanggan convertMapToKategoriPelanggan(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         
         KategoriPelanggan c=new KategoriPelanggan();
         
         c.setIdKategoriPelanggan(Long.valueOf(linkedHashMap.get("idKategoriPelanggan")+""));
         c.setKeterangan(linkedHashMap.get("keterangan")+"");
         c.setPersenDiskon(Integer.parseInt(linkedHashMap.get("persenDiskon")+""));
         return c;
         
     }
    
    public static List<KategoriPelanggan> convertMapToKategoriPelangganList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<KategoriPelanggan> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToKategoriPelanggan(linkedHashMap));
         }
         
         return list;
     }
    
    public static Pelanggan convertMapToPelanggan(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         
         Pelanggan c=new Pelanggan();
         
         c.setIdPelanggan(Long.valueOf(linkedHashMap.get("idPelanggan")+""));
         c.setAlamat(linkedHashMap.get("alamat")+"");
         c.setNama(linkedHashMap.get("nama")+"");
         c.setNoID(linkedHashMap.get("noID")+"");
         c.setNoTlpPelanggan(linkedHashMap.get("noTlpPelanggan")+"");
         c.setIdKategoriPelanggan(Long.valueOf(linkedHashMap.get("idKategoriPelanggan")+""));
         return c;
         
     }
    
    public static List<Pelanggan> convertMapToPelangganList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<Pelanggan> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToPelanggan(linkedHashMap));
         }
         
         return list;
     }
    
    public static Satuan convertMapToSatuan(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         
         Satuan c=new Satuan();
         
         c.setIdSatuan(Long.valueOf(linkedHashMap.get("idSatuan")+""));
         c.setNamaSatuan(linkedHashMap.get("namaSatuan")+"");
         return c;
         
     }
    
    public static List<Satuan> convertMapToSatuanList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<Satuan> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToSatuan(linkedHashMap));
         }
         
         return list;
     }
    
    public static Supliyer convertMapToSupliyer(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         
         Supliyer c=new Supliyer();
         
         c.setAlamatSupliyer(linkedHashMap.get("alamatSupliyer")+"");
         c.setIdSupliyer(Long.valueOf(linkedHashMap.get("idSupliyer")+""));
         c.setNamaSupliyer(linkedHashMap.get("namaSupliyer")+"");
         c.setNoTlpSupliyer(linkedHashMap.get("noTlpSupliyer")+"");
         return c;
         
     }
    
    public static List<Supliyer> convertMapToSupliyerList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<Supliyer> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToSupliyer(linkedHashMap));
         }
         
         return list;
     }
    
    public static User convertMapToUser(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         
         User c=new User();
         
         c.setHakAkses(linkedHashMap.get("hakAkses")+"");
         c.setIdKaryawan(Long.valueOf(linkedHashMap.get("idKaryawan")+""));
         c.setIdKasir(Long.valueOf(linkedHashMap.get("idKasir")+""));
         c.setIdUser(Long.valueOf(linkedHashMap.get("idUser")+""));
         c.setLastLogin(TextUtility.stringToDate(linkedHashMap.get("lastLogin")+""));
         c.setPassword(linkedHashMap.get("password")+"");
         c.setStatusOnline(Boolean.valueOf(linkedHashMap.get("statusOnline")+""));
         c.setUsername(linkedHashMap.get("username")+"");
         return c;
         
     }
    
    public static List<User> convertMapToUserList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<User> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToUser(linkedHashMap));
         }
         
         return list;
     }
    
    public static HutangPembelian convertMapToHutangPembelian(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         
         HutangPembelian c=new HutangPembelian();
         
         c.setIdHutangPembelian(Long.valueOf(linkedHashMap.get("idHutangPembelian")+""));
         c.setIdPembelian(Long.valueOf(linkedHashMap.get("idPembelian")+""));
         c.setKeterangan(linkedHashMap.get("keterangan")+"");
         c.setStatusLunas(Boolean.valueOf(linkedHashMap.get("statusLunas")+""));
         c.setVerified(ConvertBoolean.dataToBool(linkedHashMap.get("verified")+""));
         return c;
         
     }
    
    public static List<HutangPembelian> convertMapToHutangPembelianList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<HutangPembelian> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToHutangPembelian(linkedHashMap));
         }
         
         return list;
     }
        
    public static PiutangPenjualan convertMapToPiutangPenjualan(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         
         PiutangPenjualan c=new PiutangPenjualan();
         
         c.setIdPelanggan(Long.valueOf(linkedHashMap.get("idPelanggan")+""));
         c.setIdPenjualan(Long.valueOf(linkedHashMap.get("idPenjualan")+""));
         c.setIdPiutangPenjualan(Long.valueOf(linkedHashMap.get("idPiutangPenjualan")+""));
         c.setKeterangan(linkedHashMap.get("keterangan")+"");
         c.setStatusLunas(Boolean.valueOf(linkedHashMap.get("statusLunas")+""));
         c.setVerified(ConvertBoolean.dataToBool(linkedHashMap.get("verified")+""));
         return c;
         
     }
    
    public static List<PiutangPenjualan> convertMapToPiutangPenjualanList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<PiutangPenjualan> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToPiutangPenjualan(linkedHashMap));
         }
         
         return list;
     }
    
    public static CicilanHutangPembelian convertMapToCicilanHutangPembelian(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         
         CicilanHutangPembelian c=new CicilanHutangPembelian();
         
         c.setIdCicilanHutangPembelian(Long.valueOf(linkedHashMap.get("idCicilanHutangPembelian")+""));
         c.setIdHutangPembelian(Long.valueOf(linkedHashMap.get("idHutangPembelian")+""));
         c.setIdUser(Long.valueOf(linkedHashMap.get("idUser")+""));
         c.setJumlahBayar(Double.valueOf(linkedHashMap.get("jumlahBayar")+""));
         c.setKeterangan(linkedHashMap.get("keterangan")+"");
         c.setTanggalBayar(TextUtility.stringToDate(linkedHashMap.get("tanggalBayar")+""));
         return c;
         
     }
    
    public static List<CicilanHutangPembelian> convertMapToCicilanHutangPembelianList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<CicilanHutangPembelian> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToCicilanHutangPembelian(linkedHashMap));
         }
         
         return list;
     }
    
    public static CicilanPiutangPenjualan convertMapToCicilanPiutangPenjualan(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         
         CicilanPiutangPenjualan c=new CicilanPiutangPenjualan();
         
         c.setIdCicilanPiutang(Long.valueOf(linkedHashMap.get("idCicilanPiutang")+""));
         c.setIdPiutangPenjualan(Long.valueOf(linkedHashMap.get("idPiutangPenjualan")+""));
         c.setIdUser(Long.valueOf(linkedHashMap.get("idUser")+""));
         c.setJumlahBayar(Double.valueOf(linkedHashMap.get("jumlahBayar")+""));
         c.setKeterangan(linkedHashMap.get("keterangan")+"");
         c.setTanggalBayar(TextUtility.stringToDate(linkedHashMap.get("tanggalBayar")+""));
         return c;
         
     }
    
    public static List<CicilanPiutangPenjualan> convertMapToCicilanPiutangList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<CicilanPiutangPenjualan> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToCicilanPiutangPenjualan(linkedHashMap));
         }
         
         return list;
     }
        
    public static Pembelian convertMapToPembelian(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         
         Pembelian c=new Pembelian();
         
         c.setIdKasir(Long.valueOf(linkedHashMap.get("idKasir")+""));
         c.setIdPembelian(Long.valueOf(linkedHashMap.get("idPembelian")+""));
         c.setIdSupliyer(Long.valueOf(linkedHashMap.get("idSupliyer")+""));
         c.setIdUser(Long.valueOf(linkedHashMap.get("idUser")+""));
         c.setKeterangan(linkedHashMap.get("keterangan")+"");
         c.setNoFaktur(linkedHashMap.get("noFaktur")+"");
         c.setStatusPindahGudang(ConvertBoolean.dataToBool(linkedHashMap.get("statusPindahGudang")+""));
         c.setStatusHutang(ConvertBoolean.dataToBool(linkedHashMap.get("statusHutang")+""));
         c.setTanggalBeli(TextUtility.stringToDate(linkedHashMap.get("tanggalBeli")+""));
         c.setTotalHarga(Double.valueOf(linkedHashMap.get("totalHarga")+""));
         c.setVerified(ConvertBoolean.dataToBool(linkedHashMap.get("verified")+""));
         return c;
         
     }
    
    public static List<Pembelian> convertMapToPembelianList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<Pembelian> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToPembelian(linkedHashMap));
         }
         
         return list;
     }
    
    public static Penjualan convertMapToPenjualan(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         
         Penjualan p = new Penjualan();
         p.setDiskonPelanggan(Double.valueOf(linkedHashMap.get("diskonPelanggan")+""));
         p.setIdKasir(Long.valueOf(linkedHashMap.get("idKasir")+""));
         p.setIdPelanggan(Long.valueOf(linkedHashMap.get("idPelanggan")+""));
         p.setIdPenjualan(Long.valueOf(linkedHashMap.get("idPenjualan")+""));
         p.setIdUser(Long.valueOf(linkedHashMap.get("idUser")+""));
         p.setPotonganLangsung(Double.valueOf(linkedHashMap.get("potonganLangsung")+""));
         p.setStatusDikirim(ConvertBoolean.dataToBool(linkedHashMap.get("statusDikirim")+""));
         p.setStatusJual(ConvertBoolean.dataToBool(linkedHashMap.get("statusJual")+""));
         p.setTanggalJual(TextUtility.stringToDate(linkedHashMap.get("tanggalJual")+""));
         p.setTotalHarga(Double.valueOf(linkedHashMap.get("totalHarga")+""));
         p.setVerified(ConvertBoolean.dataToBool(linkedHashMap.get("verified")+""));
         
         return p;
         
     }
    
    public static List<Penjualan> convertMapToPenjualanList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<Penjualan> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToPenjualan(linkedHashMap));
         }
         
         return list;
     }
    
    public static DetailPenjualan convertMapToDetailPenjualan(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         
         DetailPenjualan dp = new DetailPenjualan();
         dp.setIdDetailPenjualan(Long.valueOf(linkedHashMap.get("idDetailPenjualan")+""));
         dp.setDiskonPersen(Long.valueOf(linkedHashMap.get("diskonPersen")+""));
         dp.setDiskonRupiah(Double.valueOf(linkedHashMap.get("diskonRupiah")+""));
         dp.setHarga(Double.valueOf(linkedHashMap.get("harga")+""));
         dp.setJumlah(Integer.parseInt(linkedHashMap.get("jumlah")+""));
         dp.setKeuntunganSatuan(Double.valueOf(linkedHashMap.get("keuntunganSatuan")+""));
         dp.setSubTotal(Double.valueOf(linkedHashMap.get("subTotal")+""));
         dp.setIdBarang(Long.valueOf(linkedHashMap.get("idBarang")+""));
         dp.setIdPenjualan(Long.valueOf(linkedHashMap.get("idPenjualan")+""));
         
         return dp;
    }
    
    public static List<DetailPenjualan> convertMapToDetailPenjualanList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<DetailPenjualan> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToDetailPenjualan(linkedHashMap));
         }
         return list;
    }
    
    public static DetailPembelian convertMapToDetailPembelian(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         
         DetailPembelian dp = new DetailPembelian();
         dp.setIdDetailPembelian(Long.valueOf(linkedHashMap.get("idDetailPembelian")+""));
         dp.setJumlah(Integer.valueOf(linkedHashMap.get("jumlah")+""));
         dp.setHarga(Double.valueOf(linkedHashMap.get("harga")+""));
         dp.setSubTotal(Double.valueOf(linkedHashMap.get("subTotal")+""));
         dp.setIdBarang(Long.valueOf(linkedHashMap.get("idBarang")+""));
         dp.setIdPembelian(Long.valueOf(linkedHashMap.get("idPembelian")+""));
         
         return dp;
    }
    
    public static List<DetailPembelian> convertMapToDetailPembelianList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<DetailPembelian> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToDetailPembelian(linkedHashMap));
         }
         return list;
    }
    
    public static BarangHilang convertMapToBarangHilang(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         
         BarangHilang b = new BarangHilang();
         b.setIdBarangHilang(Long.valueOf(linkedHashMap.get("idBarangHilang")+""));
         b.setIdBarang(Long.valueOf(linkedHashMap.get("idBarang")+""));
         b.setIdCabang(Long.valueOf(linkedHashMap.get("idCabang")+""));
         b.setIdUser(Long.valueOf(linkedHashMap.get("idUser")+""));
         b.setJumlah(Long.valueOf(linkedHashMap.get("jumlah")+""));
         b.setHargaRata(Double.valueOf(linkedHashMap.get("hargaRata")+""));
         b.setTanggalHilang(TextUtility.stringToDate(linkedHashMap.get("tanggalHilang")+""));
         b.setVerified(ConvertBoolean.dataToBool(linkedHashMap.get("verified")+""));         
         return b;
    }
    
    public static List<BarangHilang> convertMapToBarangHilangList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<BarangHilang> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToBarangHilang(linkedHashMap));
         }
         return list;
    }
    
    public static com.blung.pos.entity.bantuan.JmlBarang convertMapToJmlBarangBantu(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         
         com.blung.pos.entity.bantuan.JmlBarang b=new com.blung.pos.entity.bantuan.JmlBarang();
         b.setIdBarang(Long.valueOf(linkedHashMap.get("idBarang")+""));
         b.setNamaBarang(String.valueOf(linkedHashMap.get("namabarang")));
         b.setJumlahBarangCabang(Long.valueOf(linkedHashMap.get("jmlSisaCabang")+""));
         List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) linkedHashMap.get("jmlGudang");
         b.setListGudang(convertMapToGudangBantuList(linkedHashMaps));
         return b;
    }
    
    public static List<com.blung.pos.entity.bantuan.JmlBarang> convertMapTojmlbarangBantuList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<com.blung.pos.entity.bantuan.JmlBarang> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToJmlBarangBantu(linkedHashMap));
         }
         return list;
    }
    
    public static com.blung.pos.entity.bantuan.JmlGudang convertMapToGudangBantu(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         
         com.blung.pos.entity.bantuan.JmlGudang b=new com.blung.pos.entity.bantuan.JmlGudang();
         b.setJumlahBarangGudang(Long.valueOf(linkedHashMap.get("jmlSisaGudang")+""));
         b.setNamaGudang(String.valueOf(linkedHashMap.get("namaGudang")));
         return b;
    }
    
    public static List<com.blung.pos.entity.bantuan.JmlGudang> convertMapToGudangBantuList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<com.blung.pos.entity.bantuan.JmlGudang> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToGudangBantu(linkedHashMap));
         }
         return list;
    }
    
    public static SetoranHarian convertMapToSetoranHarian(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         
         SetoranHarian b = new SetoranHarian();
         b.setCekBarangHilang(Double.valueOf(linkedHashMap.get("cekBarangHilang")+""));
         b.setCekCicilanHutang(Double.valueOf(linkedHashMap.get("cekCicilanHutang")+""));
         b.setCekCicilanPiutang(Double.valueOf(linkedHashMap.get("cekCicilanPiutang")+""));
         b.setCekHutang(Double.valueOf(linkedHashMap.get("cekHutang")+""));
         b.setCekPembelian(Double.valueOf(linkedHashMap.get("cekPembelian")+""));
         b.setCekPenjualan(Double.valueOf(linkedHashMap.get("cekPenjualan")+""));
         b.setCekPiutang(Double.valueOf(linkedHashMap.get("cekPiutang")+""));
         b.setCekReturPembelian(Double.valueOf(linkedHashMap.get("cekReturPembelian(")+""));
         b.setCekReturPenjualan(Double.valueOf(linkedHashMap.get("cekReturPenjualan")+""));
         
         return b;
    }
    
    public static List<SetoranHarian> convertMapToSetoranHarianList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<SetoranHarian> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToSetoranHarian(linkedHashMap));
         }
         return list;
    }
    
    public static ReturPembelian convertMapToReturPembelian(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         ReturPembelian r=new ReturPembelian();
         r.setIdKasir(Long.valueOf(linkedHashMap.get("idKasir")+""));
         r.setIdPembelian(Long.valueOf(linkedHashMap.get("idPembelian")+""));
         r.setIdReturPembelian(Long.valueOf(linkedHashMap.get("idReturPembelian")+""));
         r.setIdUser(Long.valueOf(linkedHashMap.get("idUser")+""));
         r.setTanggal(TextUtility.stringToDate(linkedHashMap.get("tanggal")+""));
         return r;
     }
    
    public static List<ReturPembelian> convertMapToReturPembeliansList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<ReturPembelian> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToReturPembelian(linkedHashMap));
         }
         return list;
    }
    
    public static ReturPenjualan convertMapToReturPenjualan(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         ReturPenjualan r=new ReturPenjualan();
         r.setIdKasir(Long.valueOf(linkedHashMap.get("idKasir")+""));
         r.setIdPenjualan(Long.valueOf(linkedHashMap.get("idPenjualan")+""));
         r.setIdReturPenjualan(Long.valueOf(linkedHashMap.get("idReturPenjualan")+""));
         r.setIdUser(Long.valueOf(linkedHashMap.get("idUser")+""));
         r.setTanggal(TextUtility.stringToDate(linkedHashMap.get("tanggal")+""));
         return r;
     }
    
    public static List<ReturPenjualan> convertMapToReturPenjualanList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<ReturPenjualan> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToReturPenjualan(linkedHashMap));
         }
         return list;
    }
    
    public static DetailReturPembelian convertMapToDetailReturPembelian(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         DetailReturPembelian r=new DetailReturPembelian();
         r.setIdDetailReturPembelian(Long.valueOf(linkedHashMap.get("idDetailReturPembelian")+""));
         r.setIdBarang(Long.valueOf(linkedHashMap.get("idBarang")+""));
         r.setJumlah(Integer.parseInt(linkedHashMap.get("jumlah")+""));
         r.setHarga(Double.valueOf(linkedHashMap.get("harga")+""));
         r.setKeterangan((linkedHashMap.get("keterangan")+""));
         r.setIdReturPembelian(Long.valueOf(linkedHashMap.get("idReturPembelian")+""));
         return r;
     }
    
    public static List<DetailReturPembelian> convertMapToDetailReturPembelianList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<DetailReturPembelian> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToDetailReturPembelian(linkedHashMap));
         }
         return list;
    }
    
    public static DetailReturPenjualan convertMapToDetailReturPenjualan(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         DetailReturPenjualan r=new DetailReturPenjualan();
         r.setIdDetailReturPenjualan(Long.valueOf(linkedHashMap.get("idDetailReturPenjualan")+""));
         r.setIdBarang(Long.valueOf(linkedHashMap.get("idBarang")+""));
         r.setJumlah(Integer.parseInt(linkedHashMap.get("jumlah")+""));
         r.setHarga(Double.valueOf(linkedHashMap.get("harga")+""));
         r.setKeterangan((linkedHashMap.get("keterangan")+""));
         r.setIdReturPenjualan(Long.valueOf(linkedHashMap.get("idReturPenjualan")+""));
         return r;
     }
    
    public static List<DetailReturPenjualan> convertMapToDetailReturPenjualanList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<DetailReturPenjualan> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToDetailReturPenjualan(linkedHashMap));
         }
         return list;
    }
    
    public static Servis convertMapToServis(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         Servis r=new Servis();
         r.setIdServis(Long.valueOf(linkedHashMap.get("idServis")+""));
         r.setIdKaryawan(Long.valueOf(linkedHashMap.get("idKaryawan")+""));
         r.setIdCabang(Long.valueOf(linkedHashMap.get("idCabang")+""));
         r.setNamaDevice(linkedHashMap.get("namaDevice")+"");
         r.setNamaPelanggan(linkedHashMap.get("namaPelanggan")+"");
         r.setTanggalMasuk(TextUtility.stringToDate(linkedHashMap.get("tanggalMasuk")+""));
         r.setTanggalJadi(TextUtility.stringToDate(linkedHashMap.get("tanggalJadi")+""));
         r.setKeluhan(linkedHashMap.get("keluhan")+"");
         r.setStatus(ConvertBoolean.dataToBool(linkedHashMap.get("status")+""));
         return r;
     }
    
    public static List<Servis> convertMapToServisList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<Servis> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToServis(linkedHashMap));
         }
         return list;
    }
    
    public static DetailServis convertMapToDetailServis(LinkedHashMap linkedHashMap){
         if(linkedHashMap==null)return null;
         DetailServis r=new DetailServis();
         r.setIdDetailServis(Long.valueOf(linkedHashMap.get("idDetailServis")+""));
         r.setIdServis(Long.valueOf(linkedHashMap.get("idServis")+""));
         r.setNamaServis(linkedHashMap.get("namaServis")+"");
         r.setHarga(Double.valueOf(linkedHashMap.get("harga")+""));
         return r;
     }
    
    public static List<DetailServis> convertMapToDetailServisList(List<LinkedHashMap> linkedHashMaps){
         if(linkedHashMaps==null)return null;
         
         List<DetailServis> list=new ArrayList<>();
         for(LinkedHashMap linkedHashMap:linkedHashMaps){
             list.add(convertMapToDetailServis(linkedHashMap));
         }
         return list;
    }
}
