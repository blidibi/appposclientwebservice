/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.utility;

/**
 *
 * @author lukman
 */
public class ServiceUtility {
    public final static String getBarangByID ="barang_c/barangById?id={id}&simple=yes";
    public final static String getBarangByIDBarcodeAndCabang ="barang_c/barangByIdBarcodeAndCabang?idcb={idcb}&idbc={idbc}&simple=yes";
    public final static String getBarangAll ="barang_c/barangAll?simple=yes";
    public final static String getBarangAllByCabang ="barang_c/barangAllByCabang?id={id}&simple=yes";
    public final static String jumlahBarangByIDandCabang ="barang_c/jumlahBarangByIDandCabang?idBarang={idb}&idCabang={idc}";
    public final static String alljumlahBarangByCabang ="barang_c/jumlahBarangAllByCabang?idCabang={idc}";
    
    public final static String saveBarang ="barang_c/saveBarang";
    public final static String updateBarang = "barang_c/updateBarang";
    public final static String deleteBarang = "barang_c/deleteBarang";
    
    public final static String getBarangHilangAll = "baranghilang_c/barangHilangAll";
    public final static String getBarangHilangById = "baranghilang_c/barangHilangById?id={id}&simple=yes";
    public final static String getBarangHilangByCabangAndDate = "baranghilang_c/barangHilangByCabangAndDate?id={id}&datestart={start}&dateend={end}&simple=yes";
    public final static String simpanBarangHilang = "baranghilang_c/simpanBarangHilang";
    
    public final static String getCabangByID ="cabang_c/cabangById?id={id}&simple=yes";
    public final static String getCabangAll ="cabang_c/cabangAll?simple=yes";
    public final static String saveCabang = "cabang_c/saveCabang";
    public final static String updateCabang = "cabang_c/updateCabang";
    public final static String deleteCabang = "cabang_c/deleteCabang";
    
    public final static String getGudangByID ="gudang_c/gudangById?id={id}&simple=yes";
    public final static String getGudangByCabang ="gudang_c/gudangByCabang?id={id}&simple=yes";
    public final static String getGudangAll ="gudang_c/gudangAll?simple=yes";
    public final static String saveGudang = "gudang_c/saveGudang";
    public final static String updateGudang = "gudang_c/updateGudang";
    public final static String deleteGudang = "gudang_c/deleteGudang";
    
    public final static String saveKaryawan = "karyawan_c/saveKaryawan";
    public final static String getKaryawanByID ="karyawan_c/KaryawanById?id={id}&simple=yes";
    public final static String getKaryawanAll ="karyawan_c/KaryawanAll?simple=yes";
    public final static String getKaryawanAllByCabang ="karyawan_c/karyawanByCabang?id={id}simple=yes";
    public final static String updateKaryawan = "karyawan_c/updateKaryawan";
    public final static String deleteKaryawan = "karyawan_c/deleteKaryawan";
    
    public final static String saveKasir = "kasir_c/savekasir";
    public final static String getKasirByID ="kasir_c/KasirById?id={id}&simple=yes";
    public final static String getKasirAll ="kasir_c/KasirAll?simple=yes";
    public final static String getKasirByCabang ="kasir_c/kasirByCabang?id={id}&simple=yes";
    public final static String updateKasir = "kasir_c/updateKasir";
    public final static String deleteKasir = "kasir_c/deleteKasir";
    
    public final static String saveKategori = "kategori_c/saveKategori";
    public final static String getKategoriByID ="kategori_c/KategoriById?id={id}&simple=yes";
    public final static String getKategoriAll ="kategori_c/kategoriAll?simple=yes";
    public final static String updateKategori = "kategori_c/updateKategori";
    public final static String deleteKategori = "kategori_c/deleteKategori";
    
    public final static String saveKategoriPelanggan = "kategoripelanggan_c/saveKategoriPelanggan";
    public final static String getKategoriPelangganByID ="kategoripelanggan_c/KategoriPelangganById?id={id}&simple=yes";
    public final static String getKategoriPelangganAll ="kategoripelanggan_c/KategoriPelangganAll?simple=yes";
    public final static String updateKategoriPelanggan = "kategoripelanggan_c/updateKategoriPelanggan";
    public final static String deleteKategoriPelanggan = "kategoripelanggan_c/deleteKategoriPelanggan";
    
    public final static String savePelanggan = "pelanggan_c/savepelanggan";
    public final static String getPelangganByID ="pelanggan_c/PelangganById?id={id}&simple=yes";
    public final static String getPelangganAll ="pelanggan_c/PelangganAll?simple=yes";
    public final static String updatePelanggan = "pelanggan_c/updatePelanggan";
    public final static String deletePelanggan = "pelanggan_c/deletePelanggan";
    
    public final static String saveSatuan = "satuan_c/saveSatuan";
    public final static String getSatuanByID ="satuan_c/SatuanById?id={id}&simple=yes";
    public final static String getSatuanAll ="satuan_c/SatuanAll?simple=yes";
    public final static String updateSatuan = "satuan_c/updateSatuan";
    public final static String deleteSatuan = "satuan_c/deleteSatuan";
    
    public final static String saveSettingAplikasi = "settingaplikasi_c/saveSettingAplikasi";
    public final static String getSettingAplikasiByID ="settingaplikasi_c/SettingAplikasiById?id={id}&simple=yes";
    public final static String updateSettingAplikasi = "settingaplikasi_c/updateSettingAplikasi";
    public final static String deleteSettingAplikasi = "settingaplikasi_c/deleteSettingAplikasi";
    
    public final static String saveSupliyer = "supliyer_c/saveSupliyer";
    public final static String getSupliyerByID ="supliyer_c/SupliyerById?id={id}&simple=yes";
    public final static String getSupliyerAll ="supliyer_c/SupliyerAll?simple=yes";
    public final static String updateSupliyer = "supliyer_c/updateSupliyer";
    public final static String deleteSupliyer = "supliyer_c/deleteSupliyer";
    
    public final static String saveUser = "user_c/saveUser";
    public final static String saveLogin = "user_c/saveLogin";
    public final static String getUserByID ="user_c/UserById?id={id}&simple=yes";//public final static String getUserByUserAndPass ="user_c/userByUsernameAndPassword?user={user}&pass={ps}";
    public final static String getUserByUserAndPass ="user_c/userByUsernameAndPassword?user={us}&pass={ps}&simple=yes";
    public final static String getUserAll ="user_c/UserAll?simple=yes";
    public final static String getUserByCabang ="user_c/userAllByCabang?id={id}";
    public final static String updateUser = "user_c/updateUser";
    public final static String deleteUser = "user_c/deleteUser";
    
    public final static String saveHutangPembelian = "hutangpembelian_c/saveHutangPembelian";
    public final static String getHutangPembelianByID ="hutangpembelian_c/HutangPembelianById?id={id}&simple=yes";
    public final static String getHutangPembelian ="hutangpembelian_c/HutangPembelian?start={start}&end={end}&simple=yes";
    public final static String getHutangPembelianAll ="hutangpembelian_c/HutangPembelianAll?simple=yes";
    public final static String getHutangPembelianByCabang ="hutangpembelian_c/HutangPembelianByCabang?id={id}&simple=yes";
    public final static String getHutangpembelianByCabangAndDate ="hutangpembelian_c/hutangpembelianByCabangAndDate?id={id}&datestart={start}&dateend={end}&simple=yes";
    public final static String getHutangPembelianBySupliyer ="hutangpembelian_c/HutangPembelianBySupliyer?id={id}&simple=yes";
    public final static String getHutangPembelianByCabangDanBySupliyer ="hutangpembelian_c/HutangPembelianByCabangDanBySupliyer?idCabang={idCabang}&idSupliyer={idSupliyer}&simple=yes";
    
    public final static String savePiutangPenjualan = "piutangpenjualan_c/savePiutangPenjualan";
    public final static String getPiutangPenjualanAll ="piutangpenjualan_c/piutangPenjualanAll?datestart={datestart}&dateend={dateend}&simple=yes";
    public final static String getPiutangPenjualanById ="piutangpenjualan_c/piutangPenjualanById?idPiutangPenjualan={idPiutangPenjualan}&simple=yes";
    public final static String getPiutangPenjualanByCabang ="piutangpenjualan_c/getPiutangPenjualanByCabang?idCabang={idCabang}&datestart={datestart}&dateend={dateend}&simple=yes";
    public final static String getPiutangPenjualanByPelanggan ="piutangpenjualan_c/getPiutangPenjualanByPelanggan?idPelanggan={idPelanggan}&start={start}&end={end}&simple=yes";
    public final static String getPiutangPenjualanByCabangDanPelanggan ="piutangpenjualan_c/getPiutangPenjualanByCabangDanPelanggan?idCabang={idCabang}&idPelanggan={idPelanggan}&start={start}&end={end}&simple=yes";
    
    public final static String saveCicilanHutangPembelian = "cicilanhutangpembelian_c/saveCicilanHutang";
    public final static String getCicilanHutangPembelianById = "cicilanhutangpembelian_c/getCicilanHutangPembelian?idCicilanHutangPembelian={idCicilanHutangPembelian}";
    public final static String getCicilanHutangPembelianAll = "cicilanhutangpembelian_c/CicilanHutangPembelianAll?simple=yes";
    public final static String getCicilanHutangPembelianByHutangPembelian = "cicilanhutangpembelian_c/getCicilanHutangPembelianByHutangPembelian?idPembelian={idPembelian}&simple=yes";
    public final static String getcicilanHutangPembelianByCabangAndDate = "cicilanhutangpembelian_c/cicilanHutangPembelianByCabangAndDate?id={id}&datestart={start}&dateend={end}&simple=yes";
    
    public final static String saveCicilanPiutang = "cicilanpiutangpenjualan_c/saveCicilanPiutang";
    public final static String getCicilanPiutangPenjualanByID = "cicilanpiutangpenjualan_c/cicilanPiutangPenjualanById?id={id}simple=yes";
    public final static String getCicilanPiutangPenjualanAll = "cicilanpiutangpenjualan_c/CicilanPiutangPenjualanAll?simple=yes";
    public final static String getCicilanPiutangPenjualanByPiutangPenjualan = "cicilanpiutangpenjualan_c/getCicilanPiutangPenjualanByPiutangPenjualan?id={id}&simple=yes";
    public final static String getcicilanPiutangPenjualanByCabangAndDate = "cicilanpiutangpenjualan_c/cicilanPiutangPenjualanByCabangAndDate?id={id}&datestart={start}&dateend={end}&simple=yes";
    
    public static final String savePenjualan="penjualan_c/simpanPenjualan";
    public static final String savePembelian="pembelian_c/simpanPembelian";
    public static final String updatePembelian="pembelian_c/updatePembelian";
    public static final String saveTransferGudang="transfergudang_c/simpanTransferGudang";
    public static final String saveTransfergudangkecabang="transfergudangkecabang_c/saveTransfergudangkecabang";
    public static final String getPembelianAll="pembelian_c/pembelianAll?datestart={start}&dateend={end}&simple=yes";
    public static final String getPembelianById="pembelian_c/pembelianById?id={id}";
    public static final String getPembelianBySupliyer="pembelian_c/pembelianBySupliyer?datestart={start}&dateend={end}&id={id}&simple=yes";
    public static final String getPembelianByCabang="pembelian_c/pembelianByCabang?datestart={datestart}&dateend={dateend}&id={id}&simple=yes";
    public static final String getpembelianBySupliyerAndCabang="pembelian_c/pembelianBySupliyerAndCabang?datestart={start}&dateend={end}&idCabang={idC}&idSupliyer={idS}&simple=yes";
    public static final String getpembelianBelumPindahGudangByCabang="pembelian_c/pembelianBelumPindahGudangByCabang?id={id}";
    public static final String getpembelianBelumRekapByCabangAndUser="pembelian_c/pembelianBelumRekapByCabangAndUser?idCabang={idc}&idUser={idu}";
    public static final String getPenjualanAll="penjualan_c/penjualanAll?datestart={start}&dateend={end}&simple=yes";
    public static final String getPenjualanByID="penjualan_c/penjualanById?id={id}&simple=yes";
    public static final String getPenjualanByCabang="penjualan_c/penjualanByCabang?id={id}&datestart={start}&dateend={end}&simple=yes";
    public static final String getLastPenjualanByCabangDanKasir="penjualan_c/lastPenjualanByCabangAndKasir?idc={idc}&idk={idk}&simple=yes";
    public static final String getPenjualanBelumRekapByCabangAndUser="penjualan_c/penjualanBelumRekapByCabangAndUser?idCabang={idc}&idUser={idu}";
    
    public static final String getdetailPenjualanByCabang="detailpenjualan_c/detailPenjualanByCabang?datestart={start}&dateend={end}&id={id}&simple=yes";
    public static final String getdetailPenjualanByPenjualan="detailpenjualan_c/detailPenjualanByPenjualan?id={id}&simple=yes";
    public static final String getdetailPenjualanById="detailpenjualan_c/detailPenjualanById?id&simple=yes";
    
    public static final String getdetailPembelianById="detailpembelian_c/detailPembelianById?id&simple=yes";
    public static final String getdetailPembelianByPembelian="detailpembelian_c/detailPembelianByPembelian?id={id}&simple=yes";
    public static final String getdetailPembelianByCabang="detailpembelian_c/detailPembelianByCabang?datestart={start}&dateend={end}&id={id}&simple=yes";
    
    public static final String saveReturpembelian="returpembelian_c/saveReturpembelian";
    public static final String getreturPembelianById="returpembelian_c/returPembelianById?id={id}";
    public static final String getreturPembelianByCabangAndDate="returpembelian_c/returPembelianByIdByCabangAndDate?id={id}&datestart={start}&dateend{end}";
    public static final String getdetailReturPembelianByReturPembelian="detailreturpembelian_c/detailReturPembelianByReturPembelian?id={id}";
    public static final String getdetailReturPembelianByCabangAndDate="detailreturpembelian_c/detailReturPembelianByCabangAndDate?id={id}&datestart={start}&dateend{end}";
    
    
    public static final String saveReturpenjualan="returpenjualan_c/saveReturpenjualan";
    public static final String getreturPenjualanById="returpenjualan_c/returPenjualanById?id={id}";
    public static final String getreturPenjualanByCabangAndDate="returpenjualan_c/returPenjualanByCabangAndDate?id={id}&datestart={start}&dateend{end}";
    public static final String getdetailReturPenjualanByReturPenjualan="detailreturpenjualan_c/detailReturPenjualanByReturPenjualan?id={id}";
    public static final String getdetailReturPenjualanByCabangAndDate="detailreturpenjualan_c/detailReturPenjualanByCabangAndDate?id={id}&datestart={start}&dateend{end}";
    
    //---------------------------------------------------------------------------------
    //tambahan perdana dan servis
    public static final String servisSudahByCabangAndDate="servis_c/servisSudahByCabangAndDate?id={id}&datestart={start}&dateend={end}";
    public static final String servisBelumByCabangAndDate="servis_c/servisBelumByCabangAndDate?id={id}&datestart={start}&dateend={end}";
    public static final String saveServis="servis_c/saveServis";
    public static final String saveServisORM="servis_c/saveServisORM";
    public static final String updateServis="servis_c/updateServis";
    
    public static final String detailservisByServis="detailservis_c/detailservisByServis?id={id}";
    public static final String detailservisByCabangAndDate="detailservis_c/detailservisByCabangAndDate?id={id}&datestart={start}&dateend={end}";
    public static final String savedetailservis="detailservis_c/savedetailservis";
    public static final String updatedetailservis="detailservis_c/updatedetailservis";
    
}
