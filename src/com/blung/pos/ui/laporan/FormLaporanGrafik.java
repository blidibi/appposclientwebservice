/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.laporan;

import com.blung.pos.entity.simple.Pembelian;
import com.blung.pos.entity.simple.Penjualan;
import com.blung.pos.sorter.SorterGrafik;
import com.blung.pos.sorter.bantuGrafikDataSet;
import com.blung.pos.ui.main.mainFrame;
import com.lukman.utility.converter.TextUtility;
import com.toedter.calendar.JCalendar;
import java.awt.BorderLayout;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author lukman
 */
public class FormLaporanGrafik extends javax.swing.JInternalFrame {

    
    private List<Pembelian> listPembelian = new ArrayList<>();
    private List<Penjualan> listPenjualan = new ArrayList<>();
    private List<bantuGrafikDataSet> list = new ArrayList<>();
    public FormLaporanGrafik() {
        initComponents();
        tglAwal.setDate(new Date());
        tglAkhir.setDate(new Date());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        panelChart = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        comboBox = new javax.swing.JComboBox();
        tglAwal = new com.toedter.calendar.JDateChooser();
        tglAkhir = new com.toedter.calendar.JDateChooser();
        jButton2 = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setFrameIcon(null);
        try {
            setSelected(true);
        } catch (java.beans.PropertyVetoException e1) {
            e1.printStackTrace();
        }
        setVisible(true);

        panelChart.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout panelChartLayout = new javax.swing.GroupLayout(panelChart);
        panelChart.setLayout(panelChartLayout);
        panelChartLayout.setHorizontalGroup(
            panelChartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 692, Short.MAX_VALUE)
        );
        panelChartLayout.setVerticalGroup(
            panelChartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 422, Short.MAX_VALUE)
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        comboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "LINE", "LINE 3D", "BAR", "BAR 3D" }));

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("com/blung/pos/ui/laporan/Bundle"); // NOI18N
        jButton2.setText(bundle.getString("FormLaporanGrafik.jButton2.text_1")); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(comboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62)
                .addComponent(tglAwal, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(tglAkhir, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton2)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(comboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(tglAkhir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(3, 3, 3))
                        .addComponent(tglAwal, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(30, 30, 30))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelChart, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelChart, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        lihatData();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void lihatData(){
        listPenjualan = mainFrame.toi.getpenjualanByCabang(mainFrame.cabang.getIdCabang(),tglAwal.getDate(), tglAkhir.getDate());
        listPembelian = mainFrame.toi.getPembelianByCabang(mainFrame.cabang.getIdCabang(),tglAwal.getDate(), tglAkhir.getDate());
        generateChart();
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox comboBox;
    private javax.swing.JButton jButton2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel panelChart;
    private com.toedter.calendar.JDateChooser tglAkhir;
    private com.toedter.calendar.JDateChooser tglAwal;
    // End of variables declaration//GEN-END:variables

    

    private void generateChart() {
        String categoryLabel = TextUtility.formatTanggal(tglAwal.getDate())+" Sampai "+TextUtility.formatTanggal(tglAkhir.getDate());
        String valueLAbel = "Rupiah";
        list = new ArrayList<>();
        CategoryDataset dataSet = populateDataSetHari();
        String type = comboBox.getSelectedItem().toString();
        JFreeChart chart = null;
        switch (type) {
            case "LINE":
                chart = ChartFactory.createLineChart(title, categoryLabel, valueLAbel, dataSet, PlotOrientation.VERTICAL, true, false, false);
                break;
            case "LINE 3D":
                chart = ChartFactory.createLineChart3D(title, categoryLabel, valueLAbel, dataSet, PlotOrientation.VERTICAL, true, false, false);
                break;
            case "BAR":
                chart = ChartFactory.createBarChart(title, categoryLabel, valueLAbel, dataSet, PlotOrientation.VERTICAL, true, false, false);
                break;
            case "BAR 3D":
                chart = ChartFactory.createBarChart3D(title, categoryLabel, valueLAbel, dataSet, PlotOrientation.VERTICAL, true, false, false);
                break;
        }
        
        ChartPanel chartPanel = new ChartPanel( chart );
        chartPanel.setMouseWheelEnabled(true);
        panelChart.setLayout(new java.awt.BorderLayout());
        panelChart.add(chartPanel,BorderLayout.CENTER);
        panelChart.validate();
    }
    
    private CategoryDataset populateDataSetHari() {
        DefaultCategoryDataset dataSet = new DefaultCategoryDataset();
        Double pengeluaran;
        Double pemasukan;
            //setPenjualan
            for (int i=0 ; i<listPenjualan.size() ; i++) {
                pemasukan=Double.valueOf("0");
                for(int j=0;j<listPenjualan.size();j++){
                    if(listPenjualan.get(j).getTanggalJual().equals(listPenjualan.get(i).getTanggalJual())) {
                        pemasukan+=listPenjualan.get(j).getTotalHarga();
                    }
                }
                bantuGrafikDataSet bd = new bantuGrafikDataSet();
                bd.setData1(pemasukan);
                bd.setData2("Penjualan");
                bd.setData3(TextUtility.formatTanggalFromDateTime(listPenjualan.get(i).getTanggalJual()));
                list.add(bd);
                //dataSet.addValue(pemasukan, "Penjualan", listPenjualan.get(i).getTanggalJual());
            }
            //setDataPembelianByPenjualan
            for (int i=0 ; i<listPenjualan.size() ; i++) {
                pengeluaran=Double.valueOf("0");
                for(int j=0;j<listPembelian.size();j++){
                    if(listPembelian.get(j).getTanggalBeli().equals(listPenjualan.get(i).getTanggalJual())){
                        for(int k=0;k<listPembelian.size();k++){
                            if(listPembelian.get(k).getTanggalBeli().equals(listPembelian.get(j).getTanggalBeli()))
                            pengeluaran+=listPembelian.get(k).getTotalHarga();
                        }
                    }
                bantuGrafikDataSet bd = new bantuGrafikDataSet();
                bd.setData1(pengeluaran);
                bd.setData2("Pembelian");
                bd.setData3(TextUtility.formatTanggalFromDateTime(listPenjualan.get(i).getTanggalJual()));
                list.add(bd);
                    //dataSet.addValue(pengeluaran, "Pembelian", listPenjualan.get(i).getTanggalJual());
                }    
            }
            //setPembelian
            for (int i=0 ; i<listPembelian.size() ; i++) {
                pengeluaran=Double.valueOf("0");
                for(int j=0;j<listPembelian.size();j++){
                    if(listPembelian.get(j).getTanggalBeli().equals(listPembelian.get(i).getTanggalBeli()))
                    pengeluaran+=listPembelian.get(j).getTotalHarga();
                }
                bantuGrafikDataSet bd = new bantuGrafikDataSet();
                bd.setData1(pengeluaran);
                bd.setData2("Pembelian");
                bd.setData3(TextUtility.formatTanggalFromDateTime(listPembelian.get(i).getTanggalBeli()));
                list.add(bd);
                //dataSet.addValue(pengeluaran, "Pembelian", listPembelian.get(i).getTanggalBeli());
            }
            //setDataPenjualanByPembelian
            for (int i=0 ; i<listPembelian.size() ; i++) {
                pemasukan=Double.valueOf("0");
                for(int j=0;j<listPenjualan.size();j++){
                    if(listPenjualan.get(j).getTanggalJual().equals(listPembelian.get(i).getTanggalBeli())){
                        for(int k=0;k<listPenjualan.size();k++){
                            if(listPenjualan.get(k).getTanggalJual().equals(listPenjualan.get(j).getTanggalJual()))
                            pemasukan+=listPenjualan.get(k).getTotalHarga();
                        }
                    }
                bantuGrafikDataSet bd = new bantuGrafikDataSet();
                bd.setData1(pemasukan);
                bd.setData2("Penjualan");
                bd.setData3(TextUtility.formatTanggalFromDateTime(listPembelian.get(i).getTanggalBeli()));
                list.add(bd);
                //dataSet.addValue(pemasukan, "Penjualan", listPembelian.get(i).getTanggalBeli());
                }    
            }
        Collections.sort(list, new SorterGrafik());
        for(int x=0;x<list.size();x++){
            dataSet.addValue(list.get(x).getData1(), list.get(x).getData2(), list.get(x).getData3());
        }
        return dataSet;
    }
}


