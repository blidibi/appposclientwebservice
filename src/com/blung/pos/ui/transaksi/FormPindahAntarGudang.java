/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.transaksi;

import com.blung.pos.entity.bantuan.JmlBarang;
import com.blung.pos.entity.orm.TransferGudang;
import com.blung.pos.entity.simple.Barang;
import com.blung.pos.entity.simple.DetailTransferGudang;
import com.blung.pos.entity.simple.Gudang;
import com.blung.pos.ui.cari.dialog.DialogCariBarang;
import com.blung.pos.ui.cari.dialog.DialogCariGudang;
import com.blung.pos.ui.cari.dialog.DialogCariJumlahBarang;
import com.blung.pos.ui.main.mainFrame;
import com.blung.pos.ui.tabel.TabelDetailTransferGudang;
import com.lukman.utility.checker.CheckerData;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author lukman
 */
public class FormPindahAntarGudang extends javax.swing.JInternalFrame {

    /**
     * Creates new form FormPindahAntarGudang
     */
    private Barang barang;
    private Gudang gudangAsal;
    private Gudang gudang;
    private TransferGudang transferGudang;
    List<DetailTransferGudang> list;
    TabelDetailTransferGudang tabelDetailTransferGudang;
    public FormPindahAntarGudang(Gudang g) {
        gudangAsal=g;
        list=new ArrayList<>();
        tabelDetailTransferGudang=new TabelDetailTransferGudang();
        this.setTitle("Form Pindah Dari Gudang: "+gudangAsal.getNamaGudang());
        initComponents();
        loadTabel();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel5 = new javax.swing.JPanel();
        txtID = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabelui = new javax.swing.JTable();
        jPanel7 = new javax.swing.JPanel();
        tomSimpan = new javax.swing.JButton();
        cekboxCetak1 = new javax.swing.JCheckBox();
        jPanel6 = new javax.swing.JPanel();
        txtGudang = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/com/blung/pos/ui/icon/buatJIntframe.png"))); // NOI18N

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        txtID.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtIDKeyReleased(evt);
            }
        });

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/blung/pos/ui/icon/cari.png"))); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jLabel4.setText("ID:");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(244, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4)))
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tabelui.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null}
            },
            new String [] {
                "Data"
            }
        ));
        tabelui.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabeluiMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tabelui);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 596, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 245, Short.MAX_VALUE)
        );

        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tomSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/blung/pos/ui/icon/pindah.png"))); // NOI18N
        tomSimpan.setText("Pindah Gudang");
        tomSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tomSimpanActionPerformed(evt);
            }
        });

        cekboxCetak1.setSelected(true);
        cekboxCetak1.setText("Cetak Nota");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cekboxCetak1, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(tomSimpan, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(tomSimpan)
                .addComponent(cekboxCetak1))
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/blung/pos/ui/icon/cari.png"))); // NOI18N
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel5.setText("Gudang:");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtGudang, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(218, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtGudang, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel5)))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtIDKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIDKeyReleased
        int key = evt.getKeyCode();
        if (key == KeyEvent.VK_ENTER){
            String id = txtID.getText();
            if(!id.isEmpty()){
                Barang br = mainFrame.msi.getBarangByIDAndCabang(id, mainFrame.cabang);
                if(br!=null){
                    String jmlBarang = JOptionPane.showInternalInputDialog(this,"Masukkan Jumlah Barang - "+br.getNamaBarang());
                    if(CheckerData.isNumberFromString(jmlBarang)){
                        proses(br,Long.valueOf(jmlBarang));
                    }else JOptionPane.showMessageDialog(rootPane, "Inputan Salah");
                }else JOptionPane.showMessageDialog(rootPane, "Silahkan Pilih Barang");
                txtID.setText("");
            }else {
                //txtBayar.requestFocusInWindow();
            }
        }
    }//GEN-LAST:event_txtIDKeyReleased

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        JmlBarang jb=new JmlBarang();
        jb=new DialogCariJumlahBarang().cari();
        if(jb!=null){
            String jmlBarang = JOptionPane.showInternalInputDialog(this,"Masukkan Jumlah Barang - "+jb.getNamaBarang());
            barang=mainFrame.msi.getBarangByIDAndCabang(jb.getIdBarang(), mainFrame.cabang);
            if(CheckerData.isNumberFromString(jmlBarang)){
                proses(barang,Long.valueOf(jmlBarang));
                txtID.setText("");
            }else JOptionPane.showMessageDialog(rootPane, "Inputan Salah");
        }else JOptionPane.showMessageDialog(rootPane, "Silahkan Pilih Barang");
        
    }//GEN-LAST:event_jButton3ActionPerformed

    private void tabeluiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabeluiMouseClicked

    }//GEN-LAST:event_tabeluiMouseClicked

    private void tomSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tomSimpanActionPerformed
        simpan();
    }//GEN-LAST:event_tomSimpanActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        gudang=new Gudang();
        gudang=new DialogCariGudang().cariGudang();
        if(gudang==null) JOptionPane.showMessageDialog(rootPane, "Silahkan Pilih Gudang");
        else{
            txtGudang.setText(gudang.getNamaGudang()+"-"+mainFrame.cabang.getNamaCabang());
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox cekboxCetak1;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tabelui;
    private javax.swing.JButton tomSimpan;
    private javax.swing.JTextField txtGudang;
    private javax.swing.JTextField txtID;
    // End of variables declaration//GEN-END:variables

    private void proses(Barang br, Long jml) {
        DetailTransferGudang dtg=new DetailTransferGudang();
        dtg.setHarga(br.getHarga().getHargaBeliRata());
        dtg.setIdBarang(br.getIdBarang());
        dtg.setJumlah(jml);
        dtg.setSubTotal(Double.valueOf(jml*br.getHarga().getHargaBeliRata()));
        list.add(dtg);
        loadTabel();
    }
    
    private void loadTabel() {
        tabelDetailTransferGudang.update(list);
        tabelui.setModel(tabelDetailTransferGudang);
    }

    private void simpan() {
        if(gudang!=null){
            transferGudang=new TransferGudang();
            transferGudang.setIdGudangPenerima(gudang.getIdGudang());
            transferGudang.setIdGudangPengirim(gudangAsal.getIdGudang());
            transferGudang.setIdKaryawanPenerima(mainFrame.user.getIdKaryawan());
            transferGudang.setIdKaryawanPengirim(mainFrame.user.getIdKaryawan());
            transferGudang.setIdUser(mainFrame.user.getIdUser());
            transferGudang.setTanggalKirim(new Date());
            transferGudang.setTanggalTerima(new Date());
            transferGudang.setList(list);
            if(mainFrame.toi.saveTransferGudangKeGudang(transferGudang)){
                list.clear();
                JOptionPane.showMessageDialog(rootPane, "Simpan");
                loadTabel();
                barang=null;
                gudang=null;
            }
        }
    }
}
