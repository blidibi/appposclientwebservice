/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.transaksi;

import com.blung.pos.entity.bantuan.JmlBarang;
import com.blung.pos.entity.orm.TransferGudangKeCabang;
import com.blung.pos.entity.simple.Barang;
import com.blung.pos.entity.simple.Cabang;
import com.blung.pos.entity.simple.DetailTransferGudangKeCabang;
import com.blung.pos.entity.simple.Gudang;
import com.blung.pos.ui.cari.dialog.DialogCariCabang;
import com.blung.pos.ui.cari.dialog.DialogCariJumlahBarang;
import com.blung.pos.ui.main.mainFrame;
import com.blung.pos.ui.tabel.TabelDetailTransferGudangKeCabang;
import com.lukman.utility.checker.CheckerData;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author lukman
 */
public class FormPindahGudangKeCabang extends javax.swing.JInternalFrame {

    /**
     * Creates new form FormPindahGudangKeCabang
     */
    private Gudang gudang;
    private Cabang cabang;
    private Barang barang;
    private TransferGudangKeCabang tgkc;
    private List<DetailTransferGudangKeCabang> list;
    private TabelDetailTransferGudangKeCabang tabelDetailTransferGudangKeCabang;
    public FormPindahGudangKeCabang(Gudang gg) {
        gudang=gg;
        initComponents();
        list=new ArrayList<>();
        tabelDetailTransferGudangKeCabang=new TabelDetailTransferGudangKeCabang();
        loadTabel();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabelui = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        txtID = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        txtCabang = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jButton5 = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        cetakBtn = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tabelui.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null}
            },
            new String [] {
                "Data"
            }
        ));
        tabelui.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabeluiMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tabelui);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 371, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 273, Short.MAX_VALUE)
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        txtID.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtIDKeyReleased(evt);
            }
        });

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/blung/pos/ui/icon/cari.png"))); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jLabel4.setText("ID:");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(19, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4)))
                .addContainerGap())
        );

        jPanel8.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Pilih Cabang:");

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/blung/pos/ui/icon/cari.png"))); // NOI18N
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtCabang, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(28, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtCabang, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel7)))
                .addContainerGap())
        );

        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        cetakBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/blung/pos/ui/icon/pindah.png"))); // NOI18N
        cetakBtn.setText("Pindah Cabang");
        cetakBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cetakBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap(214, Short.MAX_VALUE)
                .addComponent(cetakBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cetakBtn)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tabeluiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabeluiMouseClicked

    }//GEN-LAST:event_tabeluiMouseClicked

    private void txtIDKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIDKeyReleased
        int key = evt.getKeyCode();
        if (key == KeyEvent.VK_ENTER){
            String id = txtID.getText();
            if(!id.isEmpty()){
                Barang br = mainFrame.msi.getBarangByIDAndCabang(id, mainFrame.cabang);
                if(br!=null){
                    String jmlBarang = JOptionPane.showInternalInputDialog(this,"Masukkan Jumlah Barang - "+br.getNamaBarang());
                    if(CheckerData.isNumberFromString(jmlBarang)){
                        proses(br,Long.valueOf(jmlBarang));
                    }else JOptionPane.showMessageDialog(rootPane, "Inputan Salah");
                }else JOptionPane.showMessageDialog(rootPane, "Silahkan Pilih Barang");
                txtID.setText("");
            }else {
                //txtBayar.requestFocusInWindow();
            }
        }
    }//GEN-LAST:event_txtIDKeyReleased

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        JmlBarang jb=new JmlBarang();
        jb=new DialogCariJumlahBarang().cari();
        if(jb!=null){
            String jmlBarang = JOptionPane.showInternalInputDialog(this,"Masukkan Jumlah Barang - "+jb.getNamaBarang());
            barang=mainFrame.msi.getBarangByIDAndCabang(jb.getIdBarang(), mainFrame.cabang);
            if(CheckerData.isNumberFromString(jmlBarang)){
                proses(barang,Long.valueOf(jmlBarang));
                txtID.setText("");
            }else JOptionPane.showMessageDialog(rootPane, "Inputan Salah");
        }else JOptionPane.showMessageDialog(rootPane, "Silahkan Pilih Barang");
        
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        cabang=new DialogCariCabang().pilihCabang();
        if(cabang!=null){
             txtCabang.setText(cabang.getNamaCabang());
        }else{
            JOptionPane.showMessageDialog(rootPane, "Silahkan Pilih Cabang");
        }
    }//GEN-LAST:event_jButton5ActionPerformed

    private void cetakBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cetakBtnActionPerformed
        simpanCabang();
    }//GEN-LAST:event_cetakBtnActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cetakBtn;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton5;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tabelui;
    private javax.swing.JTextField txtCabang;
    private javax.swing.JTextField txtID;
    // End of variables declaration//GEN-END:variables

    private void proses(Barang barang, Long jml) {
        DetailTransferGudangKeCabang dtgkc=new DetailTransferGudangKeCabang();
        dtgkc.setIdBarang(barang.getIdBarang());
        dtgkc.setJumlah(jml);
        list.add(dtgkc);
    }

    private void simpanCabang() {
        if(cabang!=null){
            tgkc=new TransferGudangKeCabang();
            tgkc.setIdCabang(cabang.getIdCabang());
            tgkc.setIdGudang(gudang.getIdGudang());
            tgkc.setIdUser(mainFrame.user.getIdUser());
            tgkc.setIdKaryawanPenerima(mainFrame.user.getIdKaryawan());
            tgkc.setIdKaryawanPengirim(mainFrame.user.getIdKaryawan());
            tgkc.setTanggalKirim(new Date());
            tgkc.setTanggalTerima(new Date());
            tgkc.setList(list);
            if(mainFrame.toi.saveTransferGudangKeCabang(tgkc)){
                list.clear();
                JOptionPane.showMessageDialog(rootPane, "Simpan");
                loadTabel();
                cabang=null;
            }
        }else JOptionPane.showMessageDialog(rootPane, "Silahkan Pilih Cabang");
        
    }

    private void loadTabel() {
        tabelDetailTransferGudangKeCabang.update(list);
        tabelui.setModel(tabelDetailTransferGudangKeCabang);
    }
}
