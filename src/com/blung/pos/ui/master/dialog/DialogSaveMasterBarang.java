/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.master.dialog;

import com.blung.pos.entity.simple.Barang;
import com.blung.pos.entity.simple.Harga;
import com.blung.pos.entity.simple.Kategori;
import com.blung.pos.entity.simple.Satuan;
import com.blung.pos.entity.simple.Supliyer;
import com.blung.pos.ui.cari.dialog.DialogCariKategori;
import com.blung.pos.ui.cari.dialog.DialogCariSatuan;
import com.blung.pos.ui.cari.dialog.DialogCariSupliyer;
import com.blung.pos.ui.main.mainFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author lukman
 */
public class DialogSaveMasterBarang extends javax.swing.JDialog {

    /**
     * Creates new form DialogSaveMasterBarang
     */
    private Boolean statUpdate=false;
    private Barang barang;
    private Harga harga;
    private Kategori kategori;
    private Supliyer supliyer;
    private Satuan satuan;
    
    public DialogSaveMasterBarang() {
        super(mainFrame.mf, true);
        barang=new Barang();
        this.setLocationRelativeTo(null);
        initComponents();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtBarcode = new javax.swing.JTextField();
        txtNamaBarang = new javax.swing.JTextField();
        txtKategori = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtSupliyer = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtSatuan = new javax.swing.JTextField();
        txtHargaBeliTerakhir = new javax.swing.JTextField();
        txtHargaBeliRata = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        tomSimpan = new javax.swing.JButton();
        tomReset = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        txtBatas11 = new javax.swing.JTextField();
        txtBatas21 = new javax.swing.JTextField();
        txtBatas31 = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtBatas12 = new javax.swing.JTextField();
        txtBatas22 = new javax.swing.JTextField();
        txtBatas32 = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        txtHrg1 = new javax.swing.JTextField();
        txtHrg2 = new javax.swing.JTextField();
        txtHrg3 = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        hargaAtas = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        diskon = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Barcode: ");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Nama Barang: ");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Kategori: ");

        txtKategori.setEditable(false);

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Supliyer: ");

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Satuan: ");

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Harga Beli Terakhir: ");

        txtSupliyer.setEditable(false);

        jLabel7.setText("Harga Beli Rata-Rata: ");

        txtSatuan.setEditable(false);

        txtHargaBeliTerakhir.setEditable(false);
        txtHargaBeliTerakhir.setText("0");

        txtHargaBeliRata.setEditable(false);
        txtHargaBeliRata.setText("0");

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/blung/pos/ui/icon/cari.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/blung/pos/ui/icon/cari.png"))); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/blung/pos/ui/icon/cari.png"))); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtKategori, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtNamaBarang, javax.swing.GroupLayout.DEFAULT_SIZE, 222, Short.MAX_VALUE)
                                .addComponent(txtBarcode))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtSupliyer)
                            .addComponent(txtSatuan, javax.swing.GroupLayout.DEFAULT_SIZE, 222, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtHargaBeliTerakhir)
                            .addComponent(txtHargaBeliRata))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtBarcode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtNamaBarang, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel3)
                                .addComponent(txtKategori, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(txtSupliyer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(txtSatuan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtHargaBeliTerakhir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtHargaBeliRata, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tomSimpan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/blung/pos/ui/icon/tambah.png"))); // NOI18N
        tomSimpan.setText("Simpan");
        tomSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tomSimpanActionPerformed(evt);
            }
        });

        tomReset.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/blung/pos/ui/icon/refresh.png"))); // NOI18N
        tomReset.setText("Reset");
        tomReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tomResetActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tomSimpan)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tomReset, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tomSimpan)
                    .addComponent(tomReset))
                .addGap(0, 8, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Harga Jual"));

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Qty"));

        txtBatas11.setEditable(false);
        txtBatas11.setText("1");

        txtBatas21.setEditable(false);
        txtBatas21.setText("1");

        txtBatas31.setEditable(false);
        txtBatas31.setText("1");

        jLabel8.setText(" - ");

        jLabel9.setText(" - ");

        jLabel10.setText(" - ");

        txtBatas12.setText("1");
        txtBatas12.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBatas12KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBatas12KeyReleased(evt);
            }
        });

        txtBatas22.setText("1");
        txtBatas22.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBatas22KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBatas22KeyReleased(evt);
            }
        });

        txtBatas32.setText("1");
        txtBatas32.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBatas32KeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(txtBatas21, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 65, Short.MAX_VALUE)
                    .addComponent(txtBatas31, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtBatas11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBatas22, javax.swing.GroupLayout.DEFAULT_SIZE, 72, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBatas32))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBatas12))))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBatas11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(txtBatas12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBatas21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(txtBatas22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBatas31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(txtBatas32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Harga"));

        txtHrg1.setText("0");
        txtHrg1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtHrg1MouseClicked(evt);
            }
        });
        txtHrg1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtHrg1KeyReleased(evt);
            }
        });

        txtHrg2.setText("0");
        txtHrg2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtHrg2MouseClicked(evt);
            }
        });
        txtHrg2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtHrg2KeyReleased(evt);
            }
        });

        txtHrg3.setText("0");
        txtHrg3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtHrg3MouseClicked(evt);
            }
        });
        txtHrg3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtHrg3KeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(txtHrg1)
            .addComponent(txtHrg2)
            .addComponent(txtHrg3, javax.swing.GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(txtHrg1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtHrg2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtHrg3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder("Harga Diluar Qty"));

        hargaAtas.setText("0");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(hargaAtas, javax.swing.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(hargaAtas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel11.setText("Diskon Promo : Rp");

        diskon.setText("0");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(diskon))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(diskon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 6, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tomSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tomSimpanActionPerformed
        simpan();
    }//GEN-LAST:event_tomSimpanActionPerformed

    private void txtBatas12KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBatas12KeyPressed
        
    }//GEN-LAST:event_txtBatas12KeyPressed

    private void txtBatas22KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBatas22KeyPressed
       
    }//GEN-LAST:event_txtBatas22KeyPressed

    private void tomResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tomResetActionPerformed
        reset();
    }//GEN-LAST:event_tomResetActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        pilihKategori();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        pilihSupliyer();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        pilihSatuan();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void txtBatas12KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBatas12KeyReleased
        batas();
    }//GEN-LAST:event_txtBatas12KeyReleased

    private void batas(){
        txtBatas21.setText(String.valueOf(Integer.parseInt(txtBatas12.getText())+1));
        if(Integer.parseInt(txtBatas22.getText())<Integer.parseInt(txtBatas21.getText())){
            txtBatas22.setText(txtBatas21.getText());
            txtBatas31.setText(String.valueOf(Integer.parseInt(txtBatas22.getText())));
        }
        if(Integer.parseInt(txtBatas32.getText())<Integer.parseInt(txtBatas31.getText())){
            txtBatas32.setText(txtBatas31.getText());
        }
    }
    
    private void txtBatas22KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBatas22KeyReleased
        txtBatas31.setText(String.valueOf(Integer.parseInt(txtBatas22.getText())+1));
        if(Integer.parseInt(txtBatas32.getText())<Integer.parseInt(txtBatas31.getText())){
            txtBatas32.setText(txtBatas31.getText());
        }
    }//GEN-LAST:event_txtBatas22KeyReleased

    private void txtBatas32KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBatas32KeyReleased
        
    }//GEN-LAST:event_txtBatas32KeyReleased

    private void txtHrg1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHrg1KeyReleased
        if(Integer.parseInt(txtHrg2.getText())>Integer.parseInt(txtHrg1.getText())){
            txtHrg2.setText(txtHrg1.getText());
        }
        if(Integer.parseInt(txtHrg3.getText())>Integer.parseInt(txtHrg2.getText())){
            txtHrg3.setText(txtHrg2.getText());
        }
        if(Integer.parseInt(hargaAtas.getText())>Integer.parseInt(txtHrg3.getText())){
            hargaAtas.setText(txtHrg3.getText());
        }
        txtHrg2.setText(txtHrg1.getText());
        txtHrg3.setText(txtHrg2.getText());
        int a=hargaAtas();
        hargaAtas.setText(String.valueOf(a));
    }//GEN-LAST:event_txtHrg1KeyReleased

    private void txtHrg1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtHrg1MouseClicked
        txtHrg1.setText("");
    }//GEN-LAST:event_txtHrg1MouseClicked

    private void txtHrg2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtHrg2MouseClicked
        txtHrg2.setText("");
    }//GEN-LAST:event_txtHrg2MouseClicked

    private void txtHrg3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtHrg3MouseClicked
        txtHrg3.setText("");
    }//GEN-LAST:event_txtHrg3MouseClicked

    private void txtHrg2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHrg2KeyReleased
        if(Integer.parseInt(txtHrg3.getText())>Integer.parseInt(txtHrg2.getText())){
            txtHrg3.setText(txtHrg2.getText());
        }
        if(Integer.parseInt(hargaAtas.getText())>Integer.parseInt(txtHrg3.getText())){
            hargaAtas.setText(txtHrg3.getText());
        }
        txtHrg3.setText(txtHrg2.getText());
        int a=hargaAtas();
        hargaAtas.setText(String.valueOf(a));
    }//GEN-LAST:event_txtHrg2KeyReleased

    private void txtHrg3KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHrg3KeyReleased
        if(Integer.parseInt(hargaAtas.getText())>Integer.parseInt(txtHrg3.getText())){
            hargaAtas.setText(txtHrg3.getText());
        }
        int a=hargaAtas();
        hargaAtas.setText(String.valueOf(a));
        
    }//GEN-LAST:event_txtHrg3KeyReleased
    
    private int hargaAtas(){
        int h1=Integer.parseInt(txtHrg1.getText());
        int h2=Integer.parseInt(txtHrg2.getText());
        int h3=Integer.parseInt(txtHrg3.getText());
        int h4=Integer.parseInt(hargaAtas.getText());
        if(h4<h1) h4=h1;
        if(h4<h2) h4=h2;
        if(h4<h3) h4=h3;
        return h4;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField diskon;
    private javax.swing.JTextField hargaAtas;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JButton tomReset;
    private javax.swing.JButton tomSimpan;
    private javax.swing.JTextField txtBarcode;
    private javax.swing.JTextField txtBatas11;
    private javax.swing.JTextField txtBatas12;
    private javax.swing.JTextField txtBatas21;
    private javax.swing.JTextField txtBatas22;
    private javax.swing.JTextField txtBatas31;
    private javax.swing.JTextField txtBatas32;
    private javax.swing.JTextField txtHargaBeliRata;
    private javax.swing.JTextField txtHargaBeliTerakhir;
    private javax.swing.JTextField txtHrg1;
    private javax.swing.JTextField txtHrg2;
    private javax.swing.JTextField txtHrg3;
    private javax.swing.JTextField txtKategori;
    private javax.swing.JTextField txtNamaBarang;
    private javax.swing.JTextField txtSatuan;
    private javax.swing.JTextField txtSupliyer;
    // End of variables declaration//GEN-END:variables

    void simpan(){
        if(cekInputan()==true){
            if(statUpdate==false){
                barang=new Barang();
                harga=new Harga();
            }
            harga.setIdKategori(kategori.getIdKategori());
            harga.setIdSupliyer(supliyer.getIdSupliyer());
            harga.setIdSatuan(satuan.getIdSatuan());
            harga.setHargaBeliTerakhir(Double.valueOf(txtHargaBeliTerakhir.getText()));
            harga.setHargaBeliRata(Double.valueOf(txtHargaBeliRata.getText()));
            harga.setBatasSatu(Long.valueOf(txtBatas12.getText()));
            harga.setBatasDua(Long.valueOf(txtBatas22.getText()));
            harga.setBatasTiga(Long.valueOf(txtBatas32.getText()));
            harga.setHargaSatu(Double.valueOf(txtHrg1.getText()));
            harga.setHargaDua(Double.valueOf(txtHrg2.getText()));
            harga.setHargaTiga(Double.valueOf(txtHrg3.getText()));
            harga.setHargaEmpat(Double.valueOf(hargaAtas.getText()));
            harga.setDiskonPromo(Double.valueOf(diskon.getText()));
            harga.setIdCabang(mainFrame.cabang.getIdCabang());
            barang.setIdBarangBarcode(txtBarcode.getText());
            barang.setNamaBarang(txtNamaBarang.getText());
            barang.setHarga(harga);
            if(statUpdate==false){
                if(mainFrame.msi.save(barang)==true){
                    JOptionPane.showMessageDialog(rootPane, "Simpan");
                }
            }else{
                if(mainFrame.msi.update(barang)==true){
                    JOptionPane.showMessageDialog(rootPane, "Update");
                }
            }
            this.dispose();
        }
    }
    
    public void tambah(){
        this.setVisible(true);
    }
    
    public void rubah(Barang b){
        barang=b;
        harga=barang.getHarga();
        statUpdate=true;
        kategori=mainFrame.msi.getKategoriByID(harga.getIdKategori());
        supliyer=mainFrame.msi.getSupliyerByID(harga.getIdSupliyer());
        satuan=mainFrame.msi.getSatuanByID(harga.getIdSatuan());
        txtBarcode.setText(barang.getIdBarangBarcode());
        txtNamaBarang.setText(barang.getNamaBarang());
        txtHargaBeliTerakhir.setText(String.valueOf(harga.getHargaBeliTerakhir()));
        txtHargaBeliRata.setText(String.valueOf(harga.getHargaBeliRata()));
        txtBatas11.setText("1");
        txtBatas12.setText(String.valueOf(harga.getBatasSatu()));
        batas();
        txtBatas22.setText(String.valueOf(harga.getBatasDua()));
        batas();
        txtBatas32.setText(String.valueOf(harga.getBatasTiga()));
        batas();
        txtHrg1.setText(String.valueOf(harga.getHargaSatu()));
        txtHrg2.setText(String.valueOf(harga.getHargaDua()));
        txtHrg3.setText(String.valueOf(harga.getHargaTiga()));
        hargaAtas.setText(String.valueOf(harga.getHargaEmpat()));
        txtSupliyer.setText(supliyer.getNamaSupliyer());
        txtKategori.setText(kategori.getNamaKategori());
        txtSatuan.setText(satuan.getNamaSatuan());
        diskon.setText(String.valueOf(harga.getDiskonPromo()));
        this.setVisible(true);
    }
    
    private void reset() {
        txtBarcode.setText("");
        txtNamaBarang.setText("");
        txtHargaBeliTerakhir.setText("");
        txtHargaBeliRata.setText("");
        txtBatas11.setText("1");
        txtBatas12.setText("1");
        txtBatas21.setText("1");
        txtBatas22.setText("1");
        txtBatas31.setText("1");
        txtBatas32.setText("1");
        txtHrg1.setText("0");
        txtHrg2.setText("0");
        txtHrg3.setText("0");
        hargaAtas.setText("0");
        txtSupliyer.setText("");
        txtKategori.setText("");
        txtSatuan.setText("");
        diskon.setText("0");
        barang=new Barang();
        kategori=new Kategori();
        supliyer=new Supliyer();
        satuan=new Satuan();
    }

    private void pilihKategori(){
        kategori=new DialogCariKategori().cari();
        if(kategori!=null){
             txtKategori.setText(kategori.getNamaKategori());
        }else{
            JOptionPane.showMessageDialog(rootPane, "Silahkan Pilih Kategori");
        }
    }

    private void pilihSupliyer() {
        supliyer = new DialogCariSupliyer().cari();
        if(supliyer != null){
            txtSupliyer.setText(supliyer.getNamaSupliyer());
        }
        else{
            JOptionPane.showMessageDialog(rootPane, "Silahkan Pilih Supliyer");
        }
    }

    private void pilihSatuan() {
        satuan = new DialogCariSatuan().cari();
        if(satuan != null){
            txtSatuan.setText(satuan.getNamaSatuan());
        }
        else{
            JOptionPane.showMessageDialog(rootPane, "Silahkan Pilih Satuan");
        }
    }

    private boolean cekInputan() {
        if(txtBarcode.getText().isEmpty() || txtHargaBeliRata.getText().isEmpty() || txtHargaBeliTerakhir.getText().isEmpty() || txtHrg1.getText().isEmpty() || txtHrg2.getText().isEmpty() || txtHrg3.getText().isEmpty() || txtKategori.getText().isEmpty() || txtNamaBarang.getText().isEmpty() || txtSatuan.getText().isEmpty() || txtSupliyer.getText().isEmpty() || hargaAtas.getText().isEmpty() || !cekBatasHarga()){
            JOptionPane.showMessageDialog(rootPane, "Inputan Ada Yang Salah atau Kosong");
            return false;
        }
        else return true;
    }
    
    private Boolean cekBatasHarga(){
        int b1=Integer.parseInt(txtBatas12.getText());
        int b2=Integer.parseInt(txtBatas22.getText());
        int b3=Integer.parseInt(txtBatas32.getText());
        if(b2<b1 || b3<b1 || b3<b2){
            if(cekHarga()) return true;
            else return false;
        }
        else return true;
    }
    
    private boolean cekHarga(){
        int h1=Integer.parseInt(txtHrg1.getText());
        int h2=Integer.parseInt(txtHrg2.getText());
        int h3=Integer.parseInt(txtHrg3.getText());
        int h4=Integer.parseInt(hargaAtas.getText());
        if(h2>h1 || h3>h2 || h3>h1 || h4>h3 || h4>h2 || h4>h1) return false;
        else return true;
    }

}
