/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.tabel;

import com.blung.pos.entity.simple.CicilanPiutangPenjualan;
import com.blung.pos.entity.simple.Karyawan;
import com.blung.pos.entity.simple.User;
import com.blung.pos.ui.main.mainFrame;
import com.blung.pos.utility.FindObjectOnList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author user
 */
public class TabelCicilanPiutangPenjualan extends AbstractTableModel{
    
    List<CicilanPiutangPenjualan> list = new ArrayList<>();
    List<User> list1 = mainFrame.msi.getAllUser();
    List<Karyawan> list2 = mainFrame.msi.getAllKaryawan();

    @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    @Override
    public boolean isCellEditable(int row, int col) {
        if(col < 2) return false;
        else return true;
    }
    
    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return rowIndex+1;
            case 1:
                return list.get(rowIndex).getIdCicilanPiutang();
            case 2:
                return list.get(rowIndex).getTanggalBayar();
            case 3:
                return list.get(rowIndex).getIdCicilanPiutang();
            case 4:
                return FindObjectOnList.findKaryawanFromListById(list2, FindObjectOnList.findUserFromListById(list1, list.get(rowIndex).getIdUser()).getIdKaryawan()).getNama();
            case 5:
                return list.get(rowIndex).getKeterangan();
            default:
                return null;
        }
    }
    
     public String getColumnName(int column) {
        switch(column){
            case 0:
                return "No";
            case 1:
                return "Tanggal Bayar";
            case 2:
                return "Nama Karyawan";
            case 3:
                return "Jumlah Bayar";
            case 4:
                return "Keterangan";
            default:
                return null;
        }
    }
     
     public CicilanPiutangPenjualan getCicilanPiutangPenjualanAt(int row){
        return list.get(row);
    }
    
    public void add(CicilanPiutangPenjualan c){
        list.add(c);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
    public void delete(int index){
        list.remove(index);
        fireTableRowsDeleted(index, index);
    }
    public void update(){
        fireTableDataChanged();
    }
    public void update(Collection<CicilanPiutangPenjualan> b){
        list.clear();
        list.addAll(b);
        fireTableDataChanged();
    }
    
}
