/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.tabel;

import com.blung.pos.entity.simple.Gudang;
import com.blung.pos.entity.simple.Karyawan;
import com.blung.pos.entity.simple.TransferGudang;
import com.blung.pos.entity.simple.User;
import com.blung.pos.ui.main.mainFrame;
import com.blung.pos.utility.FindObjectOnList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author lukman
 */
public class TabelTransferGudang extends AbstractTableModel{
    List<TransferGudang> list = new ArrayList<>();
    List<Gudang> lg=mainFrame.msi.getAllGudang();
    List<Karyawan> lk=mainFrame.msi.getAllKaryawan();
    List<User> lu=mainFrame.msi.getAllUser();
    
    @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    @Override
    public boolean isCellEditable(int row, int col) {
        if(col < 2) return false;
        else return true;
    }
    
    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 8;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            
            case 0:
                return rowIndex+1;
            case 1:
                return FindObjectOnList.findGudangFromListById(lg, list.get(rowIndex).getIdGudangPengirim()).getNamaGudang();
            case 2:
                return FindObjectOnList.findGudangFromListById(lg, list.get(rowIndex).getIdGudangPenerima()).getNamaGudang();
            case 3:
                return FindObjectOnList.findKaryawanFromListById(lk, list.get(rowIndex).getIdKaryawanPengirim()).getNama();
            case 4:
                return FindObjectOnList.findKaryawanFromListById(lk, list.get(rowIndex).getIdKaryawanPenerima()).getNama();
            case 5:
                return FindObjectOnList.findUserFromListById(lu, list.get(rowIndex).getIdUser()).getUsername();
            case 6:
                return list.get(rowIndex).getTanggalKirim();
            case 7:
                return list.get(rowIndex).getTanggalTerima();
            default:
                return null;
            
        }
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "No";
            case 1:
                return "Gudang Kirim";
            case 2:
                return "Gudang Terima";
            case 3:
                return "Karyawan Kirim";
            case 4:
                return "Karyawan Terima";
            case 5:
                return "User";
            case 6:
                return "Tanggal Kirim";
            case 7:
                return "Tanggal Terima";
            default:
                return null;
        }
    }
    
    public TransferGudang getTransferGudangAt(int row){
        return list.get(row);
    }
    
    public void add(TransferGudang c){
        list.add(c);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
    public void delete(int index){
        list.remove(index);
        fireTableRowsDeleted(index, index);
    }
    public void update(){
        fireTableDataChanged();
    }
    public void update(Collection<TransferGudang> b){
        list.clear();
        list.addAll(b);
        fireTableDataChanged();
    }
}
