/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.tabel;

import com.blung.pos.entity.simple.CicilanPiutangPenjualan;
import com.blung.pos.entity.simple.Pelanggan;
import com.blung.pos.entity.simple.Penjualan;
import com.blung.pos.entity.simple.PiutangPenjualan;
import com.blung.pos.ui.main.mainFrame;
import com.blung.pos.utility.FindObjectOnList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author lukman
 */
public class TabelPiutangPenjualan extends AbstractTableModel{

    public TabelPiutangPenjualan(Date start, Date end) {
        lp=mainFrame.toi.getpenjualanByCabang(mainFrame.cabang.getIdCabang(), start, end);
        lpp=mainFrame.msi.getAllPelanggan();
        lcicilan = mainFrame.tsi.getCicilanPiutangPenjualanByCabangAndDate(start, end, mainFrame.cabang.getIdCabang());
    }
    
    List<PiutangPenjualan> list = new ArrayList<>();
    List<Penjualan> lp= new ArrayList<>();
    List<Pelanggan> lpp= new ArrayList<>();
    List<CicilanPiutangPenjualan> lcicilan = new ArrayList<>();
    
    @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    @Override
    public boolean isCellEditable(int row, int col) {
        if(col < 2) return false;
        else return true;
    }
    
    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 8;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            
            case 0:
                return rowIndex+1;
            case 1:
                return list.get(rowIndex).getIdPenjualan();
            case 2:
                return FindObjectOnList.findPenjualanFromListById(lp, list.get(rowIndex).getIdPenjualan()).getTanggalJual();
            case 3:
                return FindObjectOnList.findPenjualanFromListById(lp, list.get(rowIndex).getIdPenjualan()).getTotalHarga();
            case 4:
                return FindObjectOnList.findPelangganFromListById(lpp, list.get(rowIndex).getIdPelanggan()).getNama();
            case 5:
                Double totcil=Double.valueOf("0");
                for(CicilanPiutangPenjualan cp:lcicilan){
                    if(cp.getIdPiutangPenjualan().equals(list.get(rowIndex).getIdPiutangPenjualan())){
                        totcil+=cp.getJumlahBayar();
                    }
                }
                return String.valueOf(FindObjectOnList.findPenjualanFromListById(lp, list.get(rowIndex).getIdPenjualan()).getTotalHarga())+"("+String.valueOf(totcil)+")";
            case 6:
                return list.get(rowIndex).getKeterangan();
            case 7:
                if(list.get(rowIndex).getStatusLunas()==true) return "lunas"; else return "belum";
            default:
                return null;
            
        }
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "No";
            case 1:
                return "No Nota";
            case 2:
                return "Tanggal";
            case 3:
                return "Jumlah";
            case 4:
                return "Pelanggan";
            case 5:
                return "Jumlah(Bayar)";
            case 6:
                return "Keterangan";
            case 7:
                return "Status";
            default:
                return null;
        }
    }
    
    public PiutangPenjualan getPiutangPenjualanAt(int row){
        return list.get(row);
    }
    
    public void add(PiutangPenjualan c){
        list.add(c);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
    public void delete(int index){
        list.remove(index);
        fireTableRowsDeleted(index, index);
    }
    public void update(){
        fireTableDataChanged();
    }
    public void update(Collection<PiutangPenjualan> b){
        list.clear();
        list.addAll(b);
        fireTableDataChanged();
    }
}
