/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.tabel;


import com.blung.pos.entity.simple.Barang;
import com.blung.pos.entity.simple.DetailReturPembelian;
import com.blung.pos.entity.simple.DetailReturPenjualan;
import com.blung.pos.ui.main.mainFrame;
import com.blung.pos.utility.FindObjectOnList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author lukman
 */
public class TabelDetailReturPenjualan extends AbstractTableModel{
    List<DetailReturPenjualan> list = new ArrayList<>();
    List<Barang> lbar=mainFrame.msi.getAllBarangByCabang(mainFrame.cabang);
    
    @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    @Override
    public boolean isCellEditable(int row, int col) {
        if(col < 2) return false;
        else return true;
    }
    
    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            
            case 0:
                return rowIndex+1;
            case 1:
                return FindObjectOnList.findBarangFromListById(lbar, list.get(rowIndex).getIdBarang()).getIdBarangBarcode();
            case 2:
                return FindObjectOnList.findBarangFromListById(lbar, list.get(rowIndex).getIdBarang()).getNamaBarang();
            case 3:
                return list.get(rowIndex).getJumlah();
            case 4:
                return list.get(rowIndex).getHarga();
            case 5:
                return list.get(rowIndex).getKeterangan();
            default:
                return null;
            
        }
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "No";
            case 1:
                return "ID Barang";
            case 2:
                return "Nama Barang";
            case 3:
                return "Jumlah";
            case 4:
                return "Harga";
            case 5:
                return "Keterangan";
            default:
                return null;
        }
    }
    
    public DetailReturPenjualan getDetailReturPenjualanAt(int row){
        return list.get(row);
    }
    
    public void add(DetailReturPenjualan c){
        list.add(c);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
    public void delete(int index){
        list.remove(index);
        fireTableRowsDeleted(index, index);
    }
    public void update(){
        fireTableDataChanged();
    }
    public void update(Collection<DetailReturPenjualan> b){
        list.clear();
        list.addAll(b);
        fireTableDataChanged();
    }
}
