/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.tabel;

import com.blung.pos.entity.bantuan.JmlBarang;
import com.blung.pos.entity.bantuan.JmlGudang;
import com.blung.pos.entity.simple.Barang;
import com.blung.pos.ui.main.mainFrame;
import com.blung.pos.utility.FindObjectOnList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author lukman
 */
public class TabelJumlahBarang extends AbstractTableModel{
    
    List<JmlBarang> list =new ArrayList<>();
    List<Barang> listBarang=mainFrame.msi.getAllBarangByCabang(mainFrame.cabang);
    
    @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    @Override
    public boolean isCellEditable(int row, int col) {
        if(col < 2) return false;
        else return true;
    }
    
    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
         switch(columnIndex){
            case 0:
                return rowIndex+1;
            case 1:
                return list.get(rowIndex).getIdBarang();
            case 2:
                return FindObjectOnList.findBarangFromListById(listBarang, list.get(rowIndex).getIdBarang()).getIdBarangBarcode();
            case 3:
                return list.get(rowIndex).getNamaBarang();
            case 4:
                return list.get(rowIndex).getJumlahBarangCabang();
            case 5:
                Long jml=Long.valueOf("0");
                for(JmlGudang jg:list.get(rowIndex).getListGudang()){
                    jml+=jg.getJumlahBarangGudang();
                }
                return String.valueOf(jml)+" - ("+String.valueOf(list.get(rowIndex).getListGudang().size())+")"; 
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "No";
            case 1:
                return "ID";
            case 2:
                return "Barcode";
            case 3:
                return "Nama Barang";
            case 4:
                return "Jumlah Cabang";
            case 5:
                return "Jumlah Gudang";
            default:
                return null;
        }
    }
    
    public JmlBarang getJmlBarangAt(int row){
        return list.get(row);
    }
    
    public void add(JmlBarang c){
        list.add(c);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
    public void delete(int index){
        list.remove(index);
        fireTableRowsDeleted(index, index);
    }
    public void update(){
        fireTableDataChanged();
    }
    public void update(Collection<JmlBarang> b){
        list.clear();
        list.addAll(b);
        fireTableDataChanged();
    }
}
