/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.tabel;

import com.blung.pos.entity.simple.SetoranHarian;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author lukman
 */
public class TabelSetoranHarian extends AbstractTableModel{
    List<tampil> list=new ArrayList<>();
    
    private class tampil{
        String data;
        Double datad;
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
        if(col < 2) return false;
        else return true;
    }
    
    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 10;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return rowIndex+1;
            case 1:
                return list.get(rowIndex).data;
            case 2:
                return list.get(rowIndex).datad;
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "No";
            case 1:
                return "Kegiatan";
            case 2:
                return "Jumlah";
            default:
                return null;
        }
    }
        
    public void update(){
        fireTableDataChanged();
    }
    
    public void update(SetoranHarian b){
        list.clear();
        list=formatedData(b);
        fireTableDataChanged();
    }
    
    private List<tampil> formatedData(SetoranHarian sh){
        List<tampil> ll=new ArrayList<>();
        tampil t;
        
        t=new tampil(); t.data="Pembelian"; t.datad=sh.getCekPembelian(); ll.add(t);
        t=new tampil(); t.data="Penjualan"; t.datad=sh.getCekPenjualan(); ll.add(t);
        t=new tampil(); t.data="Cicilan Hutang"; t.datad=sh.getCekCicilanHutang(); ll.add(t);
        t=new tampil(); t.data="Cicilan Piutang"; t.datad=sh.getCekCicilanPiutang(); ll.add(t);
        t=new tampil(); t.data="Hutang"; t.datad=sh.getCekHutang(); ll.add(t);
        t=new tampil(); t.data="Piutang"; t.datad=sh.getCekPiutang(); ll.add(t);
        t=new tampil(); t.data="Retur Pembelian"; t.datad=sh.getCekReturPembelian(); ll.add(t);
        t=new tampil(); t.data="Retur Penjualan"; t.datad=sh.getCekReturPenjualan(); ll.add(t);
        t=new tampil(); t.data="Barang Hilang"; t.datad=sh.getCekBarangHilang(); ll.add(t);
        
        return ll;
    }
}
