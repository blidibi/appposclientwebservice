/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.tabel;

import com.blung.pos.entity.simple.Karyawan;
import com.blung.pos.entity.simple.Kasir;
import com.blung.pos.entity.simple.LogLogin;
import com.blung.pos.entity.simple.User;
import com.blung.pos.ui.main.mainFrame;
import com.blung.pos.utility.FindObjectOnList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author lukman
 */
public class TabelLogLogin extends AbstractTableModel{

    List<LogLogin> list = new ArrayList<>();
    List<User> lu=mainFrame.msi.getAllUser();
    List<Kasir> lk=mainFrame.msi.getAllKasir();
    List<Karyawan> lkk=mainFrame.msi.getAllKaryawan();
    
    
    @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    @Override
    public boolean isCellEditable(int row, int col) {
        if(col < 2) return false;
        else return true;
    }
    
    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return rowIndex+1;
            case 1:
                return FindObjectOnList.findUserFromListById(lu, list.get(rowIndex).getIdUser()).getUsername();
            case 2:
                return FindObjectOnList.findKaryawanFromListById(lkk, FindObjectOnList.findUserFromListById(lu, list.get(rowIndex).getIdUser()).getIdKaryawan()).getNama();
            case 3:
                return FindObjectOnList.findKasirFromListById(lk, list.get(rowIndex).getIdKasir()).getNamaKasir();
            case 4:
                return list.get(rowIndex).getWaktuLogin();
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "No";
            case 1:
                return "Username";
            case 2:
                return "Nama";
            case 3:
                return "Kasir";
            case 4:
                return "Tanggal";
            default:
                return null;
        }
    }
    
    public LogLogin getLogLoginAt(int row){
        return list.get(row);
    }
    
    public void add(LogLogin c){
        list.add(c);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
    public void delete(int index){
        list.remove(index);
        fireTableRowsDeleted(index, index);
    }
    public void update(){
        fireTableDataChanged();
    }
    public void update(Collection<LogLogin> b){
        list.clear();
        list.addAll(b);
        fireTableDataChanged();
    }
    
}
