/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.tabel;

import com.blung.pos.entity.simple.Cabang;
import com.blung.pos.entity.simple.Kasir;
import com.blung.pos.entity.simple.Pembelian;
import com.blung.pos.entity.simple.Supliyer;
import com.blung.pos.entity.simple.User;
import com.blung.pos.ui.main.mainFrame;
import com.blung.pos.utility.FindObjectOnList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author lukman
 */
public class TabelPembelian extends AbstractTableModel{

    List<Pembelian> list=new ArrayList<>();
    List<Kasir> lk=mainFrame.msi.getAllKasir();
    List<Cabang> lc=mainFrame.msi.getAllCabang();
    List<Supliyer> ls=mainFrame.msi.getAllSupliyer();
    List<User> lu=mainFrame.msi.getAllUser();
    
    @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    @Override
    public boolean isCellEditable(int row, int col) {
        if(col < 2) return false;
        else return true;
    }
    
    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 9;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return rowIndex+1;
            case 1:
                return list.get(rowIndex).getTanggalBeli();
            case 2:
                return list.get(rowIndex).getNoFaktur();
            case 3:
                return list.get(rowIndex).getIdPembelian();
            case 4:
                return FindObjectOnList.findCabangFromListById(lc, FindObjectOnList.findKasirFromListById(lk, list.get(rowIndex).getIdKasir()).getIdCabang()).getNamaCabang();
            case 5:
                return FindObjectOnList.findSupliyerFromListById(ls, list.get(rowIndex).getIdSupliyer()).getNamaSupliyer();
            case 6:
                return list.get(rowIndex).getTotalHarga();
            case 7:
                return FindObjectOnList.findUserFromListById(lu, list.get(rowIndex).getIdUser()).getUsername();
            case 8:
                return list.get(rowIndex).getKeterangan();
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "No";
            case 1:
                return "Tgl Beli";
            case 2:
                return "No Faktur";
            case 3:
                return "ID Pembelian";
            case 4:
                return "Cabang";
            case 5:
                return "Supliyer";
            case 6:
                return "Total Harga";
            case 7:
                return "Username";
            case 8:
                return "Keterangan";
            default:
                return null;
        }
    }
    
    public Pembelian getPembelianAt(int row){
        return list.get(row);
    }
    
    public void add(Pembelian c){
        list.add(c);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
    public void delete(int index){
        list.remove(index);
        fireTableRowsDeleted(index, index);
    }
    public void update(){
        fireTableDataChanged();
    }
    public void update(Collection<Pembelian> b){
        list.clear();
        list.addAll(b);
        fireTableDataChanged();
    }
    
}
