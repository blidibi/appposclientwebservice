/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.tabel;

import com.blung.pos.entity.simple.Barang;
import com.blung.pos.entity.simple.Kategori;
import com.blung.pos.entity.simple.Supliyer;
import com.blung.pos.ui.main.mainFrame;
import com.blung.pos.utility.FindObjectOnList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author lukman
 */
public class TabelBarang extends AbstractTableModel{

    List<Barang> list = new ArrayList<>();
    List<Supliyer> ls=mainFrame.msi.getAllSupliyer();
    List<Kategori> lk=mainFrame.msi.getAllKategori();
    
    @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    @Override
    public boolean isCellEditable(int row, int col) {
        if(col < 2) return false;
        else return true;
    }
    
    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 12;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
         switch(columnIndex){
            case 0:
                return rowIndex+1;
            case 1:
                return list.get(rowIndex).getIdBarangBarcode();
            case 2:
                return list.get(rowIndex).getNamaBarang();
            case 3:
                return list.get(rowIndex).getHarga().getHargaBeliTerakhir();
            case 4:
                return list.get(rowIndex).getHarga().getHargaBeliRata();
            case 5:
                return list.get(rowIndex).getHarga().getHargaSatu();
            case 6:
                return list.get(rowIndex).getHarga().getHargaDua();
            case 7:
                return list.get(rowIndex).getHarga().getHargaTiga();
            case 8:
                return list.get(rowIndex).getHarga().getHargaEmpat();
            case 9:
                return list.get(rowIndex).getHarga().getDiskonPromo();
            case 10:
                return FindObjectOnList.findSupliyerFromListById(ls, list.get(rowIndex).getHarga().getIdSupliyer()).getNamaSupliyer();
            case 11:
                return FindObjectOnList.findKategoriFromListById(lk, list.get(rowIndex).getHarga().getIdKategori()).getNamaKategori();
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "No";
            case 1:
                return "Barcode";
            case 2:
                return "Nama Barang";
            case 3:
                return "Harga Beli";
            case 4:
                return "Harga Rata2";
            case 5:
                return "Harga 1";
            case 6:
                return "Harga 2";
            case 7:
                return "Harga 3";
            case 8:
                return "Harga 4";
            case 9:
                return "DiskonPromo";
            case 10:
                return "Supliyer";
            case 11:
                return "Kategori";
            default:
                return null;
        }
    }
    
    public Barang getBarangAt(int row){
        return list.get(row);
    }
    
    public void add(Barang c){
        list.add(c);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
    public void delete(int index){
        list.remove(index);
        fireTableRowsDeleted(index, index);
    }
    public void update(){
        fireTableDataChanged();
    }
    public void update(Collection<Barang> b){
        list.clear();
        list.addAll(b);
        fireTableDataChanged();
    }
    
}
