/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.tabel;

import com.blung.pos.entity.simple.CicilanHutangPembelian;
import com.blung.pos.entity.simple.HutangPembelian;
import com.blung.pos.entity.simple.Pembelian;
import com.blung.pos.entity.simple.Supliyer;
import com.blung.pos.ui.main.mainFrame;
import com.blung.pos.utility.FindObjectOnList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author user
 */
public class TabelHutangPembelian extends AbstractTableModel{

    public TabelHutangPembelian(Date start, Date end) {
        System.out.println("halo" + start);
        list1 = mainFrame.toi.getPembelianByCabang(mainFrame.cabang.getIdCabang(), start, end);
        lS=mainFrame.msi.getAllSupliyer();
        lcicilan = mainFrame.tsi.getCicilanHutangPembelianByCabangAndDate(start, end, mainFrame.cabang.getIdCabang());
    }
    
    List<HutangPembelian> list = new ArrayList<>();
    List<Pembelian> list1=new ArrayList<>();
    List<Supliyer> lS=new ArrayList<>();
    List<CicilanHutangPembelian> lcicilan = new ArrayList<>();
    
    @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    @Override
    public boolean isCellEditable(int row, int col) {
        if(col < 2) return false;
        else return true;
    }
    
    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 8;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            
            case 0:
                return rowIndex+1;
            case 1:
                return list.get(rowIndex).getIdHutangPembelian();
            case 2:
                return FindObjectOnList.findPembelianFromListById(list1, list.get(rowIndex).getIdPembelian()).getNoFaktur();
            case 3:
                Double totcil=Double.valueOf("0");
                for(CicilanHutangPembelian cp:lcicilan){
                    if(cp.getIdHutangPembelian().equals(list.get(rowIndex).getIdHutangPembelian())){
                        totcil+=cp.getJumlahBayar();
                    }
                }
                return String.valueOf(FindObjectOnList.findPembelianFromListById(list1, list.get(rowIndex).getIdPembelian()).getTotalHarga())+"("+String.valueOf(totcil)+")";
            case 4:
                return FindObjectOnList.findPembelianFromListById(list1, list.get(rowIndex).getIdPembelian()).getTanggalBeli();
            case 5:
                return FindObjectOnList.findSupliyerFromListById(lS, FindObjectOnList.findPembelianFromListById(list1, list.get(rowIndex).getIdPembelian()).getIdSupliyer()).getNamaSupliyer();
            case 6:
                return list.get(rowIndex).getKeterangan();
            case 7:
                Boolean status=list.get(rowIndex).getStatusLunas();
                if(status==true) return "lunas"; else return "belum";
            default:
                return null;
            
        }
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "No";
            case 1:
                return "ID Hutang";
            case 2:
                return "No Faktur";
            case 3:
                return "Total(Bayar)";
            case 4:
                return "Tanggal";
            case 5:
                return "Supliyer";
            case 6:
                return "Keterangan";
            case 7:
                return "Status";
            default:
                return null;
        }
    }
    
    public HutangPembelian getHutangPembelianAt(int row){
        return list.get(row);
    }
    
    public void add(HutangPembelian c){
        list.add(c);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
    public void delete(int index){
        list.remove(index);
        fireTableRowsDeleted(index, index);
    }
    public void update(){
        fireTableDataChanged();
    }
    public void update(Collection<HutangPembelian> b){
        list.clear();
        list.addAll(b);
        fireTableDataChanged();
    }
    
}
