/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.tabel;

import com.blung.pos.entity.simple.Absensi;
import com.blung.pos.entity.simple.Karyawan;
import com.blung.pos.ui.main.mainFrame;
import com.blung.pos.utility.FindObjectOnList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author lukman
 */
public class TabelAbsensi extends AbstractTableModel{

    List<Absensi> list = new ArrayList<>();
    List<Karyawan> lk=mainFrame.msi.getAllKaryawan();
    
    @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    @Override
    public boolean isCellEditable(int row, int col) {
        if(col < 2) return false;
        else return true;
    }
    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return rowIndex+1;
            case 1:
                return list.get(rowIndex).getTanggalAbsen().toString();
            case 2:
                return list.get(rowIndex).getJamMasuk().toString();
            case 3:
                return list.get(rowIndex).getJamPulang().toString();
            case 4:
                return FindObjectOnList.findKaryawanFromListById(lk, list.get(rowIndex).getIdKaryawan()).getNama();
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "No";
            case 1:
                return "Tanggal";
            case 2:
                return "Jam Masuk";
            case 3:
                return "Jam Pulang";
            case 4:
                return "Nama";
            default:
                return null;
        }
    }
    
    public Absensi getCabangAt(int row){
        return list.get(row);
    }
    
    public void add(Absensi c){
        list.add(c);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
    public void delete(int index){
        list.remove(index);
        fireTableRowsDeleted(index, index);
    }
    public void update(){
        fireTableDataChanged();
    }
    public void update(Collection<Absensi> b){
        list.clear();
        list.addAll(b);
        fireTableDataChanged();
    }
    
}
