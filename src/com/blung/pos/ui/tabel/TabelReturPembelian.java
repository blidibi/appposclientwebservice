/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.tabel;


import com.blung.pos.entity.simple.Cabang;
import com.blung.pos.entity.simple.Karyawan;
import com.blung.pos.entity.simple.Kasir;
import com.blung.pos.entity.simple.ReturPembelian;
import com.blung.pos.entity.simple.User;
import com.blung.pos.ui.main.mainFrame;
import com.blung.pos.utility.FindObjectOnList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author lukman
 */
public class TabelReturPembelian extends AbstractTableModel{
    List<ReturPembelian> list = new ArrayList<>();
    List<Cabang> lcabang=mainFrame.msi.getAllCabang();
    List<Kasir> lkasir=mainFrame.msi.getAllKasirByCabang(mainFrame.cabang);
    List<User> lusr=mainFrame.msi.getAllUserByCabang(mainFrame.cabang);
    List<Karyawan> lkar=mainFrame.msi.getAllKaryawanByCabang(mainFrame.cabang);
    
    @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    @Override
    public boolean isCellEditable(int row, int col) {
        if(col < 2) return false;
        else return true;
    }
    
    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            
            case 0:
                return rowIndex+1;
            case 1:
                return FindObjectOnList.findCabangFromListById(lcabang, FindObjectOnList.findKasirFromListById(lkasir, list.get(rowIndex).getIdKasir()).getIdCabang());
            case 2:
                return list.get(rowIndex).getIdPembelian();
            case 3:
                return FindObjectOnList.findKaryawanFromListById(lkar, FindObjectOnList.findUserFromListById(lusr, list.get(rowIndex).getIdUser()).getIdKaryawan());
            case 4:
                return list.get(rowIndex).getTanggal();
            default:
                return null;
            
        }
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "No";
            case 1:
                return "Cabang";
            case 2:
                return "Id Pembelian";
            case 3:
                return "Karyawan";
            case 4:
                return "Tanggal";
            default:
                return null;
        }
    }
    
    public ReturPembelian getReturPembelianAt(int row){
        return list.get(row);
    }
    
    public void add(ReturPembelian c){
        list.add(c);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
    public void delete(int index){
        list.remove(index);
        fireTableRowsDeleted(index, index);
    }
    public void update(){
        fireTableDataChanged();
    }
    public void update(Collection<ReturPembelian> b){
        list.clear();
        list.addAll(b);
        fireTableDataChanged();
    }
}
