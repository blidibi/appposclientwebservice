/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.tabel;

import com.blung.pos.entity.simple.Cabang;
import com.blung.pos.entity.simple.Kasir;
import com.blung.pos.entity.simple.Pelanggan;
import com.blung.pos.entity.simple.Penjualan;
import com.blung.pos.entity.simple.User;
import com.blung.pos.ui.main.mainFrame;
import com.blung.pos.utility.FindObjectOnList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author lukman
 */
public class TabelPenjualan extends AbstractTableModel{
    
    List<Penjualan> list = new ArrayList<>();
    List<User> lu=mainFrame.msi.getAllUser();
    List<Kasir> lk=mainFrame.msi.getAllKasir();
    List<Cabang> lc=mainFrame.msi.getAllCabang();
    List<Pelanggan> lp=mainFrame.msi.getAllPelanggan();
    
    @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    @Override
    public boolean isCellEditable(int row, int col) {
        if(col < 2) return false;
        else return true;
    }
    
    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 10;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return rowIndex+1;
            case 1:
                return list.get(rowIndex).getTanggalJual();
            case 2:
                return list.get(rowIndex).getIdPenjualan();
            case 3:
                return FindObjectOnList.findUserFromListById(lu, list.get(rowIndex).getIdUser()).getUsername();
            case 4:
                return FindObjectOnList.findKasirFromListById(lk, list.get(rowIndex).getIdKasir()).getNamaKasir();
            case 5:
                return FindObjectOnList.findCabangFromListById(lc, FindObjectOnList.findKasirFromListById(lk, list.get(rowIndex).getIdKasir()).getIdCabang()).getNamaCabang();
            case 6:
                return FindObjectOnList.findPelangganFromListById(lp, list.get(rowIndex).getIdPelanggan()).getNama();
            case 7:
                return list.get(rowIndex).getDiskonPelanggan();
            case 8:
                return list.get(rowIndex).getPotonganLangsung();
            case 9:
                return list.get(rowIndex).getTotalHarga();
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "No";
            case 1:
                return "Tanggal";
            case 2:
                return "ID";
            case 3:
                return "User";
            case 4:
                return "Kasir";
            case 5:
                return "Cabang";
            case 6:
                return "Pelanggan";
            case 7:
                return "DiskonPelanggan";
            case 8:
                return "PotonganLangsung";
            case 9:
                return "TotalHarga";
            default:
                return null;
        }
    }
    
    public Penjualan getKaryawanAt(int row){
        return list.get(row);
    }
    
    public void add(Penjualan c){
        list.add(c);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
    public void delete(int index){
        list.remove(index);
        fireTableRowsDeleted(index, index);
    }
    public void update(){
        fireTableDataChanged();
    }
    public void update(Collection<Penjualan> b){
        list.clear();
        list.addAll(b);
        fireTableDataChanged();
    }
    
}
