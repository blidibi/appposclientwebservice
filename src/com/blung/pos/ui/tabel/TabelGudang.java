/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.tabel;

import com.blung.pos.entity.simple.Cabang;
import com.blung.pos.entity.simple.Gudang;
import com.blung.pos.ui.main.mainFrame;
import com.blung.pos.utility.FindObjectOnList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author user
 */
public class TabelGudang extends AbstractTableModel{
    
    List<Gudang> list = new ArrayList<>();
    List<Cabang> list1 = mainFrame.msi.getAllCabang();
    
    @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    @Override
    public boolean isCellEditable(int row, int col) {
        if(col < 2) return false;
        else return true;
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            
            case 0:
                return rowIndex+1;
            case 1:
                return list.get(rowIndex).getNamaGudang();
            case 2:
                return FindObjectOnList.findCabangFromListById(list1, list.get(rowIndex).getIdCabang()).getNamaCabang();
            default:
                return null;
            
        }
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "No";
            case 1:
                return "Nama Gudang";
            case 2:
                return "Nama Cabang";
            default:
                return null;
        }
    }
    
    public Gudang getGudangAt(int row){
        return list.get(row);
    }
    
    public void add(Gudang c){
        list.add(c);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
    public void delete(int index){
        list.remove(index);
        fireTableRowsDeleted(index, index);
    }
    public void update(){
        fireTableDataChanged();
    }
    public void update(Collection<Gudang> b){
        list.clear();
        list.addAll(b);
        fireTableDataChanged();
    }
    
}
