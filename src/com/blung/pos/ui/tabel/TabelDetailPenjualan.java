/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.tabel;

import com.blung.pos.entity.simple.Barang;
import com.blung.pos.entity.simple.DetailPenjualan;
import com.blung.pos.ui.main.mainFrame;
import com.blung.pos.utility.FindObjectOnList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author user
 */
public class TabelDetailPenjualan extends AbstractTableModel{
    
    List<DetailPenjualan> list = new ArrayList<>();
    List<Barang> list1 = mainFrame.msi.getAllBarangByCabang(mainFrame.cabang);

    
    @Override
    public boolean isCellEditable(int row, int col) {
        if(col < 2) return false;
        else return true;
    }
    
    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            
            case 0:
                return rowIndex+1;
            case 1:
                return FindObjectOnList.findBarangFromListById(list1, list.get(rowIndex).getIdBarang()).getNamaBarang();
            case 2:
                return list.get(rowIndex).getHarga();
            case 3:
                return list.get(rowIndex).getDiskonRupiah();
            case 4:
                return list.get(rowIndex).getJumlah();
            case 5:
                return list.get(rowIndex).getSubTotal();
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "No";
            case 1:
                return "Nama Barang";
            case 2:
                return "Harga Barang";
            case 3:
                return "Diskon";
            case 4:
                return "Jumlah";
            case 5:
                return "Total";
            default:
                return null;
        }
    }
    
    public DetailPenjualan getDetailPenjualanAt(int row){
        return list.get(row);
    }
    
    public void add(DetailPenjualan c){
        list.add(c);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
    public void delete(int index){
        list.remove(index);
        fireTableRowsDeleted(index, index);
    }
    public void update(){
        fireTableDataChanged();
    }
    public void update(Collection<DetailPenjualan> b){
        list.clear();
        list.addAll(b);
        fireTableDataChanged();
    }
    
}
