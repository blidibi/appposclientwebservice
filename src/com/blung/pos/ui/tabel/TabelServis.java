/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.tabel;

import com.blung.pos.entity.simple.DetailServis;
import com.blung.pos.entity.simple.Servis;
import com.blung.pos.ui.main.mainFrame;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author lukman
 */
public class TabelServis extends AbstractTableModel{
    
    public TabelServis(Date start, Date end) {
        ld=mainFrame.tsi.detailservisByCabangAndDate(start, end, mainFrame.cabang);
    }
    
    List<Servis> list = new ArrayList<>();
    List<DetailServis> ld= new ArrayList<>();

    public List<DetailServis> getLd() {
        return ld;
    }
    
    @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    @Override
    public boolean isCellEditable(int row, int col) {
        if(col < 2) return false;
        else return true;
    }
    
    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            
            case 0:
                return rowIndex+1;
            case 1:
                return list.get(rowIndex).getIdServis();
            case 2:
                return list.get(rowIndex).getNamaDevice();
            case 3:
                return list.get(rowIndex).getNamaPelanggan();
            case 4:
                return list.get(rowIndex).getTanggalJadi();
            case 5:
                Double tot=Double.valueOf("0");
                for(DetailServis ds:ld){
                    if(ds.getIdServis().equals(list.get(rowIndex).getIdServis())){
                        tot+=ds.getHarga();
                    }
                }
                return tot;
            default:
                return null;
            
        }
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "No";
            case 1:
                return "ID";
            case 2:
                return "Device";
            case 3:
                return "Nama Pelanggan";
            case 4:
                return "Tgl Jadi";
            case 5:
                return "Harga";
            default:
                return null;
        }
    }
    
    public Servis getServisAt(int row){
        return list.get(row);
    }
    
    public void add(Servis c){
        list.add(c);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
    public void delete(int index){
        list.remove(index);
        fireTableRowsDeleted(index, index);
    }
    public void update(){
        fireTableDataChanged();
    }
    public void update(Collection<Servis> b){
        list.clear();
        list.addAll(b);
        fireTableDataChanged();
    }
}
