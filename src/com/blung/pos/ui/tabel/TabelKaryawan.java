/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.tabel;

import com.blung.pos.entity.simple.Cabang;
import com.blung.pos.entity.simple.Karyawan;
import com.blung.pos.ui.main.mainFrame;
import com.blung.pos.utility.FindObjectOnList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author lukman
 */
public class TabelKaryawan extends AbstractTableModel{

    List<Karyawan> list=new ArrayList<>();
    List<Cabang> lc=mainFrame.msi.getAllCabang();
    
    @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    @Override
    public boolean isCellEditable(int row, int col) {
        if(col < 2) return false;
        else return true;
    }
    
    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return rowIndex+1;
            case 1:
                return list.get(rowIndex).getNama();
            case 2:
                return list.get(rowIndex).getNoID();
            case 3:
                return list.get(rowIndex).getNoTlp();
            case 4:
                return list.get(rowIndex).getAlamat();
            case 5:
                return FindObjectOnList.findCabangFromListById(lc, list.get(rowIndex).getIdCabang()).getNamaCabang();
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "No";
            case 1:
                return "Nama";
            case 2:
                return "No ID";
            case 3:
                return "No Tlp";
            case 4:
                return "Alamat";
            case 5:
                return "Cabang";
            default:
                return null;
        }
    }
    
    public Karyawan getKaryawanAt(int row){
        return list.get(row);
    }
    
    public void add(Karyawan c){
        list.add(c);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
    public void delete(int index){
        list.remove(index);
        fireTableRowsDeleted(index, index);
    }
    public void update(){
        fireTableDataChanged();
    }
    public void update(Collection<Karyawan> b){
        list.clear();
        list.addAll(b);
        fireTableDataChanged();
    }
    
}
