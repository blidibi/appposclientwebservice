/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.tabel;

import com.blung.pos.entity.bantuan.DetailJumlahBarang;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author lukman
 */
public class TabelDetailJumlahBarang extends AbstractTableModel{
    
    List<DetailJumlahBarang> list =new ArrayList<>();
    
    @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    @Override
    public boolean isCellEditable(int row, int col) {
        if(col < 2) return false;
        else return true;
    }
    
    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
         switch(columnIndex){
            case 0:
                return list.get(rowIndex).getParam();
            case 1:
                return list.get(rowIndex).getValue();
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "Tempat";
            case 1:
                return "Jumlah Barang";
            default:
                return null;
        }
    }
    
    public DetailJumlahBarang getDetailJumlahBarangAt(int row){
        return list.get(row);
    }
    
    public void add(DetailJumlahBarang c){
        list.add(c);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
    public void delete(int index){
        list.remove(index);
        fireTableRowsDeleted(index, index);
    }
    public void update(){
        fireTableDataChanged();
    }
    public void update(Collection<DetailJumlahBarang> b){
        list.clear();
        list.addAll(b);
        fireTableDataChanged();
    }
}
