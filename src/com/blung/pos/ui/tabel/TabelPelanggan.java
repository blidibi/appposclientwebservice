/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.tabel;

import com.blung.pos.entity.simple.KategoriPelanggan;
import com.blung.pos.entity.simple.Pelanggan;
import com.blung.pos.ui.main.mainFrame;
import com.blung.pos.utility.FindObjectOnList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author lukman
 */
public class TabelPelanggan extends AbstractTableModel{

    List<Pelanggan> list = new ArrayList<>();
    List<KategoriPelanggan> lkp=mainFrame.msi.getAllKategoriPelanggan();
    
    @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    @Override
    public boolean isCellEditable(int row, int col) {
        if(col < 2) return false;
        else return true;
    }
    
    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return rowIndex+1;
            case 1:
                return list.get(rowIndex).getNama();
            case 2:
                return list.get(rowIndex).getNoID();
            case 3:
                return list.get(rowIndex).getNoTlpPelanggan();
            case 4:
                return list.get(rowIndex).getAlamat();
            case 5:
                return FindObjectOnList.findKategoriPelangganiFromListById(lkp, list.get(rowIndex).getIdKategoriPelanggan()).getKeterangan();
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "No";
            case 1:
                return "Nama";
            case 2:
                return "No ID";
            case 3:
                return "No Tlp";
            case 4:
                return "Alamat";
            case 5:
                return "Kategori";
            default:
                return null;
        }
    }
    
    public Pelanggan getPelangganAt(int row){
        return list.get(row);
    }
    
    public void add(Pelanggan c){
        list.add(c);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
    public void delete(int index){
        list.remove(index);
        fireTableRowsDeleted(index, index);
    }
    public void update(){
        fireTableDataChanged();
    }
    public void update(Collection<Pelanggan> b){
        list.clear();
        list.addAll(b);
        fireTableDataChanged();
    }
    
}
