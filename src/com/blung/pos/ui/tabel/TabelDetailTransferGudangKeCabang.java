/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.tabel;

import com.blung.pos.entity.simple.Barang;
import com.blung.pos.entity.simple.DetailTransferGudangKeCabang;
import com.blung.pos.ui.main.mainFrame;
import com.blung.pos.utility.FindObjectOnList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author user
 */
public class TabelDetailTransferGudangKeCabang extends AbstractTableModel{
    
    List<DetailTransferGudangKeCabang> list = new ArrayList<>();
    List<Barang> list1 = mainFrame.msi.getAllBarangByCabang(mainFrame.cabang);

    @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    @Override
    public boolean isCellEditable(int row, int col) {
        if(col < 2) return false;
        else return true;
    }
    
    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            
            case 0:
                return rowIndex+1;
            case 1:
                return FindObjectOnList.findBarangFromListById(list1, list.get(rowIndex).getIdBarang()).getNamaBarang();
            case 2:
                return list.get(rowIndex).getJumlah();
            default:
                return null;
            
        }
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "No";
            case 1:
                return "Nama Barang";
            case 2:
                return "Jumlah";
            default:
                return null;
        }
    }
    
    public DetailTransferGudangKeCabang getDetailTransferGudangKeCabangAt(int row){
        return list.get(row);
    }
    
    public void add(DetailTransferGudangKeCabang c){
        list.add(c);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
    public void delete(int index){
        list.remove(index);
        fireTableRowsDeleted(index, index);
    }
    public void update(){
        fireTableDataChanged();
    }
    public void update(Collection<DetailTransferGudangKeCabang> b){
        list.clear();
        list.addAll(b);
        fireTableDataChanged();
    }
    
}
