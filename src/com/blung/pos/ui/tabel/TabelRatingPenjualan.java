/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.ui.tabel;

import com.blung.pos.entity.simple.Barang;
import com.blung.pos.entity.simple.DetailPenjualan;
import com.blung.pos.ui.main.mainFrame;
import com.blung.pos.utility.FindObjectOnList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author lukman
 */
public class TabelRatingPenjualan extends AbstractTableModel{

    private List<DetailPenjualan> l = new ArrayList<>();
    private List<Barang> lb=mainFrame.msi.getAllBarangByCabang(mainFrame.cabang);
    
    ///////////////////////////////////////////////////
    private boolean DEBUG = false;
    
    @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    @Override
    public boolean isCellEditable(int row, int col) {
        if(col < 2) return false;
        else return true;
    }
    //////////////////////////////////////////////////
    
    @Override
    public int getRowCount() {
        return l.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return rowIndex+1;
            case 1:
                return FindObjectOnList.findBarangFromListById(lb, l.get(rowIndex).getIdBarang()).getNamaBarang();
            case 2:
                return l.get(rowIndex).getJumlah();
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0:
                return "No";
            case 1:
                return "ID Barang";
            case 2:
                return "Nama Barang";
            case 3:
                return "Jumlah";
            default:
                return null;
        }
    }
    
    public DetailPenjualan getDetailPenjualanAt(int row){
        return l.get(row);
    }
    
    public void add(DetailPenjualan dp){
        l.add(dp);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
    
    public void delete(int index){
        l.remove(index);
        fireTableRowsDeleted(index, index);
    }
    
    public void update(){
        fireTableDataChanged();
    }
    
    public void update(Collection<DetailPenjualan> dp){
        l.clear();
        l.addAll(dp);
        fireTableDataChanged();
    }    
}
