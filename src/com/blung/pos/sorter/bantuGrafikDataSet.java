/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.sorter;

import java.util.Date;

/**
 *
 * @author lukman
 */
public class bantuGrafikDataSet {
    private Double data1;
    private String data2;
    private Date data3;   

    public Double getData1() {
        return data1;
    }

    public void setData1(Double data1) {
        this.data1 = data1;
    }

    public String getData2() {
        return data2;
    }

    public void setData2(String data2) {
        this.data2 = data2;
    }

    public Date getData3() {
        return data3;
    }

    public void setData3(Date data3) {
        this.data3 = data3;
    }
    
}
