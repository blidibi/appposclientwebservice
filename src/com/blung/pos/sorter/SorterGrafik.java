/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.sorter;

import java.util.Comparator;

/**
 *
 * @author lukman
 */
public class SorterGrafik implements Comparator<bantuGrafikDataSet> {

    @Override
    public int compare(bantuGrafikDataSet o1, bantuGrafikDataSet o2) {
        return o1.getData3().compareTo(o2.getData3());
    }

}
