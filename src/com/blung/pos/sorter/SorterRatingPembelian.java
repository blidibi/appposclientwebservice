/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.sorter;

import com.blung.pos.entity.simple.DetailPembelian;
import java.util.Comparator;

/**
 *
 * @author lukman
 */
public class SorterRatingPembelian implements Comparator<DetailPembelian> {

    @Override
    public int compare(DetailPembelian o1, DetailPembelian o2) {
        return o2.getJumlah() - o1.getJumlah();
    }
    
}
