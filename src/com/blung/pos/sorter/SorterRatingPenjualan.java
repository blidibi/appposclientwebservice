/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.sorter;

import com.blung.pos.entity.simple.DetailPenjualan;
import java.util.Comparator;

/**
 *
 * @author lukman
 */
public class SorterRatingPenjualan implements Comparator<DetailPenjualan> {

    @Override
    public int compare(DetailPenjualan o1, DetailPenjualan o2) {
        return o2.getJumlah() - o1.getJumlah();
    }
    
}
