/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.bantuan;

import java.util.List;

/**
 *
 * @author lukman
 */
public class JmlBarang {
    private Long idBarang;
    private String namaBarang;
    private Long jumlahBarangCabang;
    List<JmlGudang> listGudang;

    public Long getIdBarang() {
        return idBarang;
    }

    public void setIdBarang(Long idBarang) {
        this.idBarang = idBarang;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public Long getJumlahBarangCabang() {
        return jumlahBarangCabang;
    }

    public void setJumlahBarangCabang(Long jumlahBarangCabang) {
        this.jumlahBarangCabang = jumlahBarangCabang;
    }

    public List<JmlGudang> getListGudang() {
        return listGudang;
    }

    public void setListGudang(List<JmlGudang> listGudang) {
        this.listGudang = listGudang;
    }
}
