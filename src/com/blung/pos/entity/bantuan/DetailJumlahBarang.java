/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.bantuan;

import com.blung.pos.entity.simple.Gudang;
import com.blung.pos.ui.main.mainFrame;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lukman
 */
public class DetailJumlahBarang {
    private String param;
    private Long value;
    private List<Gudang> lg=mainFrame.msi.getGudangByCabang(mainFrame.cabang);
    private JmlBarang jbb;

    public void setJbb(JmlBarang jbb) {
        this.jbb = jbb;
    }
    
    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }
   
    
    public List<DetailJumlahBarang> getData(){
        List<DetailJumlahBarang> list=new ArrayList<>();
        DetailJumlahBarang dtb=new DetailJumlahBarang();
        dtb.setParam("jumlahCabang");
        dtb.setValue(jbb.getJumlahBarangCabang());
        list.add(dtb);
        for(JmlGudang jg:jbb.getListGudang()){
            dtb=new DetailJumlahBarang();
            dtb.setParam(jg.getNamaGudang());
            dtb.setValue(jg.getJumlahBarangGudang());
            list.add(dtb);
        }
        return list;
    } 
}
