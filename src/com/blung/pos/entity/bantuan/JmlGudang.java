/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.bantuan;

/**
 *
 * @author lukman
 */
public class JmlGudang {
    private String namaGudang;
    private Long jumlahBarangGudang;

    public String getNamaGudang() {
        return namaGudang;
    }

    public void setNamaGudang(String namaGudang) {
        this.namaGudang = namaGudang;
    }

    public Long getJumlahBarangGudang() {
        return jumlahBarangGudang;
    }

    public void setJumlahBarangGudang(Long jumlahBarangGudang) {
        this.jumlahBarangGudang = jumlahBarangGudang;
    }
}
