/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.bantuan;

/**
 *
 * @author lukman
 */
public class Previlage {
    private Boolean m1;
    private Boolean m11;
    private Boolean m12;
    private Boolean m13;
    private Boolean m14;
    private Boolean m15;
    private Boolean m16;
    private Boolean m161;
    private Boolean m162;
    private Boolean m17;
    private Boolean m2;
    private Boolean m21;
    private Boolean m211;
    private Boolean m212;
    private Boolean m22;
    private Boolean m221;
    private Boolean m222;
    private Boolean m23;
    private Boolean m231;
    private Boolean m232;
    private Boolean m233;
    private Boolean m24;
    private Boolean m241;
    private Boolean m242;
    private Boolean m25;
    private Boolean m26;
    private Boolean m3;
    private Boolean m31=false;
    private Boolean m32;
    private Boolean m33;
    private Boolean m34;
    private Boolean m341;
    private Boolean m342;
    private Boolean m343;
    private Boolean m35;
    private Boolean m351;
    private Boolean m352;
    private Boolean m353;
    private Boolean m36;
    private Boolean m37;
    private Boolean m371;
    private Boolean m372;
    private Boolean m38;
    private Boolean m381;
    private Boolean m382;
    private Boolean m39;
    private Boolean m4;
    private Boolean m41;
    private Boolean m42;
    private Boolean m5;
    private Boolean m51;
    private Boolean m52;

    public Boolean getM1() {
        return m1;
    }

    public void setM1(Boolean m1) {
        this.m1 = m1;
    }

    public Boolean getM11() {
        return m11;
    }

    public void setM11(Boolean m11) {
        this.m11 = m11;
    }

    public Boolean getM12() {
        return m12;
    }

    public void setM12(Boolean m12) {
        this.m12 = m12;
    }

    public Boolean getM13() {
        return m13;
    }

    public void setM13(Boolean m13) {
        this.m13 = m13;
    }

    public Boolean getM14() {
        return m14;
    }

    public void setM14(Boolean m14) {
        this.m14 = m14;
    }

    public Boolean getM15() {
        return m15;
    }

    public void setM15(Boolean m15) {
        this.m15 = m15;
    }

    public Boolean getM16() {
        return m16;
    }

    public void setM16(Boolean m16) {
        this.m16 = m16;
    }

    public Boolean getM161() {
        return m161;
    }

    public void setM161(Boolean m161) {
        this.m161 = m161;
    }

    public Boolean getM162() {
        return m162;
    }

    public void setM162(Boolean m162) {
        this.m162 = m162;
    }

    public Boolean getM17() {
        return m17;
    }

    public void setM17(Boolean m17) {
        this.m17 = m17;
    }

    public Boolean getM2() {
        return m2;
    }

    public void setM2(Boolean m2) {
        this.m2 = m2;
    }

    public Boolean getM21() {
        return m21;
    }

    public void setM21(Boolean m21) {
        this.m21 = m21;
    }

    public Boolean getM211() {
        return m211;
    }

    public void setM211(Boolean m211) {
        this.m211 = m211;
    }

    public Boolean getM212() {
        return m212;
    }

    public void setM212(Boolean m212) {
        this.m212 = m212;
    }

    public Boolean getM22() {
        return m22;
    }

    public void setM22(Boolean m22) {
        this.m22 = m22;
    }

    public Boolean getM221() {
        return m221;
    }

    public void setM221(Boolean m221) {
        this.m221 = m221;
    }

    public Boolean getM222() {
        return m222;
    }

    public void setM222(Boolean m222) {
        this.m222 = m222;
    }

    public Boolean getM23() {
        return m23;
    }

    public void setM23(Boolean m23) {
        this.m23 = m23;
    }

    public Boolean getM231() {
        return m231;
    }

    public void setM231(Boolean m231) {
        this.m231 = m231;
    }

    public Boolean getM232() {
        return m232;
    }

    public void setM232(Boolean m232) {
        this.m232 = m232;
    }

    public Boolean getM233() {
        return m233;
    }

    public void setM233(Boolean m233) {
        this.m233 = m233;
    }

    public Boolean getM24() {
        return m24;
    }

    public void setM24(Boolean m24) {
        this.m24 = m24;
    }

    public Boolean getM241() {
        return m241;
    }

    public void setM241(Boolean m241) {
        this.m241 = m241;
    }

    public Boolean getM242() {
        return m242;
    }

    public void setM242(Boolean m242) {
        this.m242 = m242;
    }

    public Boolean getM25() {
        return m25;
    }

    public void setM25(Boolean m25) {
        this.m25 = m25;
    }

    public Boolean getM26() {
        return m26;
    }

    public void setM26(Boolean m26) {
        this.m26 = m26;
    }

    public Boolean getM3() {
        return m3;
    }

    public void setM3(Boolean m3) {
        this.m3 = m3;
    }

    public Boolean getM31() {
        return false;
    }

    public void setM31(Boolean m31) {
        this.m31 = false;
    }

    public Boolean getM32() {
        return m32;
    }

    public void setM32(Boolean m32) {
        this.m32 = m32;
    }

    public Boolean getM33() {
        return m33;
    }

    public void setM33(Boolean m33) {
        this.m33 = m33;
    }

    public Boolean getM34() {
        return m34;
    }

    public void setM34(Boolean m34) {
        this.m34 = m34;
    }

    public Boolean getM341() {
        return m341;
    }

    public void setM341(Boolean m341) {
        this.m341 = m341;
    }

    public Boolean getM342() {
        return m342;
    }

    public void setM342(Boolean m342) {
        this.m342 = m342;
    }

    public Boolean getM343() {
        return m343;
    }

    public void setM343(Boolean m343) {
        this.m343 = m343;
    }

    public Boolean getM35() {
        return m35;
    }

    public void setM35(Boolean m35) {
        this.m35 = m35;
    }

    public Boolean getM351() {
        return m351;
    }

    public void setM351(Boolean m351) {
        this.m351 = m351;
    }

    public Boolean getM352() {
        return m352;
    }

    public void setM352(Boolean m352) {
        this.m352 = m352;
    }

    public Boolean getM353() {
        return m353;
    }

    public void setM353(Boolean m353) {
        this.m353 = m353;
    }

    public Boolean getM36() {
        return m36;
    }

    public void setM36(Boolean m36) {
        this.m36 = m36;
    }

    public Boolean getM37() {
        return m37;
    }

    public void setM37(Boolean m37) {
        this.m37 = m37;
    }

    public Boolean getM371() {
        return m371;
    }

    public void setM371(Boolean m371) {
        this.m371 = m371;
    }

    public Boolean getM372() {
        return m372;
    }

    public void setM372(Boolean m372) {
        this.m372 = m372;
    }

    public Boolean getM38() {
        return m38;
    }

    public void setM38(Boolean m38) {
        this.m38 = m38;
    }

    public Boolean getM381() {
        return m381;
    }

    public void setM381(Boolean m381) {
        this.m381 = m381;
    }

    public Boolean getM382() {
        return m382;
    }

    public void setM382(Boolean m382) {
        this.m382 = m382;
    }

    public Boolean getM39() {
        return m39;
    }

    public void setM39(Boolean m39) {
        this.m39 = m39;
    }

    public Boolean getM4() {
        return m4;
    }

    public void setM4(Boolean m4) {
        this.m4 = m4;
    }

    public Boolean getM41() {
        return m41;
    }

    public void setM41(Boolean m41) {
        this.m41 = m41;
    }

    public Boolean getM42() {
        return m42;
    }

    public void setM42(Boolean m42) {
        this.m42 = m42;
    }

    public Boolean getM5() {
        return m5;
    }

    public void setM5(Boolean m5) {
        this.m5 = m5;
    }

    public Boolean getM51() {
        return m51;
    }

    public void setM51(Boolean m51) {
        this.m51 = m51;
    }

    public Boolean getM52() {
        return m52;
    }

    public void setM52(Boolean m52) {
        this.m52 = m52;
    }
    
    public Previlage Format(String a){
        Previlage cp=new Previlage();
        cp.setM1(setBoolean(a.charAt(0)));
        cp.setM11(setBoolean(a.charAt(1)));
        cp.setM12(setBoolean(a.charAt(2)));
        cp.setM13(setBoolean(a.charAt(3)));
        cp.setM14(setBoolean(a.charAt(4)));
        cp.setM15(setBoolean(a.charAt(5)));
        cp.setM16(setBoolean(a.charAt(6)));
        cp.setM161(setBoolean(a.charAt(7)));
        cp.setM162(setBoolean(a.charAt(8)));
        cp.setM17(setBoolean(a.charAt(9)));
        cp.setM2(setBoolean(a.charAt(10)));
        cp.setM21(setBoolean(a.charAt(11)));
        cp.setM211(setBoolean(a.charAt(12)));
        cp.setM212(setBoolean(a.charAt(13)));
        cp.setM22(setBoolean(a.charAt(14)));
        cp.setM221(setBoolean(a.charAt(15)));
        cp.setM222(setBoolean(a.charAt(16)));
        cp.setM23(setBoolean(a.charAt(17)));
        cp.setM231(setBoolean(a.charAt(18)));
        cp.setM232(setBoolean(a.charAt(19)));
        cp.setM233(setBoolean(a.charAt(20)));
        cp.setM24(setBoolean(a.charAt(21)));
        cp.setM241(setBoolean(a.charAt(22)));
        cp.setM242(setBoolean(a.charAt(23)));
        cp.setM25(setBoolean(a.charAt(24)));
        cp.setM26(setBoolean(a.charAt(25)));
        cp.setM3(setBoolean(a.charAt(26)));
        cp.setM31(setBoolean(a.charAt(27)));
        cp.setM32(setBoolean(a.charAt(28)));
        cp.setM33(setBoolean(a.charAt(29)));
        cp.setM34(setBoolean(a.charAt(30)));
        cp.setM341(setBoolean(a.charAt(31)));
        cp.setM342(setBoolean(a.charAt(32)));
        cp.setM343(setBoolean(a.charAt(33)));
        cp.setM35(setBoolean(a.charAt(34)));
        cp.setM351(setBoolean(a.charAt(35)));
        cp.setM352(setBoolean(a.charAt(36)));
        cp.setM353(setBoolean(a.charAt(37)));
        cp.setM36(setBoolean(a.charAt(38)));
        cp.setM37(setBoolean(a.charAt(39)));
        cp.setM371(setBoolean(a.charAt(40)));
        cp.setM372(setBoolean(a.charAt(41)));
        cp.setM38(setBoolean(a.charAt(42)));
        cp.setM381(setBoolean(a.charAt(43)));
        cp.setM382(setBoolean(a.charAt(44)));
        cp.setM39(setBoolean(a.charAt(45)));
        cp.setM4(setBoolean(a.charAt(46)));
        cp.setM41(setBoolean(a.charAt(47)));
        cp.setM42(setBoolean(a.charAt(48)));
        cp.setM5(setBoolean(a.charAt(49)));
        cp.setM51(setBoolean(a.charAt(50)));
        cp.setM52(setBoolean(a.charAt(51)));
        return cp;
    }

    public String Format(Previlage cp){
        String format="";
        format+=jinol(cp.m1);
        format+=jinol(cp.m11);
        format+=jinol(cp.m12);
        format+=jinol(cp.m13);
        format+=jinol(cp.m14);
        format+=jinol(cp.m15);
        format+=jinol(cp.m16);
        format+=jinol(cp.m161);
        format+=jinol(cp.m162);
        format+=jinol(cp.m17);
        format+=jinol(cp.m2);
        format+=jinol(cp.m21);
        format+=jinol(cp.m211);
        format+=jinol(cp.m212);
        format+=jinol(cp.m22);
        format+=jinol(cp.m221);
        format+=jinol(cp.m222);
        format+=jinol(cp.m23);
        format+=jinol(cp.m231);
        format+=jinol(cp.m232);
        format+=jinol(cp.m233);
        format+=jinol(cp.m24);
        format+=jinol(cp.m241);
        format+=jinol(cp.m242);
        format+=jinol(cp.m25);
        format+=jinol(cp.m26);
        format+=jinol(cp.m3);
        format+=jinol(cp.m31);
        format+=jinol(cp.m32);
        format+=jinol(cp.m33);
        format+=jinol(cp.m34);
        format+=jinol(cp.m341);
        format+=jinol(cp.m342);
        format+=jinol(cp.m343);
        format+=jinol(cp.m35);
        format+=jinol(cp.m351);
        format+=jinol(cp.m352);
        format+=jinol(cp.m353);
        format+=jinol(cp.m36);
        format+=jinol(cp.m37);
        format+=jinol(cp.m371);
        format+=jinol(cp.m372);
        format+=jinol(cp.m38);
        format+=jinol(cp.m381);
        format+=jinol(cp.m382);
        format+=jinol(cp.m39);
        format+=jinol(cp.m4);
        format+=jinol(cp.m41);
        format+=jinol(cp.m42);
        format+=jinol(cp.m5);
        format+=jinol(cp.m51);
        format+=jinol(cp.m52);
        return format;
    }

    public String jinol(Boolean b){
        if(b==true) return "1";
        else return "0";
    }
    
    private boolean setBoolean(char x){
        if(x=='1') return true;
        else return false;
    }
}
