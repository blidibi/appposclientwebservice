/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.orm;

import com.blung.pos.entity.simple.DetailReturPembelian;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * ORM
 * @author lukman
 */
public class ReturPembelian {
    private Long idReturPembelian;
    private Long idPembelian;
    private Date tanggal;
    private Long idUser;
    private Long idKasir;
    List<DetailReturPembelian> listDetailReturPembelian=new ArrayList<>();

    public Long getIdReturPembelian() {
        return idReturPembelian;
    }

    public void setIdReturPembelian(Long idReturPembelian) {
        this.idReturPembelian = idReturPembelian;
    }

    public Long getIdPembelian() {
        return idPembelian;
    }

    public void setIdPembelian(Long idPembelian) {
        this.idPembelian = idPembelian;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Long getIdKasir() {
        return idKasir;
    }

    public void setIdKasir(Long idKasir) {
        this.idKasir = idKasir;
    }

    public List<DetailReturPembelian> getListDetailReturPembelian() {
        return listDetailReturPembelian;
    }

    public void setListDetailReturPembelian(List<DetailReturPembelian> listDetailReturPembelian) {
        this.listDetailReturPembelian = listDetailReturPembelian;
    }
}
