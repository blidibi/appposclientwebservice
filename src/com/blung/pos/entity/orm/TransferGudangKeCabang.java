/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.orm;

import com.blung.pos.entity.simple.DetailTransferGudangKeCabang;
import java.util.Date;
import java.util.List;

/**
 * ORM
 * @author lukman
 */
public class TransferGudangKeCabang {
    private Long idTransferGudangKeCabang;
    private Long idGudang;
    private Long idCabang;
    private Long idUser;
    private Date tanggalKirim;
    private Date tanggalTerima;
    private Long idKaryawanPengirim;
    private Long idKaryawanPenerima;
    private List<com.blung.pos.entity.simple.DetailTransferGudangKeCabang> list;

    public Long getIdTransferGudangKeCabang() {
        return idTransferGudangKeCabang;
    }

    public void setIdTransferGudangKeCabang(Long idTransferGudangKeCabang) {
        this.idTransferGudangKeCabang = idTransferGudangKeCabang;
    }

    public Long getIdGudang() {
        return idGudang;
    }

    public void setIdGudang(Long idGudang) {
        this.idGudang = idGudang;
    }

    public Long getIdCabang() {
        return idCabang;
    }

    public void setIdCabang(Long idCabang) {
        this.idCabang = idCabang;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Date getTanggalKirim() {
        return tanggalKirim;
    }

    public void setTanggalKirim(Date tanggalKirim) {
        this.tanggalKirim = tanggalKirim;
    }

    public Date getTanggalTerima() {
        return tanggalTerima;
    }

    public void setTanggalTerima(Date tanggalTerima) {
        this.tanggalTerima = tanggalTerima;
    }

    public Long getIdKaryawanPengirim() {
        return idKaryawanPengirim;
    }

    public void setIdKaryawanPengirim(Long idKaryawanPengirim) {
        this.idKaryawanPengirim = idKaryawanPengirim;
    }

    public Long getIdKaryawanPenerima() {
        return idKaryawanPenerima;
    }

    public void setIdKaryawanPenerima(Long idKaryawanPenerima) {
        this.idKaryawanPenerima = idKaryawanPenerima;
    }

    public List<DetailTransferGudangKeCabang> getList() {
        return list;
    }

    public void setList(List<DetailTransferGudangKeCabang> list) {
        this.list = list;
    }
}
