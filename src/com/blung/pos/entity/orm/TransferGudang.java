/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.orm;

import java.util.Date;
import java.util.List;

/**
 * ORM
 * @author lukman
 */
public class TransferGudang {
    private Long idTransfer;
    private Long idGudangPengirim;
    private Long idGudangPenerima;
    private Long idUser;
    private Date tanggalKirim;
    private Date tanggalTerima;
    private Long idKaryawanPengirim;
    private Long idKaryawanPenerima;
    private List<com.blung.pos.entity.simple.DetailTransferGudang> list;

    public List<com.blung.pos.entity.simple.DetailTransferGudang> getList() {
        return list;
    }

    public void setList(List<com.blung.pos.entity.simple.DetailTransferGudang> list) {
        this.list = list;
    }

    public Long getIdTransfer() {
        return idTransfer;
    }

    public void setIdTransfer(Long idTransfer) {
        this.idTransfer = idTransfer;
    }

    public Long getIdGudangPengirim() {
        return idGudangPengirim;
    }

    public void setIdGudangPengirim(Long idGudangPengirim) {
        this.idGudangPengirim = idGudangPengirim;
    }

    public Long getIdGudangPenerima() {
        return idGudangPenerima;
    }

    public void setIdGudangPenerima(Long idGudangPenerima) {
        this.idGudangPenerima = idGudangPenerima;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Date getTanggalKirim() {
        return tanggalKirim;
    }

    public void setTanggalKirim(Date tanggalKirim) {
        this.tanggalKirim = tanggalKirim;
    }

    public Date getTanggalTerima() {
        return tanggalTerima;
    }

    public void setTanggalTerima(Date tanggalTerima) {
        this.tanggalTerima = tanggalTerima;
    }

    public Long getIdKaryawanPengirim() {
        return idKaryawanPengirim;
    }

    public void setIdKaryawanPengirim(Long idKaryawanPengirim) {
        this.idKaryawanPengirim = idKaryawanPengirim;
    }

    public Long getIdKaryawanPenerima() {
        return idKaryawanPenerima;
    }

    public void setIdKaryawanPenerima(Long idKaryawanPenerima) {
        this.idKaryawanPenerima = idKaryawanPenerima;
    }
}
