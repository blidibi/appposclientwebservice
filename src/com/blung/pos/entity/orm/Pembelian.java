/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.orm;

import com.blung.pos.entity.simple.DetailPembelian;
import java.util.Date;
import java.util.List;

/**
 * ORM
 * @author lukman
 */
public class Pembelian {
    private Long idPembelian;
    private String noFaktur;
    private Date tanggalBeli;
    private Double totalHarga;
    private Long idUser;
    private Long idSupliyer;
    private String keterangan;
    private Long idKasir;
    private Boolean statusPindahGudang;
    private Boolean statusHutang=false;
    private List<com.blung.pos.entity.simple.DetailPembelian> ldp;

    public Long getIdPembelian() {
        return idPembelian;
    }

    public void setIdPembelian(Long idPembelian) {
        this.idPembelian = idPembelian;
    }

    public String getNoFaktur() {
        return noFaktur;
    }

    public void setNoFaktur(String noFaktur) {
        this.noFaktur = noFaktur;
    }

    public Date getTanggalBeli() {
        return tanggalBeli;
    }

    public void setTanggalBeli(Date tanggalBeli) {
        this.tanggalBeli = tanggalBeli;
    }

    public Double getTotalHarga() {
        return totalHarga;
    }

    public void setTotalHarga(Double totalHarga) {
        this.totalHarga = totalHarga;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Long getIdSupliyer() {
        return idSupliyer;
    }

    public void setIdSupliyer(Long idSupliyer) {
        this.idSupliyer = idSupliyer;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Long getIdKasir() {
        return idKasir;
    }

    public void setIdKasir(Long idKasir) {
        this.idKasir = idKasir;
    }

    public Boolean getStatusPindahGudang() {
        return statusPindahGudang;
    }

    public void setStatusPindahGudang(Boolean statusPindahGudang) {
        this.statusPindahGudang = statusPindahGudang;
    }

    public Boolean getStatusHutang() {
        return statusHutang;
    }

    public void setStatusHutang(Boolean statusHutang) {
        this.statusHutang = statusHutang;
    }

    public List<DetailPembelian> getLdp() {
        return ldp;
    }

    public void setLdp(List<DetailPembelian> ldp) {
        this.ldp = ldp;
    }
}
