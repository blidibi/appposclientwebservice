/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.orm;

import com.blung.pos.entity.simple.DetailReturPenjualan;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * ORM
 * @author lukman
 */
public class ReturPenjualan {
    private Long idReturPenjualan;
    private Long idPenjualan;
    private Date tanggal;
    private Long idUser;
    private Long idKasir;
    List<DetailReturPenjualan> listDetailReturPenjualan=new ArrayList<>();

    public Long getIdReturPenjualan() {
        return idReturPenjualan;
    }

    public void setIdReturPenjualan(Long idReturPenjualan) {
        this.idReturPenjualan = idReturPenjualan;
    }

    public Long getIdPenjualan() {
        return idPenjualan;
    }

    public void setIdPenjualan(Long idPenjualan) {
        this.idPenjualan = idPenjualan;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Long getIdKasir() {
        return idKasir;
    }

    public void setIdKasir(Long idKasir) {
        this.idKasir = idKasir;
    }

    public List<DetailReturPenjualan> getListDetailReturPenjualan() {
        return listDetailReturPenjualan;
    }

    public void setListDetailReturPenjualan(List<DetailReturPenjualan> listDetailReturPenjualan) {
        this.listDetailReturPenjualan = listDetailReturPenjualan;
    }
}
