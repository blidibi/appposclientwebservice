/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.orm;

import com.blung.pos.entity.simple.DetailPenjualan;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * ORM
 * @author lukman
 */
public class Penjualan {
    private Long idPenjualan;
    private Double totalHarga;
    private Double diskonPelanggan;
    private Double potonganLangsung;
    private String tanggalJual;
    private Long idUser;
    private Boolean statusDikirim;
    private Long idKasir;
    private Long idPelanggan;
    private Boolean statusJual;
    private List<com.blung.pos.entity.simple.DetailPenjualan> ldp = new ArrayList<>();

    public Long getIdPenjualan() {
        return idPenjualan;
    }

    public void setIdPenjualan(Long idPenjualan) {
        this.idPenjualan = idPenjualan;
    }

    public Double getTotalHarga() {
        return totalHarga;
    }

    public void setTotalHarga(Double totalHarga) {
        this.totalHarga = totalHarga;
    }

    public Double getDiskonPelanggan() {
        return diskonPelanggan;
    }

    public void setDiskonPelanggan(Double diskonPelanggan) {
        this.diskonPelanggan = diskonPelanggan;
    }

    public Double getPotonganLangsung() {
        return potonganLangsung;
    }

    public void setPotonganLangsung(Double potonganLangsung) {
        this.potonganLangsung = potonganLangsung;
    }

    public String getTanggalJual() {
        return tanggalJual;
    }

    public void setTanggalJual(String tanggalJual) {
        this.tanggalJual = tanggalJual;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Boolean getStatusDikirim() {
        return statusDikirim;
    }

    public void setStatusDikirim(Boolean statusDikirim) {
        this.statusDikirim = statusDikirim;
    }

    public Long getIdKasir() {
        return idKasir;
    }

    public void setIdKasir(Long idKasir) {
        this.idKasir = idKasir;
    }

    public Long getIdPelanggan() {
        return idPelanggan;
    }

    public void setIdPelanggan(Long idPelanggan) {
        this.idPelanggan = idPelanggan;
    }

    public Boolean getStatusJual() {
        return statusJual;
    }

    public void setStatusJual(Boolean statusJual) {
        this.statusJual = statusJual;
    }

    public List<DetailPenjualan> getLdp() {
        return ldp;
    }

    public void setLdp(List<DetailPenjualan> ldp) {
        this.ldp = ldp;
    }

}
