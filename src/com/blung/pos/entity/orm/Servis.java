/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.orm;
import com.blung.pos.entity.simple.DetailServis;
import java.util.Date;
import java.util.List;

/**
 *
 * @author lukman
 */
public class Servis {
    private Long idServis;
    private Long idKaryawan;
    private Long idCabang;
    private String namaDevice;
    private String namaPelanggan;
    private Date tanggalMasuk;
    private Date tanggalJadi;
    private String keluhan;
    private Boolean status=false;
    List<DetailServis> lds;

    public Long getIdServis() {
        return idServis;
    }

    public void setIdServis(Long idServis) {
        this.idServis = idServis;
    }

    public Long getIdKaryawan() {
        return idKaryawan;
    }

    public void setIdKaryawan(Long idKaryawan) {
        this.idKaryawan = idKaryawan;
    }

    public Long getIdCabang() {
        return idCabang;
    }

    public void setIdCabang(Long idCabang) {
        this.idCabang = idCabang;
    }

    public String getNamaDevice() {
        return namaDevice;
    }

    public void setNamaDevice(String namaDevice) {
        this.namaDevice = namaDevice;
    }

    public String getNamaPelanggan() {
        return namaPelanggan;
    }

    public void setNamaPelanggan(String namaPelanggan) {
        this.namaPelanggan = namaPelanggan;
    }

    public Date getTanggalMasuk() {
        return tanggalMasuk;
    }

    public void setTanggalMasuk(Date tanggalMasuk) {
        this.tanggalMasuk = tanggalMasuk;
    }

    public Date getTanggalJadi() {
        return tanggalJadi;
    }

    public void setTanggalJadi(Date tanggalJadi) {
        this.tanggalJadi = tanggalJadi;
    }

    public String getKeluhan() {
        return keluhan;
    }

    public void setKeluhan(String keluhan) {
        this.keluhan = keluhan;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<DetailServis> getLds() {
        return lds;
    }

    public void setLds(List<DetailServis> lds) {
        this.lds = lds;
    }
}
