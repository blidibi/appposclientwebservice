/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

import java.util.Date;

/**
 * simple
 * @author lukman
 */
public class TransferGudang {
    private Long idTransfer;
    private Long idGudangPengirim;
    private Long idGudangPenerima;
    private Long idUser;
    private Date tanggalKirim;
    private Date tanggalTerima;
    private Long idKaryawanPengirim;
    private Long idKaryawanPenerima;
    private Boolean verified=false;

    public Long getIdTransfer() {
        return idTransfer;
    }

    public void setIdTransfer(Long idTransfer) {
        this.idTransfer = idTransfer;
    }

    public Long getIdGudangPengirim() {
        return idGudangPengirim;
    }

    public void setIdGudangPengirim(Long idGudangPengirim) {
        this.idGudangPengirim = idGudangPengirim;
    }

    public Long getIdGudangPenerima() {
        return idGudangPenerima;
    }

    public void setIdGudangPenerima(Long idGudangPenerima) {
        this.idGudangPenerima = idGudangPenerima;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Date getTanggalKirim() {
        return tanggalKirim;
    }

    public void setTanggalKirim(Date tanggalKirim) {
        this.tanggalKirim = tanggalKirim;
    }

    public Date getTanggalTerima() {
        return tanggalTerima;
    }

    public void setTanggalTerima(Date tanggalTerima) {
        this.tanggalTerima = tanggalTerima;
    }

    public Long getIdKaryawanPengirim() {
        return idKaryawanPengirim;
    }

    public void setIdKaryawanPengirim(Long idKaryawanPengirim) {
        this.idKaryawanPengirim = idKaryawanPengirim;
    }

    public Long getIdKaryawanPenerima() {
        return idKaryawanPenerima;
    }

    public void setIdKaryawanPenerima(Long idKaryawanPenerima) {
        this.idKaryawanPenerima = idKaryawanPenerima;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }
}
