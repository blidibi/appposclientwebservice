/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

/**
 * simple
 * @author lukman
 */
public class Pelanggan {
    private Long idPelanggan;
    private String alamat;
    private String nama;
    private String noID;
    private String noTlpPelanggan;
    private Long idKategoriPelanggan;

    public Long getIdPelanggan() {
        return idPelanggan;
    }

    public void setIdPelanggan(Long idPelanggan) {
        this.idPelanggan = idPelanggan;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNoID() {
        return noID;
    }

    public void setNoID(String noID) {
        this.noID = noID;
    }

    public String getNoTlpPelanggan() {
        return noTlpPelanggan;
    }

    public void setNoTlpPelanggan(String noTlpPelanggan) {
        this.noTlpPelanggan = noTlpPelanggan;
    }

    public Long getIdKategoriPelanggan() {
        return idKategoriPelanggan;
    }

    public void setIdKategoriPelanggan(Long idKategoriPelanggan) {
        this.idKategoriPelanggan = idKategoriPelanggan;
    }
}
