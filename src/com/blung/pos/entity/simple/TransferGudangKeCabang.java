/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

import java.util.Date;

/**
 * simple
 * @author lukman
 */
public class TransferGudangKeCabang {
    private Long idTransferGudangKeCabang;
    private Long idGudang;
    private Long idCabang;
    private Long idUser;
    private Date tanggalKirim;
    private Date tanggalTerima;
    private Long idKaryawanPemindah;
    private Boolean verified=false;

    public Long getIdTransferGudangKeCabang() {
        return idTransferGudangKeCabang;
    }

    public void setIdTransferGudangKeCabang(Long idTransferGudangKeCabang) {
        this.idTransferGudangKeCabang = idTransferGudangKeCabang;
    }

    public Long getIdGudang() {
        return idGudang;
    }

    public void setIdGudang(Long idGudang) {
        this.idGudang = idGudang;
    }

    public Long getIdCabang() {
        return idCabang;
    }

    public void setIdCabang(Long idCabang) {
        this.idCabang = idCabang;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Date getTanggalKirim() {
        return tanggalKirim;
    }

    public void setTanggalKirim(Date tanggalKirim) {
        this.tanggalKirim = tanggalKirim;
    }

    public Date getTanggalTerima() {
        return tanggalTerima;
    }

    public void setTanggalTerima(Date tanggalTerima) {
        this.tanggalTerima = tanggalTerima;
    }

    public Long getIdKaryawanPemindah() {
        return idKaryawanPemindah;
    }

    public void setIdKaryawanPemindah(Long idKaryawanPemindah) {
        this.idKaryawanPemindah = idKaryawanPemindah;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }
}
