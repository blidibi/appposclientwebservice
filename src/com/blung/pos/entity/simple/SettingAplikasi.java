/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

/**
 * simple
 * @author lukman
 */
public class SettingAplikasi {
    private Long idSetting;
    private String alamatToko;
    private String noTlpToko;
    private Long idKasir;

    public Long getIdSetting() {
        return idSetting;
    }

    public void setIdSetting(Long idSetting) {
        this.idSetting = idSetting;
    }

    public String getAlamatToko() {
        return alamatToko;
    }

    public void setAlamatToko(String alamatToko) {
        this.alamatToko = alamatToko;
    }

    public String getNoTlpToko() {
        return noTlpToko;
    }

    public void setNoTlpToko(String noTlpToko) {
        this.noTlpToko = noTlpToko;
    }

    public Long getIdKasir() {
        return idKasir;
    }

    public void setIdKasir(Long idKasir) {
        this.idKasir = idKasir;
    }
}
