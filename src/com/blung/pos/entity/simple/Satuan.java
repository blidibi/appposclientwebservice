/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

/**
 * simple
 * @author lukman
 */
public class Satuan {
    private Long idSatuan;
    private String namaSatuan;

    public Long getIdSatuan() {
        return idSatuan;
    }

    public void setIdSatuan(Long idSatuan) {
        this.idSatuan = idSatuan;
    }

    public String getNamaSatuan() {
        return namaSatuan;
    }

    public void setNamaSatuan(String namaSatuan) {
        this.namaSatuan = namaSatuan;
    }
    
}
