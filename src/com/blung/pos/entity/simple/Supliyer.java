/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

/**
 * simple
 * @author lukman
 */
public class Supliyer {
    private Long idSupliyer;
    private String alamatSupliyer;
    private String namaSupliyer;
    private String noTlpSupliyer;

    public Long getIdSupliyer() {
        return idSupliyer;
    }

    public void setIdSupliyer(Long idSupliyer) {
        this.idSupliyer = idSupliyer;
    }
    
    
    public String getAlamatSupliyer() {
        return alamatSupliyer;
    }

    public void setAlamatSupliyer(String alamatSupliyer) {
        this.alamatSupliyer = alamatSupliyer;
    }

    public String getNamaSupliyer() {
        return namaSupliyer;
    }

    public void setNamaSupliyer(String namaSupliyer) {
        this.namaSupliyer = namaSupliyer;
    }

    public String getNoTlpSupliyer() {
        return noTlpSupliyer;
    }

    public void setNoTlpSupliyer(String noTlpSupliyer) {
        this.noTlpSupliyer = noTlpSupliyer;
    }
    
}
