/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

/**
 * simple
 * @author lukman
 */
public class DetailPenjualan {
    private Long idDetailPenjualan;
    private Long diskonPersen;
    private Double diskonRupiah;
    private Double harga;
    private int jumlah;
    private Double keuntunganSatuan;
    private Double subTotal;
    private Long idBarang;
    private Long idPenjualan;

    public Long getIdDetailPenjualan() {
        return idDetailPenjualan;
    }

    public void setIdDetailPenjualan(Long idDetailPenjualan) {
        this.idDetailPenjualan = idDetailPenjualan;
    }

    public Long getDiskonPersen() {
        return diskonPersen;
    }

    public void setDiskonPersen(Long diskonPersen) {
        this.diskonPersen = diskonPersen;
    }

    public Double getDiskonRupiah() {
        return diskonRupiah;
    }

    public void setDiskonRupiah(Double diskonRupiah) {
        this.diskonRupiah = diskonRupiah;
    }

    public Double getHarga() {
        return harga;
    }

    public void setHarga(Double harga) {
        this.harga = harga;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public Double getKeuntunganSatuan() {
        return keuntunganSatuan;
    }

    public void setKeuntunganSatuan(Double keuntunganSatuan) {
        this.keuntunganSatuan = keuntunganSatuan;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Long getIdBarang() {
        return idBarang;
    }

    public void setIdBarang(Long idBarang) {
        this.idBarang = idBarang;
    }

    public Long getIdPenjualan() {
        return idPenjualan;
    }

    public void setIdPenjualan(Long idPenjualan) {
        this.idPenjualan = idPenjualan;
    }
}
