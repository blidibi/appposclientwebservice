/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

/**
 *
 * @author lukman
 */
public class DetailServis {
    private Long idDetailServis;
    private Long idServis;
    private String namaServis;
    private Double harga;

    public Long getIdDetailServis() {
        return idDetailServis;
    }

    public void setIdDetailServis(Long idDetailServis) {
        this.idDetailServis = idDetailServis;
    }

    public Long getIdServis() {
        return idServis;
    }

    public void setIdServis(Long idServis) {
        this.idServis = idServis;
    }

    public String getNamaServis() {
        return namaServis;
    }

    public void setNamaServis(String namaServis) {
        this.namaServis = namaServis;
    }

    public Double getHarga() {
        return harga;
    }

    public void setHarga(Double harga) {
        this.harga = harga;
    }
}
