/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

/**
 * simple
 * @author lukman
 */
public class DetailTransferGudang {
    private Long idDetailTransferGudang;
    private Long idTransfer;
    private Long idBarang;
    private Long jumlah;
    private Double harga;
    private Double subTotal;

    public Long getIdDetailTransferGudang() {
        return idDetailTransferGudang;
    }

    public void setIdDetailTransferGudang(Long idDetailTransferGudang) {
        this.idDetailTransferGudang = idDetailTransferGudang;
    }

    public Long getIdTransfer() {
        return idTransfer;
    }

    public void setIdTransfer(Long idTransfer) {
        this.idTransfer = idTransfer;
    }

    public Long getIdBarang() {
        return idBarang;
    }

    public void setIdBarang(Long idBarang) {
        this.idBarang = idBarang;
    }

    public Long getJumlah() {
        return jumlah;
    }

    public void setJumlah(Long jumlah) {
        this.jumlah = jumlah;
    }

    public Double getHarga() {
        return harga;
    }

    public void setHarga(Double harga) {
        this.harga = harga;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }
    
}
