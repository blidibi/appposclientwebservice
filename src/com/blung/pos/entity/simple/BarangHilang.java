/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

import java.util.Date;

/**
 *
 * @author lukman
 */
public class BarangHilang {
    
    private Long idBarangHilang;
    private Long idBarang;
    private Long jumlah;
    private Double hargaRata;
    private Date tanggalHilang;
    private Long idUser;
    private Long idCabang;
    private Boolean verified=false;

    public Long getIdBarangHilang() {
        return idBarangHilang;
    }

    public void setIdBarangHilang(Long idBarangHilang) {
        this.idBarangHilang = idBarangHilang;
    }

    public Long getIdBarang() {
        return idBarang;
    }

    public void setIdBarang(Long idBarang) {
        this.idBarang = idBarang;
    }

    public Long getJumlah() {
        return jumlah;
    }

    public void setJumlah(Long jumlah) {
        this.jumlah = jumlah;
    }

    public Double getHargaRata() {
        return hargaRata;
    }

    public void setHargaRata(Double hargaRata) {
        this.hargaRata = hargaRata;
    }

    public Date getTanggalHilang() {
        return tanggalHilang;
    }

    public void setTanggalHilang(Date tanggalHilang) {
        this.tanggalHilang = tanggalHilang;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Long getIdCabang() {
        return idCabang;
    }

    public void setIdCabang(Long idCabang) {
        this.idCabang = idCabang;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }
}
