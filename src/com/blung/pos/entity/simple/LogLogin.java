/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

import java.util.Date;

/**
 * simple
 * @author lukman
 */
public class LogLogin {
    private Long idLogLogin;
    private Long idUser;
    private Date waktuLogin;
    private Long idKasir;

    public Long getIdLogLogin() {
        return idLogLogin;
    }

    public void setIdLogLogin(Long idLogLogin) {
        this.idLogLogin = idLogLogin;
    }
    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Date getWaktuLogin() {
        return waktuLogin;
    }

    public void setWaktuLogin(Date waktuLogin) {
        this.waktuLogin = waktuLogin;
    }

    public Long getIdKasir() {
        return idKasir;
    }

    public void setIdKasir(Long idKasir) {
        this.idKasir = idKasir;
    }
}
