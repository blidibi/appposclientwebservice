/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

/**
 *
 * @author lukman
 */
public class Harga {
    private Long idHarga;
    private Long idBarang;
    private Double diskonPromo;
    private Double hargaBeliTerakhir;
    private Double hargaBeliRata;
    private Long batasSatu;
    private Double hargaSatu;
    private Long batasDua;
    private Double hargaDua;
    private Long batasTiga;
    private Double hargaTiga;
    private Double hargaEmpat;
    private Long idKategori;
    private Long idSupliyer;
    private Long idSatuan;
    private Long idCabang;

    public Long getIdHarga() {
        return idHarga;
    }

    public void setIdHarga(Long idHarga) {
        this.idHarga = idHarga;
    }

    public Long getIdBarang() {
        return idBarang;
    }

    public void setIdBarang(Long idBarang) {
        this.idBarang = idBarang;
    }

    public Double getDiskonPromo() {
        return diskonPromo;
    }

    public void setDiskonPromo(Double diskonPromo) {
        this.diskonPromo = diskonPromo;
    }

    public Double getHargaBeliTerakhir() {
        return hargaBeliTerakhir;
    }

    public void setHargaBeliTerakhir(Double hargaBeliTerakhir) {
        this.hargaBeliTerakhir = hargaBeliTerakhir;
    }

    public Double getHargaBeliRata() {
        return hargaBeliRata;
    }

    public void setHargaBeliRata(Double hargaBeliRata) {
        this.hargaBeliRata = hargaBeliRata;
    }

    public Long getBatasSatu() {
        return batasSatu;
    }

    public void setBatasSatu(Long batasSatu) {
        this.batasSatu = batasSatu;
    }

    public Double getHargaSatu() {
        return hargaSatu;
    }

    public void setHargaSatu(Double hargaSatu) {
        this.hargaSatu = hargaSatu;
    }

    public Long getBatasDua() {
        return batasDua;
    }

    public void setBatasDua(Long batasDua) {
        this.batasDua = batasDua;
    }

    public Double getHargaDua() {
        return hargaDua;
    }

    public void setHargaDua(Double hargaDua) {
        this.hargaDua = hargaDua;
    }

    public Long getBatasTiga() {
        return batasTiga;
    }

    public void setBatasTiga(Long batasTiga) {
        this.batasTiga = batasTiga;
    }

    public Double getHargaTiga() {
        return hargaTiga;
    }

    public void setHargaTiga(Double hargaTiga) {
        this.hargaTiga = hargaTiga;
    }

    public Double getHargaEmpat() {
        return hargaEmpat;
    }

    public void setHargaEmpat(Double hargaEmpat) {
        this.hargaEmpat = hargaEmpat;
    }

    public Long getIdKategori() {
        return idKategori;
    }

    public void setIdKategori(Long idKategori) {
        this.idKategori = idKategori;
    }

    public Long getIdSupliyer() {
        return idSupliyer;
    }

    public void setIdSupliyer(Long idSupliyer) {
        this.idSupliyer = idSupliyer;
    }

    public Long getIdSatuan() {
        return idSatuan;
    }

    public void setIdSatuan(Long idSatuan) {
        this.idSatuan = idSatuan;
    }

    public Long getIdCabang() {
        return idCabang;
    }

    public void setIdCabang(Long idCabang) {
        this.idCabang = idCabang;
    }
}
