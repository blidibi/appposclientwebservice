/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

/**
 * simple
 * @author lukman
 */
public class DetailReturPembelian {
    private Long idDetailReturPembelian;
    private Long idBarang;
    private int jumlah;
    private Double harga;
    private String keterangan;
    private Long idReturPembelian;

    public Long getIdDetailReturPembelian() {
        return idDetailReturPembelian;
    }

    public void setIdDetailReturPembelian(Long idDetailReturPembelian) {
        this.idDetailReturPembelian = idDetailReturPembelian;
    }

    public Long getIdBarang() {
        return idBarang;
    }

    public void setIdBarang(Long idBarang) {
        this.idBarang = idBarang;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public Double getHarga() {
        return harga;
    }

    public void setHarga(Double harga) {
        this.harga = harga;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Long getIdReturPembelian() {
        return idReturPembelian;
    }

    public void setIdReturPembelian(Long idReturPembelian) {
        this.idReturPembelian = idReturPembelian;
    }
}
