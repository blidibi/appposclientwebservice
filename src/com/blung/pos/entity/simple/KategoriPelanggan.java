/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

/**
 * simple
 * @author lukman
 */
public class KategoriPelanggan {
    private Long idKategoriPelanggan;
    private String keterangan;
    private int persenDiskon;

    public Long getIdKategoriPelanggan() {
        return idKategoriPelanggan;
    }

    public void setIdKategoriPelanggan(Long idKategoriPelanggan) {
        this.idKategoriPelanggan = idKategoriPelanggan;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public int getPersenDiskon() {
        return persenDiskon;
    }

    public void setPersenDiskon(int persenDiskon) {
        this.persenDiskon = persenDiskon;
    }
    
}
