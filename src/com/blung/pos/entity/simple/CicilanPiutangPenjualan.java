/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

import java.util.Date;

/**
 * ORM
 * @author lukman
 */
public class CicilanPiutangPenjualan {
    private Long idCicilanPiutang;
    private Long idPiutangPenjualan;
    private Date tanggalBayar;
    private Long idUser;
    private Double jumlahBayar;
    private String keterangan;
    private Boolean verified=false;
    
    public Long getIdCicilanPiutang() {
        return idCicilanPiutang;
    }

    public void setIdCicilanPiutang(Long idCicilanPiutang) {
        this.idCicilanPiutang = idCicilanPiutang;
    }

    public Long getIdPiutangPenjualan() {
        return idPiutangPenjualan;
    }

    public void setIdPiutangPenjualan(Long idPiutangPenjualan) {
        this.idPiutangPenjualan = idPiutangPenjualan;
    }

    public Date getTanggalBayar() {
        return tanggalBayar;
    }

    public void setTanggalBayar(Date tanggalBayar) {
        this.tanggalBayar = tanggalBayar;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Double getJumlahBayar() {
        return jumlahBayar;
    }

    public void setJumlahBayar(Double jumlahBayar) {
        this.jumlahBayar = jumlahBayar;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

}
