/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

import java.sql.Time;
import java.util.Date;

/**
 * simple
 * @author lukman
 */
public class Absensi {
    private Long idAbsensi;
    private Time jamMasuk;
    private Time jamPulang;
    private Date tanggalAbsen;
    private Long idKaryawan;

    public Long getIdAbsensi() {
        return idAbsensi;
    }

    public void setIdAbsensi(Long idAbsensi) {
        this.idAbsensi = idAbsensi;
    }

    public Time getJamMasuk() {
        return jamMasuk;
    }

    public void setJamMasuk(Time jamMasuk) {
        this.jamMasuk = jamMasuk;
    }

    public Time getJamPulang() {
        return jamPulang;
    }

    public void setJamPulang(Time jamPulang) {
        this.jamPulang = jamPulang;
    }

    public Date getTanggalAbsen() {
        return tanggalAbsen;
    }

    public void setTanggalAbsen(Date tanggalAbsen) {
        this.tanggalAbsen = tanggalAbsen;
    }

    public Long getIdKaryawan() {
        return idKaryawan;
    }

    public void setIdKaryawan(Long idKaryawan) {
        this.idKaryawan = idKaryawan;
    }
    
}
