/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

import java.util.Date;

/**
 *
 * @author lukman
 */
public class Perdana {
    private Long idPerdana;
    private Long idProvider;
    private String nomor;
    private Date tanggalBeli;
    private Date tanggalExpired;
    private Date tanggalLaku;
    private Boolean statuslaku;
    private Double hargaBeli;
    private Double hargaJual;

    public Long getIdPerdana() {
        return idPerdana;
    }

    public void setIdPerdana(Long idPerdana) {
        this.idPerdana = idPerdana;
    }

    public Long getIdProvider() {
        return idProvider;
    }

    public void setIdProvider(Long idProvider) {
        this.idProvider = idProvider;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public Date getTanggalBeli() {
        return tanggalBeli;
    }

    public void setTanggalBeli(Date tanggalBeli) {
        this.tanggalBeli = tanggalBeli;
    }

    public Date getTanggalExpired() {
        return tanggalExpired;
    }

    public void setTanggalExpired(Date tanggalExpired) {
        this.tanggalExpired = tanggalExpired;
    }

    public Date getTanggalLaku() {
        return tanggalLaku;
    }

    public void setTanggalLaku(Date tanggalLaku) {
        this.tanggalLaku = tanggalLaku;
    }

    public Boolean getStatuslaku() {
        return statuslaku;
    }

    public void setStatuslaku(Boolean statuslaku) {
        this.statuslaku = statuslaku;
    }

    public Double getHargaBeli() {
        return hargaBeli;
    }

    public void setHargaBeli(Double hargaBeli) {
        this.hargaBeli = hargaBeli;
    }

    public Double getHargaJual() {
        return hargaJual;
    }

    public void setHargaJual(Double hargaJual) {
        this.hargaJual = hargaJual;
    }
}
