/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

/**
 *
 * @author lukman
 */
public class SetoranHarian {
    private Double cekPenjualan;
    private Double cekPembelian;
    private Double cekCicilanHutang;
    private Double cekCicilanPiutang;
    private Double cekHutang;
    private Double cekPiutang;
    private Double cekReturPembelian;
    private Double cekReturPenjualan;
    private Double cekBarangHilang;

    public Double getCekPenjualan() {
        return cekPenjualan;
    }

    public void setCekPenjualan(Double cekPenjualan) {
        this.cekPenjualan = cekPenjualan;
    }

    public Double getCekPembelian() {
        return cekPembelian;
    }

    public void setCekPembelian(Double cekPembelian) {
        this.cekPembelian = cekPembelian;
    }

    public Double getCekCicilanHutang() {
        return cekCicilanHutang;
    }

    public void setCekCicilanHutang(Double cekCicilanHutang) {
        this.cekCicilanHutang = cekCicilanHutang;
    }

    public Double getCekCicilanPiutang() {
        return cekCicilanPiutang;
    }

    public void setCekCicilanPiutang(Double cekCicilanPiutang) {
        this.cekCicilanPiutang = cekCicilanPiutang;
    }

    public Double getCekHutang() {
        return cekHutang;
    }

    public void setCekHutang(Double cekHutang) {
        this.cekHutang = cekHutang;
    }

    public Double getCekPiutang() {
        return cekPiutang;
    }

    public void setCekPiutang(Double cekPiutang) {
        this.cekPiutang = cekPiutang;
    }

    public Double getCekReturPembelian() {
        return cekReturPembelian;
    }

    public void setCekReturPembelian(Double cekReturPembelian) {
        this.cekReturPembelian = cekReturPembelian;
    }

    public Double getCekReturPenjualan() {
        return cekReturPenjualan;
    }

    public void setCekReturPenjualan(Double cekReturPenjualan) {
        this.cekReturPenjualan = cekReturPenjualan;
    }

    public Double getCekBarangHilang() {
        return cekBarangHilang;
    }

    public void setCekBarangHilang(Double cekBarangHilang) {
        this.cekBarangHilang = cekBarangHilang;
    }
}
