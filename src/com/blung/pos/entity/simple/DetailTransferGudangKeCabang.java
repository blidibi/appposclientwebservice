/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

/**
 * simple
 * @author lukman
 */
public class DetailTransferGudangKeCabang {
    private Long idDetailTransferGudangKeCabang;
    private Long idTransferGudangKeCabang;
    private Long idBarang;
    private Long jumlah;

    public Long getIdDetailTransferGudangKeCabang() {
        return idDetailTransferGudangKeCabang;
    }

    public void setIdDetailTransferGudangKeCabang(Long idDetailTransferGudangKeCabang) {
        this.idDetailTransferGudangKeCabang = idDetailTransferGudangKeCabang;
    }

    public Long getIdTransferGudangKeCabang() {
        return idTransferGudangKeCabang;
    }

    public void setIdTransferGudangKeCabang(Long idTransferGudangKeCabang) {
        this.idTransferGudangKeCabang = idTransferGudangKeCabang;
    }

    public Long getIdBarang() {
        return idBarang;
    }

    public void setIdBarang(Long idBarang) {
        this.idBarang = idBarang;
    }

    public Long getJumlah() {
        return jumlah;
    }

    public void setJumlah(Long jumlah) {
        this.jumlah = jumlah;
    }
}
