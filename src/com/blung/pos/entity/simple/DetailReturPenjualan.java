/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

/**
 * simple
 * @author lukman
 */
public class DetailReturPenjualan {
    private Long idDetailReturPenjualan;
    private Long idBarang;
    private int jumlah;
    private Double harga;
    private String keterangan;
    private Long idReturPenjualan;

    public Long getIdDetailReturPenjualan() {
        return idDetailReturPenjualan;
    }

    public void setIdDetailReturPenjualan(Long idDetailReturPenjualan) {
        this.idDetailReturPenjualan = idDetailReturPenjualan;
    }

    public Long getIdBarang() {
        return idBarang;
    }

    public void setIdBarang(Long idBarang) {
        this.idBarang = idBarang;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public Double getHarga() {
        return harga;
    }

    public void setHarga(Double harga) {
        this.harga = harga;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Long getIdReturPenjualan() {
        return idReturPenjualan;
    }

    public void setIdReturPenjualan(Long idReturPenjualan) {
        this.idReturPenjualan = idReturPenjualan;
    }
}
