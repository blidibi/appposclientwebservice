/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

/**
 * simple
 * @author lukman
 */
public class PiutangPenjualan {
    private Long idPiutangPenjualan;
    private Long idPenjualan;
    private Long idPelanggan;
    private Boolean statusLunas;
    private String keterangan;
    private Boolean verified=false;

    public Long getIdPiutangPenjualan() {
        return idPiutangPenjualan;
    }

    public void setIdPiutangPenjualan(Long idPiutangPenjualan) {
        this.idPiutangPenjualan = idPiutangPenjualan;
    }

    public Long getIdPenjualan() {
        return idPenjualan;
    }

    public void setIdPenjualan(Long idPenjualan) {
        this.idPenjualan = idPenjualan;
    }

    public Long getIdPelanggan() {
        return idPelanggan;
    }

    public void setIdPelanggan(Long idPelanggan) {
        this.idPelanggan = idPelanggan;
    }

    public Boolean getStatusLunas() {
        return statusLunas;
    }

    public void setStatusLunas(Boolean statusLunas) {
        this.statusLunas = statusLunas;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }
}
