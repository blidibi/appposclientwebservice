/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

import java.util.Date;

/**
 * simple
 * @author lukman
 */
public class Penjualan {
    private Long idPenjualan;
    private Double totalHarga;
    private Double diskonPelanggan;
    private Double potonganLangsung;
    private Date tanggalJual;
    private Long idUser;
    private Boolean statusDikirim;
    private Long idKasir;
    private Long idPelanggan;
    private Boolean statusJual;
    private Boolean verified=false;

    public Long getIdPenjualan() {
        return idPenjualan;
    }

    public void setIdPenjualan(Long idPenjualan) {
        this.idPenjualan = idPenjualan;
    }

    public Double getTotalHarga() {
        return totalHarga;
    }

    public void setTotalHarga(Double totalHarga) {
        this.totalHarga = totalHarga;
    }

    public Double getDiskonPelanggan() {
        return diskonPelanggan;
    }

    public void setDiskonPelanggan(Double diskonPelanggan) {
        this.diskonPelanggan = diskonPelanggan;
    }

    public Double getPotonganLangsung() {
        return potonganLangsung;
    }

    public void setPotonganLangsung(Double potonganLangsung) {
        this.potonganLangsung = potonganLangsung;
    }

    public Date getTanggalJual() {
        return tanggalJual;
    }

    public void setTanggalJual(Date tanggalJual) {
        this.tanggalJual = tanggalJual;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Boolean getStatusDikirim() {
        return statusDikirim;
    }

    public void setStatusDikirim(Boolean statusDikirim) {
        this.statusDikirim = statusDikirim;
    }

    public Long getIdKasir() {
        return idKasir;
    }

    public void setIdKasir(Long idKasir) {
        this.idKasir = idKasir;
    }

    public Long getIdPelanggan() {
        return idPelanggan;
    }

    public void setIdPelanggan(Long idPelanggan) {
        this.idPelanggan = idPelanggan;
    }

    public Boolean getStatusJual() {
        return statusJual;
    }

    public void setStatusJual(Boolean statusJual) {
        this.statusJual = statusJual;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }
}
