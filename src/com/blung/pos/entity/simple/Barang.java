/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

/**
 * simple
 * @author lukman
 */

public class Barang{
    private Long idBarang;
    private String idBarangBarcode;
    private String namaBarang;
    private Harga harga;

    public Long getIdBarang() {
        return idBarang;
    }

    public void setIdBarang(Long idBarang) {
        this.idBarang = idBarang;
    }

    public String getIdBarangBarcode() {
        return idBarangBarcode;
    }

    public void setIdBarangBarcode(String idBarangBarcode) {
        this.idBarangBarcode = idBarangBarcode;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public Harga getHarga() {
        return harga;
    }

    public void setHarga(Harga harga) {
        this.harga = harga;
    }
    
}
