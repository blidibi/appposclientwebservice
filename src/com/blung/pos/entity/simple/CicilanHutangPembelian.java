/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

import java.util.Date;

/**
 * simple
 * @author lukman
 */
public class CicilanHutangPembelian {
    private Long idCicilanHutangPembelian;
    private Long idHutangPembelian;
    private Date tanggalBayar;
    private Long idUser;
    private Double jumlahBayar;
    private String keterangan;
    private Boolean verified=false;

    public Long getIdCicilanHutangPembelian() {
        return idCicilanHutangPembelian;
    }

    public void setIdCicilanHutangPembelian(Long idCicilanHutangPembelian) {
        this.idCicilanHutangPembelian = idCicilanHutangPembelian;
    }

    public Long getIdHutangPembelian() {
        return idHutangPembelian;
    }

    public void setIdHutangPembelian(Long idHutangPembelian) {
        this.idHutangPembelian = idHutangPembelian;
    }

    public Date getTanggalBayar() {
        return tanggalBayar;
    }

    public void setTanggalBayar(Date tanggalBayar) {
        this.tanggalBayar = tanggalBayar;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Double getJumlahBayar() {
        return jumlahBayar;
    }

    public void setJumlahBayar(Double jumlahBayar) {
        this.jumlahBayar = jumlahBayar;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }
}
