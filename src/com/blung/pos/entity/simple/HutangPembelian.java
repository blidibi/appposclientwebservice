/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

/**
 * simple
 * @author lukman
 */
public class HutangPembelian {
    private Long idHutangPembelian;
    private Long idPembelian;
    private Boolean statusLunas;
    private String keterangan;
    private Boolean verified=false;

    public Long getIdHutangPembelian() {
        return idHutangPembelian;
    }

    public void setIdHutangPembelian(Long idHutangPembelian) {
        this.idHutangPembelian = idHutangPembelian;
    }

    public Long getIdPembelian() {
        return idPembelian;
    }

    public void setIdPembelian(Long idPembelian) {
        this.idPembelian = idPembelian;
    }

    public Boolean getStatusLunas() {
        return statusLunas;
    }

    public void setStatusLunas(Boolean statusLunas) {
        this.statusLunas = statusLunas;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }
}
