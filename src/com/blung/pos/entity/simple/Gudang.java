/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.entity.simple;

/**
 * simple
 * @author lukman
 */
public class Gudang {
    private Long idGudang;
    private String namaGudang;
    private Long idCabang;

    public Long getIdGudang() {
        return idGudang;
    }

    public void setIdGudang(Long idGudang) {
        this.idGudang = idGudang;
    }

    public String getNamaGudang() {
        return namaGudang;
    }

    public void setNamaGudang(String namaGudang) {
        this.namaGudang = namaGudang;
    }

    public Long getIdCabang() {
        return idCabang;
    }

    public void setIdCabang(Long idCabang) {
        this.idCabang = idCabang;
    }
    
}
