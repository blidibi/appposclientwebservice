/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.service.intrf;

import com.blung.pos.entity.bantuan.JmlBarang;
import com.blung.pos.entity.simple.Absensi;
import com.blung.pos.entity.simple.Barang;
import com.blung.pos.entity.simple.Cabang;
import com.blung.pos.entity.simple.Gudang;
import com.blung.pos.entity.simple.Karyawan;
import com.blung.pos.entity.simple.Kasir;
import com.blung.pos.entity.simple.Kategori;
import com.blung.pos.entity.simple.KategoriPelanggan;
import com.blung.pos.entity.simple.LogLogin;
import com.blung.pos.entity.simple.Pelanggan;
import com.blung.pos.entity.simple.Satuan;
import com.blung.pos.entity.simple.SettingAplikasi;
import com.blung.pos.entity.simple.Supliyer;
import com.blung.pos.entity.simple.User;
import java.util.Date;
import java.util.List;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author lukman
 */
public interface MasterSimpleIntrf {
    
    void setMasterSimpleRestAcces(RestTemplate restTemplate);
    
    List<Absensi> getAllAbsensi(Date start,Date end);
    List<Absensi> getAllAbsensiByKaryawan(Karyawan k,Date start,Date end);
    List<Absensi> getAllAbsensiByCabang(Cabang c,Date start,Date end);
    
    Boolean save(Barang b);
    Boolean update(Barang b);
    Boolean delete(Barang b);
    Barang getBarangByID(Long ID);
    Barang getBarangByIDAndCabang(String ID,Cabang c);
    Barang getBarangByIDAndCabang(Long ID,Cabang c);
    List<Barang> getAllBarang();
    List<Barang> getAllBarangByCabang(Cabang c);
    JmlBarang getJmlBarangByCabangAndID(Cabang c,Barang b);
    List<JmlBarang> getAllJmlBarangByCabang(Cabang c);
    
    Boolean save(Cabang c);
    Boolean update(Cabang c);
    Boolean delete(Cabang c);
    Cabang getCabangByID(Long ID);
    List<Cabang> getAllCabang();
    
    Boolean save(Gudang g);
    Boolean update(Gudang g);
    Boolean delete(Gudang g);
    Gudang getGudangByID(Long ID);
    List<Gudang> getGudangByCabang(Cabang c);
    List<Gudang> getAllGudang();
    
    Boolean save(Karyawan k);
    Boolean update(Karyawan k);
    Boolean delete(Karyawan k);
    Karyawan getKaryawanByID(Long ID);
    List<Karyawan> getAllKaryawan();
    List<Karyawan> getAllKaryawanByCabang(Cabang c);
    
    Boolean save(Kasir k);
    Boolean update(Kasir k);
    Boolean delete(Kasir k);
    Kasir getKasirByID(Long ID);
    List<Kasir> getAllKasir();
    List<Kasir> getAllKasirByCabang(Cabang c);
    
    Boolean save(Kategori k);
    Boolean update(Kategori k);
    Boolean delete(Kategori k);
    Kategori getKategoriByID(Long ID);
    List<Kategori> getAllKategori();
    
    Boolean save(KategoriPelanggan kp);
    Boolean update(KategoriPelanggan kp);
    Boolean delete(KategoriPelanggan kp);
    KategoriPelanggan getKategoriPelangganByID(Long ID);
    List<KategoriPelanggan> getAllKategoriPelanggan();
    
    Boolean save(Pelanggan p);
    Boolean update(Pelanggan p);
    Boolean delete(Pelanggan p);
    Pelanggan getPelangganByID(Long ID);
    List<Pelanggan> getAllPelanggan();
    
    Boolean save(Satuan s);
    Boolean update(Satuan s);
    Boolean delete(Satuan s);
    Satuan getSatuanByID(Long ID);
    List<Satuan> getAllSatuan();
    
    Boolean save(SettingAplikasi sa);
    Boolean update(SettingAplikasi sa);
    Boolean delete(SettingAplikasi sa);
    Satuan getSettingAplikasiByKasir(Kasir k);
    Satuan getSettingAplikasiByKasirByID(Long ID);
    List<SettingAplikasi> getAllSettingAplikasi();
    
    Boolean save(Supliyer s);
    Boolean update(Supliyer s);
    Boolean delete(Supliyer s);
    Supliyer getSupliyerByID(Long ID);
    List<Supliyer> getAllSupliyer();
        
    Boolean save(User u);
    void saveLogin(LogLogin ll);
    Boolean update(User u);
    Boolean delete(User u);
    User getUserByID(Long ID);
    List<User> getAllUser();
    User getUserByUserAndPass(String username,String password);
    User getAllUserByID(Long ID);
    List<User> getAllUserByCabang(Cabang c);
    
}
