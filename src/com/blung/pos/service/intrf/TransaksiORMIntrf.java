/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.service.intrf;

import com.blung.pos.entity.orm.Pembelian;
import com.blung.pos.entity.orm.Penjualan;
import com.blung.pos.entity.orm.ReturPembelian;
import com.blung.pos.entity.orm.ReturPenjualan;
import com.blung.pos.entity.orm.Servis;
import com.blung.pos.entity.orm.TransferGudang;
import com.blung.pos.entity.orm.TransferGudangKeCabang;
import com.blung.pos.entity.simple.Cabang;
import com.blung.pos.entity.simple.Kasir;
import java.util.Date;
import java.util.List;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author lukman
 */
public interface TransaksiORMIntrf {
    
    void setTransaksiORMpostRestAcces(RestTemplate restTemplate);
    
    Boolean savePenjualan(Penjualan penjualan);
    Boolean savePembelian(Pembelian pembelian);
    Boolean updatePembelian(com.blung.pos.entity.simple.Pembelian pembelian);
    com.blung.pos.entity.simple.Pembelian getPembelianById(Long idPembelian);
    List<com.blung.pos.entity.simple.Pembelian> getPembelianAll(Date start,Date end);
    List<com.blung.pos.entity.simple.Pembelian> getPembelianBySupliyer(Long idSupliyer,Date start,Date end);
    List<com.blung.pos.entity.simple.Pembelian> getPembelianByCabang(Long idCabang,Date start,Date end);
    List<com.blung.pos.entity.simple.Pembelian> getPembelianByCabangAndSupliyer(Long idCabang,Long idSupliyer,Date start,Date end);
    List<com.blung.pos.entity.simple.Pembelian> getPembelianBelumPindahGudangByCabang(Long idCabang);
    List<com.blung.pos.entity.simple.Pembelian> getPembelianBelumRekapByCabangAndUser(Long idc,Long idu);
    com.blung.pos.entity.simple.Penjualan getPenjualanById(Long idPenjualan);
    com.blung.pos.entity.simple.Penjualan getLastPenjualanByCbAndKs(Cabang c,Kasir k);
    List<com.blung.pos.entity.simple.Penjualan> getpenjualanAll(Date start,Date end);
    List<com.blung.pos.entity.simple.Penjualan> getpenjualanByCabang(Long idCabang,Date start,Date end);
    List<com.blung.pos.entity.simple.Penjualan> getpenjualanBelumRekapByCabangAndUser(Long idCabang,Long idUser);
    Boolean saveTransferGudangKeGudang(TransferGudang transferGudang);
    Boolean saveTransferGudangKeCabang(TransferGudangKeCabang tgkc);
    Boolean saveReturPenjualan(ReturPenjualan returPenjualan);
    Boolean saveReturPembelian(ReturPembelian returPembelian);
    
    Boolean save(Servis s);
    
}
