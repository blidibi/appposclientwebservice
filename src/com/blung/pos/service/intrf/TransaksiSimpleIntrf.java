/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.service.intrf;

import com.blung.pos.entity.simple.ReturPembelian;
import com.blung.pos.entity.simple.BarangHilang;
import com.blung.pos.entity.simple.Cabang;
import com.blung.pos.entity.simple.CicilanHutangPembelian;
import com.blung.pos.entity.simple.CicilanPiutangPenjualan;
import com.blung.pos.entity.simple.DetailPembelian;
import com.blung.pos.entity.simple.DetailPenjualan;
import com.blung.pos.entity.simple.DetailReturPembelian;
import com.blung.pos.entity.simple.DetailReturPenjualan;
import com.blung.pos.entity.simple.DetailServis;
import com.blung.pos.entity.simple.DetailTransferGudang;
import com.blung.pos.entity.simple.DetailTransferGudangKeCabang;
import com.blung.pos.entity.simple.HutangPembelian;
import com.blung.pos.entity.simple.PiutangPenjualan;
import com.blung.pos.entity.simple.ReturPenjualan;
import com.blung.pos.entity.simple.Servis;
import com.blung.pos.entity.simple.TransferGudang;
import com.blung.pos.entity.simple.TransferGudangKeCabang;
import java.util.Date;
import java.util.List;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author user
 */
public interface TransaksiSimpleIntrf {
    public void setTransaksiRestAccess(RestTemplate restTemplate);
    
    Boolean save(HutangPembelian hp);
    HutangPembelian getHutangPembelianByID(Long idHutangPembelian);
    List<HutangPembelian> getHutangPembelianAll(Date start, Date end);
    List<HutangPembelian> getHutangPembelianByCabang(Date start, Date end, Long idCabang);
    List<HutangPembelian> getHutangPembelianBySupliyer(Date start, Date end, Long idSupliyer);
    List<HutangPembelian> getHutangPembelianByCabangDanSupliyer(Date start, Date end, Long idCabang, Long idSupliyer);
    
    Boolean save(PiutangPenjualan pp);
    PiutangPenjualan getPiutangPenjualanById(Long idPiutangPenjualan);
    List<PiutangPenjualan> getPiutangPenjualanAll(Long idPiutangPenjualan);
    List<PiutangPenjualan> getPiutangPenjualanByCabang(Date start, Date end, Long idCabang);
    List<PiutangPenjualan> getPiutangPenjualanByPelanggan(Date start, Date end, Long idPelanggan);
    List<PiutangPenjualan> getPiutangPenjualanByCabangDanPelanggan(Date start, Date end, Long idCabang, Long idPelanggan);
    
    Boolean save(CicilanHutangPembelian chp);
    List<CicilanHutangPembelian> getCicilanHutangPembelianByHutangPembelian(Long idHutangPembelian);
    List<CicilanHutangPembelian> getCicilanHutangPembelianAll();
    List<CicilanHutangPembelian> getCicilanHutangPembelianByCabangAndDate(Date start, Date end, Long idCabang);
    CicilanHutangPembelian getCicilanHutangPembelianById(Long idHutangPembelian);
    
    Boolean save(CicilanPiutangPenjualan cpp);
    List<CicilanPiutangPenjualan> getCicilanPiutangPenjualanByPiutangPenjualan(Long idPiutangPenjualan);
    List<CicilanPiutangPenjualan> getCicilanPiutangPenjualanAll(Long idCicilanPiutangPenjualan);
    List<CicilanPiutangPenjualan> getCicilanPiutangPenjualanByCabangAndDate(Date start, Date end, Long idCabang);
    CicilanPiutangPenjualan getCicilanPiutangPenjualanById(Long idCicilanPiutangPenjualan);
    
    DetailPenjualan getDetailPenjualanByid(Long id);
    List<DetailPenjualan> getDetailPenjualanByPenjualan(Long idPenjualan);
    List<DetailPenjualan> getDetailPenjualanByCabang(Date start, Date end,Long idCabang);
    List<DetailPenjualan> getDetailPenjualanByBarang(Long idBarang);
    
    DetailPembelian getDetailPembelianById(Long id);
    List<DetailPembelian> getDetailPembelianByPembelian(Long idPembelian);
    List<DetailPembelian> getDetailPembelianByCabang(Date start, Date end,Long idCabang);
    List<DetailPembelian> getDetailPembelianByBarang(Long idBarang);
    
    Boolean save(List<BarangHilang> l);
    List<BarangHilang> getBarangHilangByCabang(Date start, Date end,Cabang c);
    
    TransferGudang getTransferGudangByID(Long id);
    List<TransferGudang> getTransferGudangByCabangAndDate(Date start, Date end,Cabang c);
    List<DetailTransferGudang> getDetailTransferGudangByTransferGudang(Long ID);
    List<DetailTransferGudang> getTransferGudangByCabangAndDate();
    
    TransferGudangKeCabang getTransferGudangKeCabangByID(Long ID);
    List<TransferGudangKeCabang> getTransferGudangKeCabangByCabangAndDate();
    List<DetailTransferGudangKeCabang> getDetailTransferGudangKeCabangByTransferGudangKeCabang(Long id);
    List<DetailTransferGudangKeCabang> getDetailTransferGudangKeCabangByCabangAndDate(Date start, Date end,Cabang c);
    
    ReturPembelian getReturPembelianByID(Long ID);
    List<ReturPembelian> getReturPembelianByCabangAndDate(Date start, Date end,Cabang c);
    List<DetailReturPembelian> getDetailReturPembelianByPembelian(Long id);
    List<DetailReturPembelian> getDetailReturPembelianByCabangAndDate(Date start, Date end,Cabang c);
    
    ReturPenjualan getReturPenjualanByID(Long ID);
    List<ReturPenjualan> getReturPenjualanByCabangAndDate(Date start, Date end,Cabang c);
    List<DetailReturPenjualan> getDetailReturPenjualanByPembelian(Long id);
    List<DetailReturPenjualan> getDetailReturPenjualanByCabangAndDate(Date start, Date end,Cabang c);
    
    //------------------------------------------------------------
    Boolean save(Servis s);
    Boolean update(Servis s);
    List<Servis> servisSudahByCabangAndDate(Date start, Date end,Cabang c);
    List<Servis> servisBelumByCabangAndDate(Date start, Date end,Cabang c);
    
    Boolean save(DetailServis s);
    Boolean update(DetailServis s);
    List<DetailServis> detailservisByServis(Long id);
    List<DetailServis> detailservisByCabangAndDate(Date start, Date end,Cabang c);
    
}