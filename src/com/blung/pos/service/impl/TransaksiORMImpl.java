/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.service.impl;

import com.blung.pos.entity.orm.Servis;
import com.blung.pos.entity.simple.Cabang;
import com.blung.pos.entity.simple.Kasir;
import com.blung.pos.entity.simple.Penjualan;
import com.blung.pos.service.intrf.TransaksiORMIntrf;
import com.blung.pos.service.rest.dataacces.TransaksiORMpostRestAcces;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author lukman
 */
@Service("transaksiorm")
public class TransaksiORMImpl implements TransaksiORMIntrf{

    TransaksiORMpostRestAcces tormra;
    @Override
    public void setTransaksiORMpostRestAcces(RestTemplate restTemplate) {
        this.tormra = new TransaksiORMpostRestAcces(restTemplate);
    }

    @Override
    public Boolean savePenjualan(com.blung.pos.entity.orm.Penjualan penjualan) {
        return tormra.save(penjualan);
    }

    @Override
    public Boolean savePembelian(com.blung.pos.entity.orm.Pembelian pembelian) {
        return tormra.save(pembelian);
    }

    @Override
    public Boolean updatePembelian(com.blung.pos.entity.simple.Pembelian pembelian) {
        return tormra.update(pembelian);
    }
    
    @Override
    public Boolean saveTransferGudangKeGudang(com.blung.pos.entity.orm.TransferGudang transferGudang) {
        return tormra.save(transferGudang);
    }

    @Override
    public Boolean saveTransferGudangKeCabang(com.blung.pos.entity.orm.TransferGudangKeCabang tgkc) {
        return tormra.save(tgkc);
    }

    @Override
    public Boolean saveReturPenjualan(com.blung.pos.entity.orm.ReturPenjualan returPenjualan) {
        return tormra.save(returPenjualan);
    }

    @Override
    public Boolean saveReturPembelian(com.blung.pos.entity.orm.ReturPembelian returPembelian) {
        return tormra.save(returPembelian);
    }

    @Override
    public com.blung.pos.entity.simple.Pembelian getPembelianById(Long idPembelian) {
        return tormra.getPembelianById(idPembelian);
    }

    @Override
    public List<com.blung.pos.entity.simple.Pembelian> getPembelianAll(Date start,Date end) {
        List<com.blung.pos.entity.simple.Pembelian> l=tormra.getPembelianAll(start, end);
        if(l==null) return new ArrayList<com.blung.pos.entity.simple.Pembelian>();
        return l;
    }

    @Override
    public List<com.blung.pos.entity.simple.Pembelian> getPembelianBySupliyer(Long idSupliyer,Date start,Date end) {
        List<com.blung.pos.entity.simple.Pembelian> l=tormra.getPembelianBySupliyer(idSupliyer, start, end);
        if(l==null) return new ArrayList<com.blung.pos.entity.simple.Pembelian>();
        return l;
    }

    @Override
    public List<com.blung.pos.entity.simple.Pembelian> getPembelianByCabang(Long idCabang,Date start,Date end) {
        List<com.blung.pos.entity.simple.Pembelian> l=tormra.getPembelianByCabang(idCabang, start, end);
        if(l==null) return new ArrayList<com.blung.pos.entity.simple.Pembelian>();
        return l;
    }

    @Override
    public List<com.blung.pos.entity.simple.Pembelian> getPembelianByCabangAndSupliyer(Long idCabang, Long idSupliyer, Date start, Date end) {
        List<com.blung.pos.entity.simple.Pembelian> l=tormra.getPembelianBySupliyerAndCabang(idCabang, idSupliyer, start, end);
        if(l==null) return new ArrayList<com.blung.pos.entity.simple.Pembelian>();
        return l;
    }
    
    @Override
    public List<com.blung.pos.entity.simple.Pembelian> getPembelianBelumPindahGudangByCabang(Long idCabang) {
        List<com.blung.pos.entity.simple.Pembelian> l=tormra.getPembelianBelumPindahGudangByCabang(idCabang);
        if(l==null) return new ArrayList<com.blung.pos.entity.simple.Pembelian>();
        return l;
    }
    
    @Override
    public List<com.blung.pos.entity.simple.Pembelian> getPembelianBelumRekapByCabangAndUser(Long idc, Long idu) {
        List<com.blung.pos.entity.simple.Pembelian> l=tormra.getPembelianBelumRekapByCabangAndUser(idu, idu);
        if(l==null) return new ArrayList<com.blung.pos.entity.simple.Pembelian>();
        return l;
    }

    @Override
    public Penjualan getPenjualanById(Long idPenjualan) {
        return tormra.getPenjualanByID(idPenjualan);
    }
    
    @Override
    public Penjualan getLastPenjualanByCbAndKs(Cabang c, Kasir k) {
        return tormra.getLastPenjualanByCbAndKs(c.getIdCabang(), k.getIdKasir());
    }

    @Override
    public List<com.blung.pos.entity.simple.Penjualan> getpenjualanAll(Date start,Date end) {
        List<com.blung.pos.entity.simple.Penjualan> l=tormra.getPenjualanAll(start, end);
        if(l==null) return new ArrayList<com.blung.pos.entity.simple.Penjualan>();
        return l;
    }

    @Override
    public List<Penjualan> getpenjualanByCabang(Long idCabang, Date start, Date end) {
        List<com.blung.pos.entity.simple.Penjualan> l=tormra.getPenjualanByCabang(idCabang, start, end);
        if(l==null) return new ArrayList<com.blung.pos.entity.simple.Penjualan>();
        return l;
    }

    @Override
    public List<Penjualan> getpenjualanBelumRekapByCabangAndUser(Long idCabang, Long idUser) {
        List<com.blung.pos.entity.simple.Penjualan> l=tormra.getPenjualanBelumRekapByCabangAndUser(idCabang, idUser);
        if(l==null) return new ArrayList<com.blung.pos.entity.simple.Penjualan>();
        return l;
    }

    @Override
    public Boolean save(Servis s) {
        return tormra.save(s);
    }

}
