/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.service.impl;

import com.blung.pos.entity.bantuan.JmlBarang;
import com.blung.pos.entity.simple.Absensi;
import com.blung.pos.entity.simple.Barang;
import com.blung.pos.entity.simple.Cabang;
import com.blung.pos.entity.simple.Gudang;
import com.blung.pos.entity.simple.Karyawan;
import com.blung.pos.entity.simple.Kasir;
import com.blung.pos.entity.simple.Kategori;
import com.blung.pos.entity.simple.KategoriPelanggan;
import com.blung.pos.entity.simple.LogLogin;
import com.blung.pos.entity.simple.Pelanggan;
import com.blung.pos.entity.simple.Satuan;
import com.blung.pos.entity.simple.SettingAplikasi;
import com.blung.pos.entity.simple.Supliyer;
import com.blung.pos.entity.simple.User;
import com.blung.pos.service.intrf.MasterSimpleIntrf;
import com.blung.pos.service.rest.dataacces.MasterSimpleRestAcces;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author lukman
 */
@Service("mastersimple")
public class MasterSimpleImpl implements MasterSimpleIntrf{

    MasterSimpleRestAcces msra;

    @Override
    public void setMasterSimpleRestAcces(RestTemplate restTemplate){
        this.msra = new MasterSimpleRestAcces(restTemplate);
    }

    @Override
    public List<Absensi> getAllAbsensi(Date start, Date end) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Absensi> getAllAbsensiByKaryawan(Karyawan k, Date start, Date end) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Absensi> getAllAbsensiByCabang(Cabang c, Date start, Date end) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Boolean save(Barang b) {
        return msra.save(b);
    }

    @Override
    public Boolean update(Barang b) {
        return msra.update(b);
    }

    @Override
    public Boolean delete(Barang b) {
        return msra.delete(b);
    }

    @Override
    public Barang getBarangByID(Long ID) {
        return msra.getBarangById(ID);
    }

    @Override
    public Barang getBarangByIDAndCabang(String ID,Cabang c) {
        return msra.getBarangById(ID, c.getIdCabang());
    }
    
    @Override
    public Barang getBarangByIDAndCabang(Long ID, Cabang c) {
        return msra.getBarangById(ID, c.getIdCabang());
    }

    @Override
    public List<Barang> getAllBarang() {
        List<Barang> l=msra.getBarangAll();
        if(l==null) return new ArrayList<Barang>();
        else return l;
    }
    
    @Override
    public List<Barang> getAllBarangByCabang(Cabang c) {
        List<Barang> l=msra.getBarangAllByCabang(c.getIdCabang());
        if(l==null) return new ArrayList<Barang>();
        else return l;
    }
    
    @Override
    public JmlBarang getJmlBarangByCabangAndID(Cabang c, Barang b) {
        return msra.getJmlBarangByCabangAndID(c.getIdCabang(), b.getIdBarang());
    }

    @Override
    public List<JmlBarang> getAllJmlBarangByCabang(Cabang c) {
        List<JmlBarang> l=msra.getJmlBarangAllByCabang(c.getIdCabang());
        if(l==null) return new ArrayList<JmlBarang>();
        else return l;
    }

    @Override
    public Boolean save(Cabang c) {
        return msra.save(c);
    }

    @Override
    public Boolean update(Cabang c) {
        return msra.update(c);
    }

    @Override
    public Boolean delete(Cabang c) {
        return msra.delete(c);
    }

    @Override
    public Cabang getCabangByID(Long ID) {
        return msra.getCabangById(ID);
    }

    @Override
    public List<Cabang> getAllCabang() {
        List<Cabang> l=msra.getCabangAll();
        if(l==null) return new ArrayList<Cabang>();
        else return l;
    }

    @Override
    public Boolean save(Gudang g) {
        return msra.save(g);
    }

    @Override
    public Boolean update(Gudang g) {
        return msra.update(g);
    }

    @Override
    public Boolean delete(Gudang g) {
        return msra.delete(g);
    }

    @Override
    public Gudang getGudangByID(Long ID) {
        return msra.getGudangById(ID);
    }
    
    @Override
    public List<Gudang> getGudangByCabang(Cabang c) {
        List<Gudang> l=msra.getGudangByCabang(c.getIdCabang());
        if(l==null) return new ArrayList<Gudang>();
        else return l;
    }

    @Override
    public List<Gudang> getAllGudang() {
        List<Gudang> l=msra.getGudangAll();
        if(l==null) return new ArrayList<Gudang>();
        else return l;
    }

    @Override
    public Boolean save(Karyawan k) {
        return msra.save(k);
    }

    @Override
    public Boolean update(Karyawan k) {
        return msra.update(k);
    }

    @Override
    public Boolean delete(Karyawan k) {
        return msra.delete(k);
    }

    @Override
    public Karyawan getKaryawanByID(Long ID) {
        return msra.getKaryawanById(ID);
    }

    @Override
    public List<Karyawan> getAllKaryawan() {
        List<Karyawan> l = msra.getKaryawanAll();
        if(l==null) return new ArrayList<Karyawan>();
        else return l;
    }

    @Override
    public List<Karyawan> getAllKaryawanByCabang(Cabang c) {
        List<Karyawan> l = msra.getKaryawanByCabang(c.getIdCabang());
        if(l==null) return new ArrayList<Karyawan>();
        else return l;
    }

    @Override
    public Boolean save(Kasir k) {
        return msra.save(k);
    }

    @Override
    public Boolean update(Kasir k) {
        return msra.update(k);
    }

    @Override
    public Boolean delete(Kasir k) {
        return msra.delete(k);
    }

    @Override
    public Kasir getKasirByID(Long ID) {
        return msra.getKasirById(ID);
    }

    @Override
    public List<Kasir> getAllKasir() {
        List<Kasir> l=msra.getKasirAll();
        if(l==null) return new ArrayList<Kasir>();
        else return l;
    }

    @Override
    public List<Kasir> getAllKasirByCabang(Cabang c) {
        List<Kasir> l=msra.getKasirByCabang(c.getIdCabang());
        if(l==null) return new ArrayList<Kasir>();
        else return l;
    }

    @Override
    public Boolean save(Kategori k) {
        return msra.save(k);
    }

    @Override
    public Boolean update(Kategori k) {
        return msra.update(k);
    }

    @Override
    public Boolean delete(Kategori k) {
        return msra.delete(k);
    }

    @Override
    public Kategori getKategoriByID(Long ID) {
        return msra.getKategoriById(ID);
    }

    @Override
    public List<Kategori> getAllKategori() {
        List<Kategori> l=msra.getKategoriAll();
        if(l==null) return new ArrayList<Kategori>();
        else return l;
    }

    @Override
    public Boolean save(KategoriPelanggan kp) {
        return msra.save(kp);
    }

    @Override
    public Boolean update(KategoriPelanggan kp) {
        return msra.update(kp);
    }

    @Override
    public Boolean delete(KategoriPelanggan kp) {
        return msra.delete(kp);
    }

    @Override
    public KategoriPelanggan getKategoriPelangganByID(Long ID) {
        return msra.getKategoriPelangganById(ID);
    }

    @Override
    public List<KategoriPelanggan> getAllKategoriPelanggan() {
        List<KategoriPelanggan> l=msra.getKategoriPelangganAll();
        if(l==null) return new ArrayList<KategoriPelanggan>();
        else return l;
    }

    @Override
    public Boolean save(Pelanggan p) {
        return msra.save(p);
    }

    @Override
    public Boolean update(Pelanggan p) {
        return msra.update(p);
    }

    @Override
    public Boolean delete(Pelanggan p) {
        return msra.delete(p);
    }

    @Override
    public Pelanggan getPelangganByID(Long ID) {
        return msra.getPelangganById(ID);
    }

    @Override
    public List<Pelanggan> getAllPelanggan() {
        List<Pelanggan> l=msra.getPelangganAll();
        if(l==null) return new ArrayList<Pelanggan>();
        else return l;
    }

    @Override
    public Boolean save(Satuan s) {
        return msra.save(s);
    }

    @Override
    public Boolean update(Satuan s) {
        return msra.update(s);
    }

    @Override
    public Boolean delete(Satuan s) {
        return msra.delete(s);
    }

    @Override
    public Satuan getSatuanByID(Long ID) {
        return msra.getSatuanById(ID);
    }

    @Override
    public List<Satuan> getAllSatuan() {
        List<Satuan> l=msra.getSatuanAll();
        if(l==null) return new ArrayList<Satuan>();
        else return l;
    }

    @Override
    public Boolean save(SettingAplikasi sa) {
        return msra.save(sa);
    }

    @Override
    public Boolean update(SettingAplikasi sa) {
        return msra.update(sa);
    }

    @Override
    public Boolean delete(SettingAplikasi sa) {
        return msra.delete(sa);
    }

    @Override
    public Satuan getSettingAplikasiByKasir(Kasir k) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Satuan getSettingAplikasiByKasirByID(Long ID) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<SettingAplikasi> getAllSettingAplikasi() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Boolean save(Supliyer s) {
        return msra.save(s);
    }

    @Override
    public Boolean update(Supliyer s) {
        return msra.update(s);
    }

    @Override
    public Boolean delete(Supliyer s) {
        return msra.delete(s);
    }

    @Override
    public Supliyer getSupliyerByID(Long ID) {
        return msra.getSupliyerById(ID);
    }

    @Override
    public List<Supliyer> getAllSupliyer() {
        List<Supliyer> l=msra.getSupliyerAll();
        if(l==null) return new ArrayList<Supliyer>();
        else return l;
    }

    @Override
    public Boolean save(User u) {
        return msra.save(u);
    }

    @Override
    public Boolean update(User u) {
        return msra.update(u);
    }

    @Override
    public Boolean delete(User u) {
        return msra.delete(u);
    }

    @Override
    public User getUserByID(Long ID) {
        return msra.getUserById(ID);
    }

    @Override
    public List<User> getAllUser() {
        List<User> l=msra.getUserAll();
        if(l==null) return new ArrayList<User>();
        else return l;
    }

    @Override
    public User getAllUserByID(Long ID) {
        return msra.getUserById(ID);
    }

    @Override
    public List<User> getAllUserByCabang(Cabang c) {
        List<User> l=msra.getAllUserByCabang(c.getIdCabang());
        if(l==null) return new ArrayList<User>();
        else return l;
    }

    @Override
    public User getUserByUserAndPass(String username, String password) {
        return msra.getUserByUNandPass(username, password);
    }

    @Override
    public void saveLogin(LogLogin l) {
        msra.saveLogin(l);
    }

    

}
