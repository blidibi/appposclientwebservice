/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.service.impl;

import com.blung.pos.entity.simple.BarangHilang;
import com.blung.pos.entity.simple.Cabang;
import com.blung.pos.entity.simple.CicilanHutangPembelian;
import com.blung.pos.entity.simple.CicilanPiutangPenjualan;
import com.blung.pos.entity.simple.DetailPembelian;
import com.blung.pos.entity.simple.DetailPenjualan;
import com.blung.pos.entity.simple.DetailReturPembelian;
import com.blung.pos.entity.simple.DetailReturPenjualan;
import com.blung.pos.entity.simple.DetailServis;
import com.blung.pos.entity.simple.DetailTransferGudang;
import com.blung.pos.entity.simple.DetailTransferGudangKeCabang;
import com.blung.pos.entity.simple.HutangPembelian;
import com.blung.pos.entity.simple.PiutangPenjualan;
import com.blung.pos.entity.simple.ReturPembelian;
import com.blung.pos.entity.simple.ReturPenjualan;
import com.blung.pos.entity.simple.Servis;
import com.blung.pos.entity.simple.TransferGudang;
import com.blung.pos.entity.simple.TransferGudangKeCabang;
import com.blung.pos.service.intrf.TransaksiSimpleIntrf;
import com.blung.pos.service.rest.dataacces.TransaksiSimpleRestAccess;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author user
 */
@Service("transaksisimple")
public class TransaksiSimpleImpl implements TransaksiSimpleIntrf{

    TransaksiSimpleRestAccess tsra;
    @Override
    public void setTransaksiRestAccess(RestTemplate restTemplate) {
        this.tsra = new TransaksiSimpleRestAccess(restTemplate);
    }

    @Override
    public Boolean save(HutangPembelian hp) {
        return tsra.save(hp);
    }
    
    @Override
    public HutangPembelian getHutangPembelianByID(Long idHutangPembelian) {
        return tsra.getHutangPembelianByID(idHutangPembelian);
    }

    @Override
    public List<HutangPembelian> getHutangPembelianAll(Date start, Date end) {
        List<HutangPembelian> l=tsra.getHutangPembelianAll();
        if(l==null) return new ArrayList<HutangPembelian>();
        else return l;
    }

    @Override
    public List<HutangPembelian> getHutangPembelianByCabang(Date start, Date end, Long idCabang) {
        List<HutangPembelian> l=tsra.getHutangPembelianByCabang(start, end, idCabang);
        if(l==null) return new ArrayList<HutangPembelian>();
        else return l;
        
    }

    @Override
    public List<HutangPembelian> getHutangPembelianBySupliyer(Date start, Date end, Long idSupliyer) {
        List<HutangPembelian> l=tsra.getHutangPembelianBySupliyer(start, end, idSupliyer);
        if(l==null) return new ArrayList<HutangPembelian>();
        else return l;
    }

    @Override
    public List<HutangPembelian> getHutangPembelianByCabangDanSupliyer(Date start, Date end, Long idCabang, Long idSupliyer) {
        List<HutangPembelian> l=tsra.getHutangPembelianByCabangDanSupliyer(start, end, idCabang, idSupliyer);
        if(l==null) return new ArrayList<HutangPembelian>();
        else return l;
    }
    

    @Override
    public Boolean save(PiutangPenjualan pp) {
        return tsra.save(pp);
    }
    
   
        @Override
    public List<PiutangPenjualan> getPiutangPenjualanAll(Long idPiutangPenjualan) {
        throw new UnsupportedOperationException("Not supported yet.");
    }


    @Override
    public PiutangPenjualan getPiutangPenjualanById(Long idPiutangPenjualan) {
        return tsra.getPiutangPenjualanById(idPiutangPenjualan);
    }

    @Override
    public List<PiutangPenjualan> getPiutangPenjualanByCabang(Date start, Date end, Long idCabang) {
        List<PiutangPenjualan> l=tsra.getPiutangPenjualanByCabang(start, end, idCabang);
        if(l==null) return new ArrayList<PiutangPenjualan>();
        else return l;
    }

    @Override
    public List<PiutangPenjualan> getPiutangPenjualanByPelanggan(Date start, Date end, Long idPelanggan) {
        List<PiutangPenjualan> l=tsra.getPiutangPenjualanByPelanggan(start, end, idPelanggan);
        if(l==null) return new ArrayList<PiutangPenjualan>();
        else return l;
    }

    @Override
    public List<PiutangPenjualan> getPiutangPenjualanByCabangDanPelanggan(Date start, Date end, Long idCabang, Long idPelanggan) {
        List<PiutangPenjualan> l=tsra.getPiutangPenjualanByCabangDanPelanggan(start, end, idCabang, idPelanggan);
        if(l==null) return new ArrayList<PiutangPenjualan>();
        else return l;
    }

    @Override
    public Boolean save(CicilanHutangPembelian chp) {
        return tsra.save(chp);
    }

    @Override
    public List<CicilanHutangPembelian> getCicilanHutangPembelianAll() {
        List<CicilanHutangPembelian> l=tsra.getCicilanHutangPembelianAll();
        if(l==null) return new ArrayList<CicilanHutangPembelian>();
        else return l;
    }

    @Override
    public CicilanHutangPembelian getCicilanHutangPembelianById(Long idHutangPembelian) {
        return tsra.getCicilanHutangPembelianById(idHutangPembelian);
    }

    @Override
    public List<CicilanHutangPembelian> getCicilanHutangPembelianByHutangPembelian(Long idHutangPembelian) {
        List<CicilanHutangPembelian> l=tsra.getCicilanHutangPembelianByHutangPembelian(idHutangPembelian);
        if(l==null) return new ArrayList<CicilanHutangPembelian>();
        else return l;
    }

    @Override
    public Boolean save(CicilanPiutangPenjualan cpp) {
        return tsra.save(cpp);
    }

    @Override
    public List<CicilanPiutangPenjualan> getCicilanPiutangPenjualanByPiutangPenjualan(Long idPiutangPenjualan) {
        List<CicilanPiutangPenjualan> l=tsra.getCicilanPiutangPenjualanByPiutangPenjualan(idPiutangPenjualan);
        if(l==null) return new ArrayList<CicilanPiutangPenjualan>();
        else return l;
    }

    @Override
    public List<CicilanPiutangPenjualan> getCicilanPiutangPenjualanAll(Long idCicilanPiutangPenjualan) {
        List<CicilanPiutangPenjualan> l=tsra.getCicilanPiutangPenjualanAll();
        if(l==null) return new ArrayList<CicilanPiutangPenjualan>();
        else return l;
    }

    @Override
    public CicilanPiutangPenjualan getCicilanPiutangPenjualanById(Long idCicilanPiutangPenjualan) {
        return tsra.getCicilanPiutangPenjualanByID(idCicilanPiutangPenjualan);
    }

    @Override
    public DetailPenjualan getDetailPenjualanByid(Long id) {
        return tsra.getDetailPenjualanID(id);
    }

    @Override
    public List<DetailPenjualan> getDetailPenjualanByPenjualan(Long idPenjualan) {
        List<DetailPenjualan> l=tsra.getDetailPenjualanByPenjualan(idPenjualan);
        if(l==null) return new ArrayList<DetailPenjualan>();
        else return l;
    }

    @Override
    public List<DetailPenjualan> getDetailPenjualanByCabang(Date start, Date end,Long idCabang) {
        List<DetailPenjualan> l=tsra.getDetailPenjualanByCabang(start, end, idCabang);
        if(l==null) return new ArrayList<DetailPenjualan>();
        else return l;
    }

    @Override
    public List<DetailPenjualan> getDetailPenjualanByBarang(Long idBarang) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public DetailPembelian getDetailPembelianById(Long id) {
        return tsra.getDetailPembelianID(id);
    }

    @Override
    public List<DetailPembelian> getDetailPembelianByPembelian(Long idPembelian) {
        List<DetailPembelian> l=tsra.getDetailPembelianByPembelian(idPembelian);
        if(l==null) return new ArrayList<DetailPembelian>();
        else return l;
    }

    @Override
    public List<DetailPembelian> getDetailPembelianByCabang(Date start, Date end,Long idCabang) {
        List<DetailPembelian> l=tsra.getDetailPembelianByCabang(start,end,idCabang);
        if(l==null) return new ArrayList<DetailPembelian>();
        else return l;
    }

    @Override
    public List<DetailPembelian> getDetailPembelianByBarang(Long idBarang) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Boolean save(List<BarangHilang> l) {
        return tsra.save(l);
    }

    @Override
    public List<BarangHilang> getBarangHilangByCabang(Date start, Date end,Cabang c) {
        List<BarangHilang> l=tsra.getBarangHilangByCabang(start,end,c.getIdCabang());
        if(l==null) return new ArrayList<BarangHilang>();
        else return l;
    }

    @Override
    public TransferGudang getTransferGudangByID(Long id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TransferGudang> getTransferGudangByCabangAndDate(Date start, Date end, Cabang c) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<DetailTransferGudang> getDetailTransferGudangByTransferGudang(Long ID) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<DetailTransferGudang> getTransferGudangByCabangAndDate() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public TransferGudangKeCabang getTransferGudangKeCabangByID(Long ID) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<TransferGudangKeCabang> getTransferGudangKeCabangByCabangAndDate() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<DetailTransferGudangKeCabang> getDetailTransferGudangKeCabangByTransferGudangKeCabang(Long id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<DetailTransferGudangKeCabang> getDetailTransferGudangKeCabangByCabangAndDate(Date start, Date end, Cabang c) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ReturPembelian getReturPembelianByID(Long ID) {
        return tsra.getReturPembelianByID(ID);
    }

    @Override
    public List<ReturPembelian> getReturPembelianByCabangAndDate(Date start, Date end, Cabang c) {
        List<ReturPembelian> l=tsra.getReturPembelianByCabang(start, end, c.getIdCabang());
        if(l==null) return new ArrayList<ReturPembelian>();
        else return l;
    }

    @Override
    public List<DetailReturPembelian> getDetailReturPembelianByPembelian(Long id) {
        List<DetailReturPembelian> l=tsra.getDetailReturPembelianByReturPembelian(id);
        if(l==null) return new ArrayList<DetailReturPembelian>();
        else return l;
    }

    @Override
    public List<DetailReturPembelian> getDetailReturPembelianByCabangAndDate(Date start, Date end, Cabang c) {
        List<DetailReturPembelian> l=tsra.getDetailReturPembelianByCabang(start, end, c.getIdCabang());
        if(l==null) return new ArrayList<DetailReturPembelian>();
        else return l;
    }

    @Override
    public ReturPenjualan getReturPenjualanByID(Long ID) {
        return tsra.getReturPenjualanByID(ID);
    }

    @Override
    public List<ReturPenjualan> getReturPenjualanByCabangAndDate(Date start, Date end, Cabang c) {
        List<ReturPenjualan> l=tsra.getReturPenjualanByCabang(start, end, c.getIdCabang());
        if(l==null) return new ArrayList<ReturPenjualan>();
        else return l;
    }

    @Override
    public List<DetailReturPenjualan> getDetailReturPenjualanByPembelian(Long id) {
        List<DetailReturPenjualan> l=tsra.getDetailReturPenjualanByReturPembelian(id);
        if(l==null) return new ArrayList<DetailReturPenjualan>();
        else return l;
    }

    @Override
    public List<DetailReturPenjualan> getDetailReturPenjualanByCabangAndDate(Date start, Date end, Cabang c) {
        List<DetailReturPenjualan> l=tsra.getDetailReturPenjualanByCabang(start, end, c.getIdCabang());
        if(l==null) return new ArrayList<DetailReturPenjualan>();
        else return l;
    }

    @Override
    public List<CicilanHutangPembelian> getCicilanHutangPembelianByCabangAndDate(Date start, Date end, Long idCabang) {
        List<CicilanHutangPembelian> l=tsra.getCicilanHutangPembelianByCabangTgl(start, end, idCabang);
        if(l==null) return new ArrayList<CicilanHutangPembelian>();
        else return l;
    }

    @Override
    public List<CicilanPiutangPenjualan> getCicilanPiutangPenjualanByCabangAndDate(Date start, Date end, Long idCabang) {
        List<CicilanPiutangPenjualan> l=tsra.getCicilanPiutangPenjualanByCabangTgl(start, end, idCabang);
        if(l==null) return new ArrayList<CicilanPiutangPenjualan>();
        else return l;
    }

    @Override
    public Boolean save(Servis s) {
        return tsra.save(s);
    }

    @Override
    public Boolean update(Servis s) {
        return tsra.update(s);
    }

    @Override
    public List<Servis> servisSudahByCabangAndDate(Date start, Date end, Cabang c) {
        List<Servis> l=tsra.getservisSudahByCabangAndDate(start, end, c.getIdCabang());
        if(l==null) return new ArrayList<Servis>();
        else return l;
    }

    @Override
    public List<Servis> servisBelumByCabangAndDate(Date start, Date end, Cabang c) {
        List<Servis> l=tsra.getservisBelumByCabangAndDate(start, end, c.getIdCabang());
        if(l==null) return new ArrayList<Servis>();
        else return l;
    }

    @Override
    public Boolean save(DetailServis s) {
        return tsra.save(s);
    }

    @Override
    public Boolean update(DetailServis s) {
        return tsra.update(s);
    }

    @Override
    public List<DetailServis> detailservisByServis(Long id) {
        List<DetailServis> l=tsra.detailservisByServis(id);
        if(l==null) return new ArrayList<DetailServis>();
        else return l;
    }

    @Override
    public List<DetailServis> detailservisByCabangAndDate(Date start, Date end, Cabang c) {
        List<DetailServis> l=tsra.detailservisByCabangAndDate(start, end, c.getIdCabang());
        if(l==null) return new ArrayList<DetailServis>();
        else return l;
    }

    
}
