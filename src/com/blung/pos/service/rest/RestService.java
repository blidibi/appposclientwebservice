/*
 * created : Jul 5, 2011
 * by : Latief
 */
package com.blung.pos.service.rest;

import com.blung.pos.utility.PropertiesApp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import org.springframework.http.*;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * Class yang mendefinisikan komunikasi dengan server secara umum, atau generic.
 *
 * @author Latief
 */

public class RestService {

    public final static String PATTERN_SERVER_URL = PropertiesApp.PATTERN_SERVER_URL;
    
    protected RestTemplate restTemplate;
    protected HttpHeaders headers;

    public RestService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        initDefaultHeaders();
    }

    
    /* ini method dipakai jika tidak ada auth apapun di rest server
    private void initDefaultHeaders() {
        List<MediaType> mediaTypes = new ArrayList<>();
        mediaTypes.add(MediaType.APPLICATION_JSON);
        headers = new HttpHeaders();
        headers.setAccept(mediaTypes);
        headers.add("Cache-Control", "no-cache");
    }
    */
    // ini method dipakai jika rest server menggunakan basic auth
    private void initDefaultHeaders() {
        List<MediaType> mediaTypes = new ArrayList<>();
        mediaTypes.add(MediaType.APPLICATION_JSON);
        headers = new HttpHeaders();
        String authorisation = PropertiesApp.UsernameAPI+":"+PropertiesApp.PasswordAPI;
        byte[] encodedAuthorisation = Base64.encode(authorisation.getBytes());
        headers.add("Authorization", "Basic " + new String(encodedAuthorisation));
        headers.setAccept(mediaTypes);
    }
     
    /**
     * Cari data di server
     *
     * @param url
     * @param uriVariables
     * @return
     * @throws RestClientException
     */
    protected Object findList(String url, HttpEntity httpEntity, Map<String, Object> uriVariables) throws RestClientException {
        try {
            ResponseEntity<Object> response = restTemplate.exchange(getServerURL() + url, HttpMethod.GET, httpEntity, Object.class, uriVariables);
            return response.getBody();
        } catch (RestClientException rce) {
            JOptionPane.showMessageDialog(null, rce, "Data tidak ditemukan", JOptionPane.ERROR_MESSAGE);
            rce.printStackTrace();
            return null;
        }
    }
    
    protected Object find(String url, HttpEntity httpEntity, Map<String, Object> uriVariables) throws RestClientException {
        try {
            ResponseEntity<Object> response = restTemplate.exchange(getServerURL() + url, HttpMethod.GET, httpEntity, Object.class, uriVariables);
            return response.getBody();
        } catch (RestClientException rce) {
            JOptionPane.showMessageDialog(null, rce, "Data tidak ditemukan", JOptionPane.ERROR_MESSAGE);
            rce.printStackTrace();
            return null;
        }
    }

    /**
     * Cari data di server
     *
     * @param url
     * @param uriVariables
     * @return
     * @throws RestClientException
     */
    protected Object find(String url, HttpEntity httpEntity) throws RestClientException {
        try {
            ResponseEntity<Object> response = restTemplate.exchange(getServerURL() + url, HttpMethod.GET, httpEntity, Object.class);
            return response.getBody();
        } catch (RestClientException rce) {
            JOptionPane.showMessageDialog(null, rce, "Data tidak ditemukan", JOptionPane.ERROR_MESSAGE);
            rce.printStackTrace();
            return null;
        }
    }
    
    protected Object findList(String url, HttpEntity httpEntity) throws RestClientException {
        try {
            ResponseEntity<Object> response = restTemplate.exchange(getServerURL() + url, HttpMethod.GET, httpEntity, Object.class);
            return response.getBody();
        } catch (RestClientException rce) {
            JOptionPane.showMessageDialog(null, rce, "Data tidak ditemukan", JOptionPane.ERROR_MESSAGE);
            System.out.println(rce);
            return null;
        }
    }

    /**
     * Cari data di server dengan method POST
     *
     * @param url
     * @param uriVariables
     * @return
     * @throws RestClientException
     */
    protected Object findPost(String url, HttpEntity httpEntity, Map<String, Object> uriVariables) throws RestClientException {
        try {
            ResponseEntity<Object> response = restTemplate.exchange(getServerURL() + url, HttpMethod.POST, httpEntity, Object.class, uriVariables);
            return response.getBody();
        } catch (RestClientException rce) {
            
            JOptionPane.showMessageDialog(null, rce, "Data tidak ditemukan", JOptionPane.ERROR_MESSAGE);
            rce.printStackTrace();
            //return new LinkedHashMap<>();
            return null;
        }
    }

    /**
     * Cari data di server dengan method POST
     *
     * @param url
     * @param uriVariables
     * @return
     * @throws RestClientException
     */
    protected Object findPost(String url, HttpEntity httpEntity) throws RestClientException {
        try {
            ResponseEntity<Object> response = restTemplate.exchange(getServerURL() + url, HttpMethod.POST, httpEntity, Object.class);
            return response.getBody();
        } catch (RestClientException rce) {
            //rce.printStackTrace();
            JOptionPane.showMessageDialog(null, rce, "Data tidak ditemukan", JOptionPane.ERROR_MESSAGE);
            rce.printStackTrace();
            //return new LinkedHashMap<>();
            return null;
        }
    }

    /**
     * Simpan data ke server.
     *
     * @param url
     * @param httpEntity
     * @param uriVariables
     * @return
     * @throws RestClientException
     */
    protected Map<String, Object> save(String url, HttpEntity httpEntity, Map<String, Object> uriVariables) throws RestClientException {
        try {
            ResponseEntity<Map> response = restTemplate.exchange(getServerURL() + url, HttpMethod.POST, httpEntity, Map.class, uriVariables);
            return response.getBody();
        } catch (RestClientException rce) {
            //rce.printStackTrace();
            JOptionPane.showMessageDialog(null, rce, "ERROR!!!", JOptionPane.ERROR_MESSAGE);
            rce.printStackTrace();
            return new HashMap<>();
        }
    }

    /**
     * Simpan data ke server.
     *
     * @param url
     * @param httpEntity
     * @return
     * @throws RestClientException
     */
    protected Map<String, Object> save(String url, HttpEntity httpEntity) throws RestClientException {
        ResponseEntity<Map> response = null;
        try {
            response = restTemplate.exchange(getServerURL() + url, HttpMethod.POST, httpEntity, Map.class);
            return response.getBody();
        } catch (RestClientException rce) {
//            System.out.println(response.toString());
            rce.printStackTrace();
            JOptionPane.showMessageDialog(null, rce, "ERROR!!!", JOptionPane.ERROR_MESSAGE);
            return new HashMap<>();
        }
    }

    /**
     *
     * Rubah data server.
     *
     * @param url
     * @param httpEntity
     * @param uriVariables
     * @return
     * @throws RestClientException
     */
    protected Map<String, Object> update(String url, HttpEntity httpEntity, Map<String, Object> uriVariables) throws RestClientException {
        try {
            ResponseEntity<Map> response = restTemplate.exchange(getServerURL() + url, HttpMethod.POST, httpEntity, Map.class, uriVariables);
            return response.getBody();
        } catch (RestClientException rce) {
          //  rce.printStackTrace();
            JOptionPane.showMessageDialog(null, rce, "ERROR!!!", JOptionPane.ERROR_MESSAGE);
            return new HashMap<>();
        }
    }

    /**
     *
     * Rubah data server.
     *
     * @param url
     * @param httpEntity
     * @return
     * @throws RestClientException
     */
    protected Map<String, Object> update(String url, HttpEntity httpEntity) throws RestClientException {
        try {
            ResponseEntity<Map> response = restTemplate.exchange(getServerURL() + url, HttpMethod.POST, httpEntity, Map.class);
            return response.getBody();
        } catch (RestClientException rce) {
            //rce.printStackTrace();
            JOptionPane.showMessageDialog(null, rce, "ERROR!!!", JOptionPane.ERROR_MESSAGE);
            return new HashMap<>();
        }
    }

    /**
     * Hapus data server
     *
     * @param url
     * @param uriVariables
     * @return
     * @throws RestClientException
     */
    protected Map<String, Object> delete(String url, HttpEntity httpEntity, Map<String, Object> uriVariables) throws RestClientException {
        try {
            ResponseEntity<Map> response = restTemplate.exchange(getServerURL() + url, HttpMethod.POST, httpEntity, Map.class, uriVariables);
            return response.getBody();
        } catch (RestClientException rce) {
            rce.printStackTrace();
            JOptionPane.showMessageDialog(null, rce, "ERROR!!!", JOptionPane.ERROR_MESSAGE);
            return new HashMap<>();
        }
    }

    /**
     * Hapus data server
     *
     * @param url
     * @return
     * @throws RestClientException
     */
    protected Map<String, Object> delete(String url, HttpEntity httpEntity) throws RestClientException {
        try {
            ResponseEntity<Map> response = restTemplate.exchange(getServerURL() + url, HttpMethod.POST, httpEntity, Map.class);
            return response.getBody();
        } catch (RestClientException rce) {
            rce.printStackTrace();
            JOptionPane.showMessageDialog(null, rce, "ERROR!!!", JOptionPane.ERROR_MESSAGE);
            return new HashMap<>();
        }
    }

    /**
     * Hapus banyak data server
     *
     * @param url
     * @return
     * @throws RestClientException
     */
    protected Map<String, Object> deleteMore(String url, HttpEntity httpEntity) throws RestClientException {
        try {
            ResponseEntity<Map> response = restTemplate.exchange(getServerURL() + url + "/delete", HttpMethod.POST, httpEntity, Map.class);
            return response.getBody();
        } catch (RestClientException rce) {
            rce.printStackTrace();
            JOptionPane.showMessageDialog(null, rce, "ERROR!!!", JOptionPane.ERROR_MESSAGE);
            return new HashMap<>();
        }
    }

    /**
     * Dapatkan Jumlah record data.
     *
     * @param url
     * @param uriVariables
     * @return
     * @throws RestClientException
     */
    protected Long count(String url, HttpEntity httpEntity, Map<String, Object> uriVariables) throws RestClientException {
        try {
            ResponseEntity<Long> response = restTemplate.exchange(getServerURL() + url, HttpMethod.GET, httpEntity, Long.class, uriVariables);
            return response.getBody();
        } catch (RestClientException rce) {
            //rce.printStackTrace();
            JOptionPane.showMessageDialog(null, rce, "ERROR!!!", JOptionPane.ERROR_MESSAGE);
            rce.printStackTrace();
            return 0l;
        }
    }

    /**
     * Dapatkan Jumlah record data.
     *
     * @param url
     * @param uriVariables
     * @return
     * @throws RestClientException
     */
    protected Long count(String url, HttpEntity httpEntity) throws RestClientException {
        try {
            ResponseEntity<Long> response = restTemplate.exchange(getServerURL() + url, HttpMethod.GET, httpEntity, Long.class);
            return response.getBody();
        } catch (RestClientException rce) {
            //System.out.println(rce);
            JOptionPane.showMessageDialog(null, rce, "ERROR!!!", JOptionPane.ERROR_MESSAGE);
            rce.printStackTrace();
            return null;
        }
    }

    public HttpHeaders getHeaders() {
        return headers;
    }

    public void setHeaders(HttpHeaders headers) {
        this.headers = headers;
    }

    protected String getServerURL() {
        String url = PATTERN_SERVER_URL;
        // url = url.replaceAll("port", fileSystemController.readServer().getProperty(FileSystemController.KEY_PORT));
        return url;
    }
}
