/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.service.rest.dataacces;

import com.blung.pos.entity.simple.Barang;
import com.blung.pos.entity.simple.BarangHilang;
import com.blung.pos.entity.simple.CicilanHutangPembelian;
import com.blung.pos.entity.simple.CicilanPiutangPenjualan;
import com.blung.pos.entity.simple.DetailPembelian;
import com.blung.pos.entity.simple.DetailPenjualan;
import com.blung.pos.entity.simple.DetailReturPembelian;
import com.blung.pos.entity.simple.DetailReturPenjualan;
import com.blung.pos.entity.simple.DetailServis;
import com.blung.pos.entity.simple.HutangPembelian;
import com.blung.pos.entity.simple.PiutangPenjualan;
import com.blung.pos.entity.simple.ReturPembelian;
import com.blung.pos.entity.simple.ReturPenjualan;
import com.blung.pos.entity.simple.Servis;
import com.blung.pos.service.rest.RestService;
import com.blung.pos.utility.ConverterMapToObject;
import com.blung.pos.utility.ServiceUtility;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author user
 */
public class TransaksiSimpleRestAccess extends RestService{

    public TransaksiSimpleRestAccess(RestTemplate restTemplate) {
        super(restTemplate);
    }
    
    public boolean save(HutangPembelian hp) throws RestClientException {
        HttpEntity<HutangPembelian> httpEntity = new HttpEntity<>(hp, headers);
        Map<String, Object> response = save(ServiceUtility.saveHutangPembelian, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public List<HutangPembelian> getHutangPembelianAll() throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        Object o = find(ServiceUtility.getHutangPembelianAll, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToHutangPembelianList(linkedHashMaps);
    }
        
    public HutangPembelian getHutangPembelianByID(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getHutangPembelianByID, new HttpEntity<HutangPembelian>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToHutangPembelian(linkedHashMaps.get(0));
    }
    
    public List<HutangPembelian> getHutangPembelianByCabang(Date start, Date end, Long idCabang) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        uriVariables.put("id", idCabang.toString());
        Object o = find(ServiceUtility.getHutangpembelianByCabangAndDate, new HttpEntity<HutangPembelian>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToHutangPembelianList(linkedHashMaps);
    }
    
    public List<HutangPembelian> getHutangPembelianBySupliyer(Date start, Date end, Long idSupliyer) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        uriVariables.put("idSupliyer", idSupliyer.toString());
        Object o = find(ServiceUtility.getHutangPembelianBySupliyer, new HttpEntity<HutangPembelian>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToHutangPembelianList(linkedHashMaps);
    }
    
    public List<HutangPembelian> getHutangPembelianByCabangDanSupliyer(Date start, Date end, Long idCabang, Long idSupliyer) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        uriVariables.put("idCabang", idCabang.toString());
        uriVariables.put("idSupliyer", idSupliyer.toString());
        Object o = find(ServiceUtility.getHutangPembelianByCabangDanBySupliyer, new HttpEntity<HutangPembelian>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToHutangPembelianList(linkedHashMaps);
    }
    
    public boolean save(PiutangPenjualan hp) throws RestClientException {
        HttpEntity<PiutangPenjualan> httpEntity = new HttpEntity<>(hp, headers);
        Map<String, Object> response = save(ServiceUtility.savePiutangPenjualan, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public List<PiutangPenjualan> getPiutangPenjualanAll() throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        Object o = find(ServiceUtility.getPiutangPenjualanAll, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToPiutangPenjualanList(linkedHashMaps);
    }
    
    public PiutangPenjualan getPiutangPenjualan(Date start, Date end) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        Object o = find(ServiceUtility.getPiutangPenjualanAll, new HttpEntity<PiutangPenjualan>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToPiutangPenjualan(linkedHashMaps.get(0));
    }
    
    public PiutangPenjualan getPiutangPenjualanById(Long idPiutangPenjualan) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("idPiutangPenjualan", idPiutangPenjualan.toString());
        Object o = find(ServiceUtility.getPiutangPenjualanById, new HttpEntity<PiutangPenjualan>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToPiutangPenjualan(linkedHashMaps.get(0));
    }
    
    public List<PiutangPenjualan> getPiutangPenjualanByCabang(Date start, Date end, Long idCabang) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        uriVariables.put("idCabang", idCabang.toString());
        Object o = find(ServiceUtility.getPiutangPenjualanByCabang, new HttpEntity<PiutangPenjualan>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToPiutangPenjualanList(linkedHashMaps);
    }
    
    public List<PiutangPenjualan> getPiutangPenjualanByPelanggan(Date start, Date end, Long idPelanggan) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        uriVariables.put("idPelanggan", idPelanggan.toString());
        Object o = find(ServiceUtility.getPiutangPenjualanByPelanggan, new HttpEntity<PiutangPenjualan>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToPiutangPenjualanList(linkedHashMaps);
    }
    
    public List<PiutangPenjualan> getPiutangPenjualanByCabangDanPelanggan(Date start, Date end, Long idCabang, Long idPelanggan) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        uriVariables.put("idCabang", idCabang.toString());
        uriVariables.put("idPelanggan", idPelanggan.toString());
        Object o = find(ServiceUtility.getPiutangPenjualanByCabangDanPelanggan, new HttpEntity<PiutangPenjualan>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToPiutangPenjualanList(linkedHashMaps);
    }
    
    public boolean save(CicilanHutangPembelian chp) throws RestClientException {
        HttpEntity<CicilanHutangPembelian> httpEntity = new HttpEntity<>(chp, headers);
        Map<String, Object> response = save(ServiceUtility.saveCicilanHutangPembelian, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public List<CicilanHutangPembelian> getCicilanHutangPembelianByHutangPembelian(Long idHutangPembelian) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("idHutangPembelian", idHutangPembelian.toString());
        Object o = find(ServiceUtility.getCicilanHutangPembelianByHutangPembelian,new HttpEntity<CicilanHutangPembelian>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToCicilanHutangPembelianList(linkedHashMaps);
    }
    
    public CicilanHutangPembelian getCicilanHutangPembelianById(Long idCicilanHutangPembelian) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("idCicilanHutangPembelian", idCicilanHutangPembelian.toString());
        Object o = find(ServiceUtility.getCicilanHutangPembelianById, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToCicilanHutangPembelian(linkedHashMaps.get(0));
    }
    
    public List<CicilanHutangPembelian> getCicilanHutangPembelianAll() throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        Object o = find(ServiceUtility.getCicilanHutangPembelianAll, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToCicilanHutangPembelianList(linkedHashMaps);
    }
    
    public List<CicilanHutangPembelian> getCicilanHutangPembelianByCabangTgl(Date start, Date end,Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        Object o = find(ServiceUtility.getcicilanHutangPembelianByCabangAndDate, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToCicilanHutangPembelianList(linkedHashMaps);
    }
    
    public boolean save(CicilanPiutangPenjualan cpp) throws RestClientException {
        HttpEntity<CicilanPiutangPenjualan> httpEntity = new HttpEntity<>(cpp, headers);
        Map<String, Object> response = save(ServiceUtility.saveCicilanPiutang, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public List<CicilanPiutangPenjualan> getCicilanPiutangPenjualanAll() throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        Object o = find(ServiceUtility.getCicilanPiutangPenjualanAll, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToCicilanPiutangList(linkedHashMaps);
    }
    
    public List<CicilanPiutangPenjualan> getCicilanPiutangPenjualanByCabangTgl(Date start, Date end,Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        Object o = find(ServiceUtility.getcicilanPiutangPenjualanByCabangAndDate, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToCicilanPiutangList(linkedHashMaps);
    }
    
    public CicilanPiutangPenjualan getCicilanPiutangPenjualanByID(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getCicilanPiutangPenjualanByID, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToCicilanPiutangPenjualan(linkedHashMaps.get(0));
    }
    
    public List<CicilanPiutangPenjualan> getCicilanPiutangPenjualanByPiutangPenjualan(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getCicilanPiutangPenjualanByPiutangPenjualan, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToCicilanPiutangList(linkedHashMaps);
    }
    
    public DetailPembelian getDetailPembelianID(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getdetailPembelianById, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToDetailPembelian(linkedHashMaps.get(0));
    }
    
    public List<DetailPembelian> getDetailPembelianByPembelian(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getdetailPembelianByPembelian, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToDetailPembelianList(linkedHashMaps);
    }
    
    public List<DetailPembelian> getDetailPembelianByCabang(Date start, Date end,Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        Object o = find(ServiceUtility.getdetailPembelianByCabang, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToDetailPembelianList(linkedHashMaps);
    }
    
    public DetailPenjualan getDetailPenjualanID(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getdetailPenjualanById, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToDetailPenjualan(linkedHashMaps.get(0));
    }
    
    public List<DetailPenjualan> getDetailPenjualanByPenjualan(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getdetailPenjualanByPenjualan, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToDetailPenjualanList(linkedHashMaps);
    }
    
    public List<DetailPenjualan> getDetailPenjualanByCabang(Date start, Date end,Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        Object o = find(ServiceUtility.getdetailPenjualanByCabang, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToDetailPenjualanList(linkedHashMaps);
    }
    
     
    public boolean save(List<BarangHilang> bh) throws RestClientException {
        HttpEntity<List<BarangHilang>> httpEntity = new HttpEntity<>(bh, headers);
        Map<String, Object> response = save(ServiceUtility.simpanBarangHilang, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public List<BarangHilang> getBarangHilangByCabang(Date start, Date end,Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        Object o = find(ServiceUtility.getBarangHilangByCabangAndDate, new HttpEntity<HutangPembelian>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToBarangHilangList(linkedHashMaps);
    }
    
    public ReturPembelian getReturPembelianByID(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getreturPembelianById, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToReturPembelian(linkedHashMaps.get(0));
    }
    
    public List<ReturPembelian> getReturPembelianByCabang(Date start, Date end,Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        Object o = find(ServiceUtility.getreturPembelianByCabangAndDate, new HttpEntity<HutangPembelian>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToReturPembeliansList(linkedHashMaps);
    }
    
    public List<DetailReturPembelian> getDetailReturPembelianByReturPembelian(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getdetailReturPembelianByReturPembelian, new HttpEntity<HutangPembelian>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToDetailReturPembelianList(linkedHashMaps);
    }
    
    public List<DetailReturPembelian> getDetailReturPembelianByCabang(Date start, Date end,Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        Object o = find(ServiceUtility.getdetailReturPembelianByCabangAndDate, new HttpEntity<HutangPembelian>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToDetailReturPembelianList(linkedHashMaps);
    }
    
    public ReturPenjualan getReturPenjualanByID(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getreturPenjualanById, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToReturPenjualan(linkedHashMaps.get(0));
    }
    
    public List<ReturPenjualan> getReturPenjualanByCabang(Date start, Date end,Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        Object o = find(ServiceUtility.getreturPenjualanByCabangAndDate, new HttpEntity<HutangPembelian>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToReturPenjualanList(linkedHashMaps);
    }
    
    public List<DetailReturPenjualan> getDetailReturPenjualanByReturPembelian(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getdetailReturPenjualanByReturPenjualan, new HttpEntity<HutangPembelian>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToDetailReturPenjualanList(linkedHashMaps);
    }
    
    public List<DetailReturPenjualan> getDetailReturPenjualanByCabang(Date start, Date end,Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        Object o = find(ServiceUtility.getdetailReturPenjualanByCabangAndDate, new HttpEntity<HutangPembelian>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToDetailReturPenjualanList(linkedHashMaps);
    }
    
    public boolean save(Servis hp) throws RestClientException {
        HttpEntity<Servis> httpEntity = new HttpEntity<>(hp, headers);
        Map<String, Object> response = save(ServiceUtility.saveServis, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean update(Servis hp) throws RestClientException {
        HttpEntity<Servis> httpEntity = new HttpEntity<>(hp, headers);
        Map<String, Object> response = save(ServiceUtility.updateServis, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public List<Servis> getservisSudahByCabangAndDate(Date start, Date end,Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        Object o = find(ServiceUtility.servisSudahByCabangAndDate, new HttpEntity<Servis>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToServisList(linkedHashMaps);
    }
    
    public List<Servis> getservisBelumByCabangAndDate(Date start, Date end,Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        Object o = find(ServiceUtility.servisBelumByCabangAndDate, new HttpEntity<HutangPembelian>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToServisList(linkedHashMaps);
    }
    
    public boolean save(DetailServis hp) throws RestClientException {
        HttpEntity<DetailServis> httpEntity = new HttpEntity<>(hp, headers);
        Map<String, Object> response = save(ServiceUtility.savedetailservis, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean update(DetailServis hp) throws RestClientException {
        HttpEntity<DetailServis> httpEntity = new HttpEntity<>(hp, headers);
        Map<String, Object> response = save(ServiceUtility.updatedetailservis, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public List<DetailServis> detailservisByServis(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.detailservisByServis, new HttpEntity<HutangPembelian>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToDetailServisList(linkedHashMaps);
    }
    
    public List<DetailServis> detailservisByCabangAndDate(Date start, Date end,Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        Object o = find(ServiceUtility.detailservisByCabangAndDate, new HttpEntity<HutangPembelian>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToDetailServisList(linkedHashMaps);
    }
}
