/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.service.rest.dataacces;

import com.blung.pos.entity.orm.Pembelian;
import com.blung.pos.entity.orm.Penjualan;
import com.blung.pos.entity.orm.ReturPembelian;
import com.blung.pos.entity.orm.ReturPenjualan;
import com.blung.pos.entity.orm.Servis;
import com.blung.pos.entity.orm.TransferGudang;
import com.blung.pos.entity.orm.TransferGudangKeCabang;
import com.blung.pos.entity.simple.PiutangPenjualan;
import com.blung.pos.service.rest.RestService;
import com.blung.pos.utility.ConverterMapToObject;
import com.blung.pos.utility.ServiceUtility;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author lukman
 */
public class TransaksiORMpostRestAcces extends RestService{

    public TransaksiORMpostRestAcces(RestTemplate restTemplate) {
        super(restTemplate);
    }
    
    public boolean save(Penjualan penjualan) throws RestClientException {
        HttpEntity<Penjualan> httpEntity = new HttpEntity<>(penjualan, headers);
        Map<String, Object> response = save(ServiceUtility.savePenjualan, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean save(Pembelian pembelian) throws RestClientException {
        HttpEntity<Pembelian> httpEntity = new HttpEntity<>(pembelian, headers);
        Map<String, Object> response = save(ServiceUtility.savePembelian, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean update(com.blung.pos.entity.simple.Pembelian c) throws RestClientException {
        HttpEntity<com.blung.pos.entity.simple.Pembelian> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = update(ServiceUtility.updatePembelian, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean save(TransferGudang transferGudang) throws RestClientException {
        HttpEntity<TransferGudang> httpEntity = new HttpEntity<>(transferGudang, headers);
        Map<String, Object> response = save(ServiceUtility.saveTransferGudang, httpEntity);
        System.out.println(response);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean save(TransferGudangKeCabang transferGudangKeCabang) throws RestClientException {
        HttpEntity<TransferGudangKeCabang> httpEntity = new HttpEntity<>(transferGudangKeCabang, headers);
        Map<String, Object> response = save(ServiceUtility.saveTransfergudangkecabang, httpEntity);
        System.out.println(response);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public com.blung.pos.entity.simple.Pembelian getPembelianById(Long idPembelian) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", idPembelian.toString());
        Object o = find(ServiceUtility.getPembelianById, new HttpEntity<PiutangPenjualan>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToPembelian(linkedHashMaps.get(0));
    }
    
    public List<com.blung.pos.entity.simple.Pembelian> getPembelianAll(Date start,Date end) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        Object o = find(ServiceUtility.getPembelianAll, new HttpEntity<PiutangPenjualan>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToPembelianList(linkedHashMaps);
    }
    
    public List<com.blung.pos.entity.simple.Pembelian> getPembelianBySupliyer(Long idSupliyer,Date start,Date end) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", idSupliyer.toString());
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        Object o = find(ServiceUtility.getPembelianBySupliyer, new HttpEntity<PiutangPenjualan>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToPembelianList(linkedHashMaps);
    }
    
    public List<com.blung.pos.entity.simple.Pembelian> getPembelianByCabang(Long idCabang,Date start,Date end) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", idCabang.toString());
        uriVariables.put("datestart", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("dateend", new SimpleDateFormat("yyyy-MM-dd").format(end));
        Object o = find(ServiceUtility.getPembelianByCabang, new HttpEntity<PiutangPenjualan>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToPembelianList(linkedHashMaps);
    }
    
    public List<com.blung.pos.entity.simple.Pembelian> getPembelianBelumPindahGudangByCabang(Long idCabang) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", idCabang.toString());
        Object o = find(ServiceUtility.getpembelianBelumPindahGudangByCabang, new HttpEntity<PiutangPenjualan>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToPembelianList(linkedHashMaps);
    }
    
    public List<com.blung.pos.entity.simple.Pembelian> getPembelianBelumRekapByCabangAndUser(Long idC,Long idU) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("idc", idC.toString());
        uriVariables.put("idu", idU.toString());
        Object o = find(ServiceUtility.getpembelianBelumRekapByCabangAndUser, new HttpEntity<PiutangPenjualan>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToPembelianList(linkedHashMaps);
    }
    
    public List<com.blung.pos.entity.simple.Pembelian> getPembelianBySupliyerAndCabang(Long idCabang,Long idSupliyer,Date start,Date end) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("idC", idCabang.toString());
        uriVariables.put("idS", idSupliyer.toString());
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        Object o = find(ServiceUtility.getpembelianBySupliyerAndCabang, new HttpEntity<PiutangPenjualan>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToPembelianList(linkedHashMaps);
    }
    
    public List<com.blung.pos.entity.simple.Penjualan> getPenjualanAll(Date start,Date end){
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        Object o = find(ServiceUtility.getPenjualanAll, new HttpEntity<PiutangPenjualan>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToPenjualanList(linkedHashMaps);
    }
    
    public com.blung.pos.entity.simple.Penjualan getPenjualanByID(Long id){
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getPenjualanByID, new HttpEntity<PiutangPenjualan>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToPenjualan(linkedHashMaps.get(0));
    }
    
    public com.blung.pos.entity.simple.Penjualan getLastPenjualanByCbAndKs(Long idc,Long idk){
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("idc", idc.toString());
        uriVariables.put("idk", idk.toString());
        Object o = find(ServiceUtility.getLastPenjualanByCabangDanKasir, new HttpEntity<PiutangPenjualan>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToPenjualan(linkedHashMaps.get(0));
    }
    
    public List<com.blung.pos.entity.simple.Penjualan> getPenjualanByCabang(Long id,Date start,Date end){
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        uriVariables.put("start", new SimpleDateFormat("yyyy-MM-dd").format(start));
        uriVariables.put("end", new SimpleDateFormat("yyyy-MM-dd").format(end));
        Object o = find(ServiceUtility.getPenjualanByCabang, new HttpEntity<PiutangPenjualan>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToPenjualanList(linkedHashMaps);
    }
    
    public List<com.blung.pos.entity.simple.Penjualan> getPenjualanBelumRekapByCabangAndUser(Long idC,Long idU) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("idc", idC.toString());
        uriVariables.put("idu", idU.toString());
        Object o = find(ServiceUtility.getPenjualanBelumRekapByCabangAndUser, new HttpEntity<PiutangPenjualan>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToPenjualanList(linkedHashMaps);
    }
    
    public boolean save(Servis hp) throws RestClientException {
        HttpEntity<Servis> httpEntity = new HttpEntity<>(hp, headers);
        Map<String, Object> response = save(ServiceUtility.saveServisORM, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean save(ReturPembelian rp) throws RestClientException {
        HttpEntity<ReturPembelian> httpEntity = new HttpEntity<>(rp, headers);
        Map<String, Object> response = save(ServiceUtility.saveReturpembelian, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean save(ReturPenjualan rp) throws RestClientException {
        HttpEntity<ReturPenjualan> httpEntity = new HttpEntity<>(rp, headers);
        Map<String, Object> response = save(ServiceUtility.saveReturpenjualan, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
}
