/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blung.pos.service.rest.dataacces;

import com.blung.pos.entity.bantuan.JmlBarang;
import com.blung.pos.entity.simple.Barang;
import com.blung.pos.entity.simple.Cabang;
import com.blung.pos.entity.simple.Gudang;
import com.blung.pos.entity.simple.Karyawan;
import com.blung.pos.entity.simple.Kasir;
import com.blung.pos.entity.simple.Kategori;
import com.blung.pos.entity.simple.KategoriPelanggan;
import com.blung.pos.entity.simple.LogLogin;
import com.blung.pos.entity.simple.Pelanggan;
import com.blung.pos.entity.simple.Satuan;
import com.blung.pos.entity.simple.SettingAplikasi;
import com.blung.pos.entity.simple.Supliyer;
import com.blung.pos.entity.simple.User;
import com.blung.pos.service.rest.RestService;
import com.blung.pos.utility.ConverterMapToObject;
import com.blung.pos.utility.ServiceUtility;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author lukman
 */
public class MasterSimpleRestAcces extends RestService{

    public MasterSimpleRestAcces(RestTemplate restTemplate) {
        super(restTemplate);
    }
    
    public boolean save(Barang barang) throws RestClientException {
        HttpEntity<Barang> httpEntity = new HttpEntity<>(barang, headers);
        Map<String, Object> response = save(ServiceUtility.saveBarang, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean update(Barang c) throws RestClientException {
        HttpEntity<Barang> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = update(ServiceUtility.updateBarang, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean delete(Barang c) throws RestClientException {
        HttpEntity<Barang> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = delete(ServiceUtility.deleteBarang, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public Barang getBarangById(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getBarangByID, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToBarang(linkedHashMaps.get(0));
    }
    
    public Barang getBarangById(String id,Long idc) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("idbc", id);
        uriVariables.put("idcb", idc.toString());
        Object o = find(ServiceUtility.getBarangByIDBarcodeAndCabang, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToBarang(linkedHashMaps.get(0));
    }
    
    public Barang getBarangById(Long id,Long idc) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("idb", id.toString());
        uriVariables.put("idc", idc.toString());
        Object o = find(ServiceUtility.jumlahBarangByIDandCabang, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToBarang(linkedHashMaps.get(0));
    }
    
    public List<Barang> getBarangAll() throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        Object o = find(ServiceUtility.getBarangAll, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToBarangList(linkedHashMaps);
    }
    
    public List<Barang> getBarangAllByCabang(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getBarangAllByCabang, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToBarangList(linkedHashMaps);
    }
    
    public List<JmlBarang> getJmlBarangAllByCabang(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("idc", id.toString());
        Object o = find(ServiceUtility.alljumlahBarangByCabang, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapTojmlbarangBantuList(linkedHashMaps);
    }
    
    public JmlBarang getJmlBarangByCabangAndID(Long idc,Long idb) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("idc", idc.toString());
        uriVariables.put("idb", idb.toString());
        Object o = find(ServiceUtility.jumlahBarangByIDandCabang, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToJmlBarangBantu(linkedHashMaps.get(0));
    }
    
    public boolean save(Cabang c) throws RestClientException {
        HttpEntity<Cabang> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = save(ServiceUtility.saveCabang, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean update(Cabang c) throws RestClientException {
        HttpEntity<Cabang> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = update(ServiceUtility.updateCabang, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean delete(Cabang c) throws RestClientException {
        HttpEntity<Cabang> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = delete(ServiceUtility.deleteCabang, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public List<Cabang> getCabangAll() throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        Object o = find(ServiceUtility.getCabangAll, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToCabangList(linkedHashMaps);
    }
    
    public Cabang getCabangById(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getCabangByID, new HttpEntity<Cabang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToCabang(linkedHashMaps.get(0));
    }
    
    public boolean save(Gudang c) throws RestClientException {
        HttpEntity<Gudang> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = save(ServiceUtility.saveGudang, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
        
    public boolean update(Gudang c) throws RestClientException {
        HttpEntity<Gudang> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = update(ServiceUtility.updateGudang, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean delete(Gudang c) throws RestClientException {
        HttpEntity<Gudang> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = delete(ServiceUtility.deleteGudang, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public List<Gudang> getGudangAll() throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        Object o = find(ServiceUtility.getGudangAll, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToGudangList(linkedHashMaps);
    }
    
    public Gudang getGudangById(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getGudangByID, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToGudang(linkedHashMaps.get(0));
    }
    
    public List<Gudang> getGudangByCabang(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getGudangByCabang, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToGudangList(linkedHashMaps);
    }
    
    public boolean save(Karyawan c) throws RestClientException {
        HttpEntity<Karyawan> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = save(ServiceUtility.saveKaryawan, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean update(Karyawan c) throws RestClientException {
        HttpEntity<Karyawan> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = update(ServiceUtility.updateKaryawan, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean delete(Karyawan c) throws RestClientException {
        HttpEntity<Karyawan> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = delete(ServiceUtility.deleteKaryawan, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public Karyawan getKaryawanById(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getKaryawanByID, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToKaryawan(linkedHashMaps.get(0));
    }
    
    public List<Karyawan> getKaryawanByCabang(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getKaryawanAllByCabang, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToKaryawanList(linkedHashMaps);
    }
    
    public List<Karyawan> getKaryawanAll() throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        Object o = find(ServiceUtility.getKaryawanAll, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToKaryawanList(linkedHashMaps);
    }
    
    public boolean save(Kasir c) throws RestClientException {
        HttpEntity<Kasir> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = save(ServiceUtility.saveKasir, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean update(Kasir c) throws RestClientException {
        HttpEntity<Kasir> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = update(ServiceUtility.updateKasir, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean delete(Kasir c) throws RestClientException {
        HttpEntity<Kasir> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = delete(ServiceUtility.deleteKasir, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public Kasir getKasirById(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getKasirByID, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToKasir(linkedHashMaps.get(0));
    }
    
    public List<Kasir> getKasirAll() throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        Object o = find(ServiceUtility.getKasirAll, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToKasirList(linkedHashMaps);
    }
    
    public List<Kasir> getKasirByCabang(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getKasirByCabang, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToKasirList(linkedHashMaps);
    }
    
    public boolean save(Kategori c) throws RestClientException {
        HttpEntity<Kategori> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = save(ServiceUtility.saveKategori, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean update(Kategori c) throws RestClientException {
        HttpEntity<Kategori> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = update(ServiceUtility.updateKategori, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean delete(Kategori c) throws RestClientException {
        HttpEntity<Kategori> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = delete(ServiceUtility.deleteKategori, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public Kategori getKategoriById(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getKategoriByID, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToKategori(linkedHashMaps.get(0));
    }
    
    public List<Kategori> getKategoriAll() throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        Object o = find(ServiceUtility.getKategoriAll, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToKategoriList(linkedHashMaps);
    }
    
    public boolean save(KategoriPelanggan c) throws RestClientException {
        HttpEntity<KategoriPelanggan> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = save(ServiceUtility.saveKategoriPelanggan, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean update(KategoriPelanggan c) throws RestClientException {
        HttpEntity<KategoriPelanggan> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = update(ServiceUtility.updateKategoriPelanggan, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean delete(KategoriPelanggan c) throws RestClientException {
        HttpEntity<KategoriPelanggan> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = delete(ServiceUtility.deleteKategoriPelanggan, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public KategoriPelanggan getKategoriPelangganById(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getKategoriPelangganByID, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToKategoriPelanggan(linkedHashMaps.get(0));
    }
    
    public List<KategoriPelanggan> getKategoriPelangganAll() throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        Object o = find(ServiceUtility.getKategoriPelangganAll, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToKategoriPelangganList(linkedHashMaps);
    }
    
    public boolean save(Pelanggan c) throws RestClientException {
        HttpEntity<Pelanggan> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = save(ServiceUtility.savePelanggan, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean update(Pelanggan c) throws RestClientException {
        HttpEntity<Pelanggan> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = update(ServiceUtility.updatePelanggan, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean delete(Pelanggan c) throws RestClientException {
        HttpEntity<Pelanggan> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = delete(ServiceUtility.deletePelanggan, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public Pelanggan getPelangganById(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getPelangganByID, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToPelanggan(linkedHashMaps.get(0));
    }
    
    public List<Pelanggan> getPelangganAll() throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        Object o = find(ServiceUtility.getPelangganAll, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToPelangganList(linkedHashMaps);
    }
    
    public boolean save(Satuan c) throws RestClientException {
        HttpEntity<Satuan> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = save(ServiceUtility.saveSatuan, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean update(Satuan c) throws RestClientException {
        HttpEntity<Satuan> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = update(ServiceUtility.updateSatuan, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean delete(Satuan c) throws RestClientException {
        HttpEntity<Satuan> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = delete(ServiceUtility.deleteSatuan, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public Satuan getSatuanById(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getSatuanByID, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToSatuan(linkedHashMaps.get(0));
    }
    
    public List<Satuan> getSatuanAll() throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        Object o = find(ServiceUtility.getSatuanAll, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToSatuanList(linkedHashMaps);
    }
    
    public boolean save(SettingAplikasi c) throws RestClientException {
        HttpEntity<SettingAplikasi> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = save(ServiceUtility.saveSettingAplikasi, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean update(SettingAplikasi c) throws RestClientException {
        HttpEntity<SettingAplikasi> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = update(ServiceUtility.updateSettingAplikasi, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean delete(SettingAplikasi c) throws RestClientException {
        HttpEntity<SettingAplikasi> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = delete(ServiceUtility.deleteSettingAplikasi, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean save(Supliyer c) throws RestClientException {
        HttpEntity<Supliyer> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = save(ServiceUtility.saveSupliyer, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean update(Supliyer c) throws RestClientException {
        HttpEntity<Supliyer> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = update(ServiceUtility.updateSupliyer, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean delete(Supliyer c) throws RestClientException {
        HttpEntity<Supliyer> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = delete(ServiceUtility.deleteSupliyer, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public Supliyer getSupliyerById(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getSupliyerByID, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToSupliyer(linkedHashMaps.get(0));
    }
    
    public List<Supliyer> getSupliyerAll() throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        Object o = find(ServiceUtility.getSupliyerAll, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToSupliyerList(linkedHashMaps);
    }
    
    public boolean save(User c) throws RestClientException {
        HttpEntity<User> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = save(ServiceUtility.saveUser, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean saveLogin(LogLogin ll) throws RestClientException {
        HttpEntity<LogLogin> httpEntity = new HttpEntity<>(ll, headers);
        Map<String, Object> response = save(ServiceUtility.saveLogin, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean update(User c) throws RestClientException {
        HttpEntity<User> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = update(ServiceUtility.updateUser, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public boolean delete(User c) throws RestClientException {
        HttpEntity<User> httpEntity = new HttpEntity<>(c, headers);
        Map<String, Object> response = delete(ServiceUtility.deleteUser, httpEntity);
        if (response.containsKey("data")) {
            return true;
        }
        return false;
    }
    
    public User getUserById(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getUserByID, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToUser(linkedHashMaps.get(0));
    }
    
    public User getUserByUNandPass(String un,String ps) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("us", un);
        uriVariables.put("ps", ps);
        Object o = find(ServiceUtility.getUserByUserAndPass, new HttpEntity<User>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToUser(linkedHashMaps.get(0));
    }
    
    public List<User> getAllUserByCabang(Long id) throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("id", id.toString());
        Object o = find(ServiceUtility.getUserByCabang, new HttpEntity<User>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToUserList(linkedHashMaps);
    }
    
    public List<User> getUserAll() throws RestClientException{
        Map<String, Object> uriVariables = new HashMap<>();
        Object o = find(ServiceUtility.getUserAll, new HttpEntity<Barang>(null, headers), uriVariables);
        if(o==null) return null;
        List<LinkedHashMap> linkedHashMaps = (List<LinkedHashMap>) o;
        return ConverterMapToObject.convertMapToUserList(linkedHashMaps);
    }
    
}
